import { RouteObject } from 'react-router';
import Users from '../pages/general/users';
import { semesters } from './semesters';
import { students } from './students';
import { courses } from './courses';
import { rooms } from './rooms';

export const general: RouteObject[] = [
    {
        path: 'users',
        element: <Users />,
    },
    ...students,
    ...semesters,
    ...courses,
    ...rooms,
];
