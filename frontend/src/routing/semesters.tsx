import { RouteObject } from 'react-router';
import Semesters from '../pages/general/semesters';
import SemesterForm from '../pages/general/semesters/SemesterForm';

export const semesters: RouteObject[] = [
    {
        path: 'semesters',
        element: <Semesters />,
    },
    {
        path: 'semesters/add',
        element: <SemesterForm />,
    },
];
