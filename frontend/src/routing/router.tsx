import { RouteObject } from 'react-router-dom';
import MainPage from '../pages/main';
import { user } from './user';
import { general } from './general';
import { studentTools } from './studentTools';
import { administration } from './administration';

export const router: RouteObject[] = [
    {
        path: '/',
        element: <MainPage />,
    },
    ...user,
    {
        path: '/general/',
        children: general,
    },
    {
        path: '/administration/',
        children: administration,
    },
    {
        path: '/students/',
        children: studentTools,
    },
];
