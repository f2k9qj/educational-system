import { RouteObject } from 'react-router';
import Rooms from '../pages/general/rooms';
import RoomForm from '../pages/general/rooms/RoomForm';

export const rooms: RouteObject[] = [
    {
        path: 'rooms',
        element: <Rooms />,
    },
    {
        path: 'rooms/add',
        element: <RoomForm />,
    },
];
