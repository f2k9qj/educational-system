import { RouteObject } from 'react-router';
import Exams from '../pages/administration/exams';
import ExamForm from '../pages/administration/exams/ExamForm';

export const exams: RouteObject[] = [
    {
        path: 'exams',
        element: <Exams />,
    },
    {
        path: 'exams/add',
        element: <ExamForm />,
    },
];
