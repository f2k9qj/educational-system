import { RouteObject } from 'react-router';
import { exams } from './exams';
import { tasks } from './tasks';
import { assessments } from './assessments';

export const administration: RouteObject[] = [...assessments, ...exams, ...tasks];
