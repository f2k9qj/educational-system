import LoginForm from '../pages/account/login';
import Logout from '../pages/account/logout';
import Notifications from '../pages/account/notifications';
import Profile from '../pages/account/profile';
import ProfileEdit from '../pages/account/profile/ProfileEdit';
import RegisterForm from '../pages/account/register';
import StudentFirstLogin from '../pages/account/studentFirstLogin';

export const user = [
    {
        path: 'register',
        element: <RegisterForm />,
    },
    {
        path: 'login',
        element: <LoginForm />,
    },
    {
        path: 'student-first-login',
        element: <StudentFirstLogin />,
    },
    {
        path: 'profile',
        element: <Profile />,
    },
    {
        path: 'profile/edit',
        element: <ProfileEdit />,
    },
    {
        path: 'notifications',
        element: <Notifications />,
    },
    {
        path: 'logout',
        element: <Logout />,
    },
];
