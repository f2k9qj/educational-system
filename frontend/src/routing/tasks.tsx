import { RouteObject } from 'react-router';
import Tasks from '../pages/administration/tasks';
import TaskForm from '../pages/administration/tasks/TaskForm';

export const tasks: RouteObject[] = [
    {
        path: 'tasks',
        element: <Tasks />,
    },
    {
        path: 'tasks/add',
        element: <TaskForm />,
    },
];
