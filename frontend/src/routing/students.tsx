import { RouteObject } from 'react-router';
import Students from '../pages/general/students';
import StudentForm from '../pages/general/students/StudentForm';

export const students: RouteObject[] = [
    {
        path: 'students',
        element: <Students />,
    },
    {
        path: 'students/add',
        element: <StudentForm />,
    },
];
