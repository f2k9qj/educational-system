import { RouteObject } from 'react-router';
import Courses from '../pages/general/courses';
import CourseForm from '../pages/general/courses/CourseForm';

export const courses: RouteObject[] = [
    {
        path: 'courses',
        element: <Courses />,
    },
    {
        path: 'courses/add',
        element: <CourseForm />,
    },
];
