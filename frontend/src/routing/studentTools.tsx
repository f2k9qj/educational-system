import { RouteObject } from 'react-router';
import Result from '../pages/students/results';
import Enrollment from '../pages/students/enrollment';
import Events from '../pages/students/events';

export const studentTools: RouteObject[] = [
    {
        path: 'enrollment',
        element: <Enrollment />,
    },
    {
        path: 'results',
        element: <Result />,
    },
    {
        path: 'events',
        element: <Events />,
    },
];
