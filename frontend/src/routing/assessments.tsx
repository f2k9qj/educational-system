import { RouteObject } from 'react-router';
import Assessments from '../pages/administration/assessments';

export const assessments: RouteObject[] = [
    {
        path: 'assessments',
        element: <Assessments />,
    },
];
