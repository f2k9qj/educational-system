import axios from 'axios';
import { getToken, logout } from '../utils/accessUtil';

const instance = axios.create({
    headers: {
        'Content-Type': 'application/json',
    },
    baseURL: process.env.REACT_APP_BACKEND_URL,
    responseType: 'json',
});

instance.interceptors.request.use(
    (config) => {
        const token = getToken();

        if (token) {
            config.headers = Object.assign({}, config.headers, {
                Authorization: `Bearer ${token}`,
            });
        }

        return config;
    },
    (error) => Promise.reject(error),
);

instance.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        const url = window.location.href;
        const path = new URL(url).pathname;
        if (error.response.status === 401 && path != '/login') {
            logout();
            window.location.href = '/login';
        }
        return Promise.reject(error);
    },
);

export default instance;
