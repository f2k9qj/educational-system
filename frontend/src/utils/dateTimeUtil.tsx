export function convertDateFormat(date?: string) {
    const linearDate = date?.replace(/\s/g, '');
    const parts = linearDate?.split('.').map((part) => part.trim());

    if (parts && parts.length >= 3) {
        const year = parts[0];
        const month = parts[1];
        const day = parts[2];

        return '' + year + '-' + month + '-' + day;
    }

    return null;
}

export function parseDateString(date?: string) {
    const linearDate = date?.replace(/\s/g, '');
    const parts = linearDate?.split('.').map((part) => part.trim());

    if (parts && parts.length >= 3) {
        const year = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10) - 1; // Months are zero-based in JavaScript
        const day = parseInt(parts[2], 10);

        const parsedDate = new Date(year, month, day);

        if (!isNaN(parsedDate.getTime())) {
            return parsedDate;
        }
    }

    return null;
}
