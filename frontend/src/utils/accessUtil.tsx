export function isLoggedIn(): boolean {
    const token = localStorage.getItem('token');

    return token ? true : false;
}

export function getToken(): string | null {
    const token = localStorage.getItem('token');

    return token;
}

export function logout(): void {
    localStorage.setItem('token', '');
}
