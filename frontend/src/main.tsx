import { createRoot } from 'react-dom/client';
import App from './App';
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './components/utils';

const rootNode = document.getElementById('root') as HTMLElement;
const root = createRoot(rootNode);

root.render(
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>,
);
