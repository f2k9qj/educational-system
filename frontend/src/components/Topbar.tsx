import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { appbarHeight, drawerWidth } from './utils';

export default function Topbar({ open, handleDrawerOpen }: { open: boolean; handleDrawerOpen: () => void }) {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar
                position="fixed"
                sx={{
                    pl: { sm: open ? `${drawerWidth}px` : 0 },
                    minHeight: `${appbarHeight}px`,
                }}
            >
                <Toolbar>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ position: 'absolute', flexGrow: 1, textAlign: open ? 'left' : 'center', width: '100%' }}
                    >
                        Tanulmányi rendszer
                    </Typography>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2, display: { xs: 'block', sm: !open ? 'block' : 'none' } }}
                        onClick={() => handleDrawerOpen()}
                    >
                        <MenuIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </Box>
    );
}
