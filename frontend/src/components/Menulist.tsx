import { List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { menu } from './utils';
import ListItemGroup from './ListItemGroup';
import { useNavigate } from 'react-router';
import { useContext } from 'react';
import { RoleContext } from '../App';

export default function Menulist() {
    const navigate = useNavigate();
    const roles = useContext(RoleContext);

    return (
        <List className="listItem" sx={{ padding: 0 }}>
            {menu(roles)?.map((item, i) => {
                if ('children' in item) {
                    return <ListItemGroup key={`itemGroup${i}`} item={item} />;
                } else {
                    return (
                        <ListItem className="listItem" key={`item${i}`}>
                            <ListItemButton onClick={() => navigate(item?.link ?? '/')}>
                                {'icon' in item ? <ListItemIcon>{item.icon}</ListItemIcon> : <></>}
                                <ListItemText primary={item.label} />
                            </ListItemButton>
                        </ListItem>
                    );
                }
            })}
        </List>
    );
}
