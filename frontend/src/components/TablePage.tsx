import {
    Box,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
    useTheme,
} from '@mui/material';
import { DataElement, TableColumnHeader, TableHeaderProps, TablePageProps } from './types';

function TableHeader(props: TableHeaderProps) {
    const theme = useTheme();

    return (
        <TableHead>
            <TableRow>
                {props.columns.map((headCell) => (
                    <TableCell
                        key={headCell.name}
                        sx={{
                            fontWeight: 'bold',
                            backgroundColor: `${theme.palette.primary.main}`,
                            border: '1px groove #f6f6f6',
                            color: '#f6f6f6',
                            minWidth: headCell?.width ?? '100px',
                            textAlign: headCell?.align ?? 'left',
                        }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

function TableData<T extends DataElement>({ data, columns }: { data: T[]; columns: TableColumnHeader[] }) {
    return (
        <TableBody>
            {data.map((dataElement: T, index) => {
                return (
                    <TableRow key={'tableRow' + index} hover>
                        {columns.map((column) => {
                            if (dataElement[column.name]) {
                                return (
                                    <TableCell sx={{ padding: '15px' }} key={column.name + index}>
                                        {dataElement[column.name]}
                                    </TableCell>
                                );
                            } else {
                                return (
                                    <TableCell sx={{ padding: '15px' }} key={column.name + index}>
                                        -
                                    </TableCell>
                                );
                            }
                        })}
                    </TableRow>
                );
            })}
            {data.length === 0 && (
                <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                        Nincs adat
                    </TableCell>
                </TableRow>
            )}
        </TableBody>
    );
}

export default function TablePage(props: TablePageProps) {
    return (
        <Box sx={{ width: '100%', overflow: 'auto' }}>
            <Paper sx={{ padding: '20px' }}>
                <Typography component="h1" variant="h5">
                    {props.title}
                </Typography>
                {props.prefilter}
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                        mb: 1,
                    }}
                >
                    {props.customButton}
                </Box>
                {!props.tableHidden && (
                    <TableContainer sx={{ overflowY: 'scroll', maxHeight: '50rem' }}>
                        <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={'small'}>
                            <TableHeader columns={props.columns} />
                            <TableData
                                data={props.data.map((e: DataElement) => ({
                                    ...e,
                                    operations: props.operations ? props.operations(e) : undefined,
                                }))}
                                columns={props.columns}
                            />
                        </Table>
                    </TableContainer>
                )}
            </Paper>
        </Box>
    );
}
