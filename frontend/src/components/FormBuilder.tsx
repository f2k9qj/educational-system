import { FormControl, Grid, InputLabel, MenuItem, Select, TextField, FormHelperText  } from '@mui/material';
import { DataElement, FormField } from './types';

export default function FormBuilder(data: FormField, errors: DataElement) {
    return (
        <Grid item xs={data?.size ?? 12} key={data.name}>
            {data.type === 'select' ? (
                <FormControl fullWidth required disabled={data.disabled} error={errors[data.errorName ?? data.name] ? true : false} >
                    <InputLabel>{data.label}</InputLabel>
                    <Select
                        label={data.label}
                        name={data.name}
                        required
                        value={data?.value as string}
                        renderValue={(value: string) => {
                            if (data.renderValue && data.renderValue(value, data)) {
                                return data.renderValue(value, data);
                            } else {
                                const option = data.options?.find((option) => option.value === value);
                                return option?.label;
                            }
                        }}
                        error={errors[data.errorName ?? data.name] ? true : false}
                    >
                        {data.options?.map((element) => (
                            <MenuItem key={element.value} value={element.value}>
                                {element.label}
                            </MenuItem>
                        ))}
                    </Select>
                    {
                        (errors[data.errorName ?? data.name] ? true : false) && (
                            <FormHelperText error={true}>{errors[data.errorName ?? data.name] ?? ''}</FormHelperText>
                        )
                    }
                </FormControl>
            ) : (
                <TextField
                    name={data.name}
                    required={data.required}
                    fullWidth
                    label={data.label}
                    disabled={data.disabled}
                    value={data.value}
                    type={data.type}
                    error={errors[data.errorName ?? data.name] ? true : false}
                    helperText={errors[data.errorName ?? data.name] ?? ''}
                    InputLabelProps={{
                        shrink: data.type === 'date' || data.type === 'datetime-local' ? true : undefined,
                    }}
                    InputProps={{ inputProps: { ...data?.inputProps } }}
                />
            )}
        </Grid>
    );
}
