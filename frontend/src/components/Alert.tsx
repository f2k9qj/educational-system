import { useContext } from 'react';
import AlertContext from './AlertProvider';
import { AlertTitle, Snackbar, Alert as MUIAlert } from '@mui/material';

export default function Alert() {
    const useAlert = useContext(AlertContext);

    return (
        <Snackbar
            open={useAlert?.alert.alert}
            autoHideDuration={3000}
            onClose={() => useAlert?.setAlert((prev) => ({ ...prev, alert: false }))}
        >
            <MUIAlert className="message" severity={useAlert?.alert?.type}>
                <AlertTitle>{useAlert?.alert?.message}</AlertTitle>
            </MUIAlert>
        </Snackbar>
    );
}
