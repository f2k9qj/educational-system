import { createTheme } from '@mui/material';
import { deepOrange, lightGreen } from '@mui/material/colors';
import { MenuItem, RoledMenuItem } from './types';
import HomeIcon from '@mui/icons-material/Home';
import { isLoggedIn } from '../utils/accessUtil';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import LogoutIcon from '@mui/icons-material/Logout';
import PeopleIcon from '@mui/icons-material/People';
import SchoolIcon from '@mui/icons-material/School';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import DateRangeIcon from '@mui/icons-material/DateRange';
import LoginIcon from '@mui/icons-material/Login';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import AssessmentIcon from '@mui/icons-material/Assessment';
import PostAddIcon from '@mui/icons-material/PostAdd';
import BookIcon from '@mui/icons-material/Book';
import TaskIcon from '@mui/icons-material/Task';
import AssignmentLateIcon from '@mui/icons-material/AssignmentLate';
import EventIcon from '@mui/icons-material/Event';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import NotificationsIcon from '@mui/icons-material/Notifications';

export const drawerWidth = 210;
export const appbarHeight = 65;

export const theme = createTheme({
    palette: {
        primary: deepOrange,
        secondary: lightGreen,
    },
});

const loggedOffProfile: MenuItem[] = [
    { label: 'Bejelentkezés', link: '/login', icon: <LoginIcon /> },
    { label: 'Regisztráció', link: '/register', icon: <VpnKeyIcon /> },
];

const loggedInProfile: MenuItem[] = [
    { label: 'Profil', link: '/profile', icon: <AccountBoxIcon /> },
    { label: 'Értesítések', link: '/notifications', icon: <NotificationsIcon /> },
    { label: 'Kijelentkezés', link: '/logout', icon: <LogoutIcon /> },
];

const profileItems = () => {
    if (isLoggedIn()) {
        return loggedInProfile;
    } else {
        return loggedOffProfile;
    }
};

const generalItems: RoledMenuItem[] = [
    {
        label: 'Felhasználók',
        link: '/general/users',
        icon: <PeopleIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR'],
    },
    {
        label: 'Hallgatók',
        link: '/general/students',
        icon: <SchoolIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR'],
    },
    {
        label: 'Szemeszterek',
        link: '/general/semesters',
        icon: <DateRangeIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR'],
    },
    {
        label: 'Kurzusok',
        link: '/general/courses',
        icon: <AutoStoriesIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'],
    },
    {
        label: 'Termek',
        link: '/general/rooms',
        icon: <MeetingRoomIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR'],
    },
];

const studentItems: RoledMenuItem[] = [
    {
        label: 'Eredmények',
        link: '/students/results',
        icon: <AssessmentIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_STUDENT'],
    },
    {
        label: 'Kurzusjelentkezés',
        link: '/students/enrollment',
        icon: <PostAddIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_STUDENT'],
    },
    {
        label: 'Események',
        link: '/students/events',
        icon: <EventIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_STUDENT'],
    },
];

const administrationItems: RoledMenuItem[] = [
    {
        label: 'Értékelések',
        link: '/administration/assessments',
        icon: <BookIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'],
    },
    {
        label: 'Vizsgák',
        link: '/administration/exams',
        icon: <AssignmentLateIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'],
    },
    {
        label: 'Feladatok',
        link: '/administration/tasks',
        icon: <TaskIcon />,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'],
    },
];

const roledItemGroups: RoledMenuItem[] = [
    { label: 'Általános', children: generalItems, roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'] },
    {
        label: 'Adminisztráció',
        children: administrationItems,
        roles: ['ROLE_SUPERADMIN', 'ROLE_ADMINISTRATOR', 'ROLE_INSTRUCTOR'],
    },
    { label: 'Hallgatói eszközök', children: studentItems, roles: ['ROLE_SUPERADMIN', 'ROLE_STUDENT'] },
];

const itemsForRole = (roles: string[]): MenuItem[] => {
    if (!isLoggedIn()) {
        return [];
    }
    const filterMenuItems = (items: RoledMenuItem[]): MenuItem[] => {
        return items
            .filter((item) => item?.roles?.some((role) => roles.includes(role)))
            .map((item) => {
                const menuItem: RoledMenuItem = {
                    label: item.label,
                    link: item.link,
                    icon: item.icon,
                };

                if (item.children) {
                    const filteredChildren = filterMenuItems(item.children);
                    if (filteredChildren.length > 0) {
                        menuItem.children = filteredChildren;
                    }
                }

                return menuItem;
            });
    };
    return filterMenuItems(roledItemGroups);
};

export const menu = (roles: string[]): MenuItem[] => {
    return [
        { label: 'Főoldal', link: '/', icon: <HomeIcon /> },
        { label: 'Fiók', children: profileItems() },
        ...itemsForRole(roles),
    ];
};
