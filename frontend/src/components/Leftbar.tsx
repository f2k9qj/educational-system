import { Box, Drawer, IconButton, styled } from '@mui/material';
import { drawerWidth } from './utils';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Menulist from './Menulist';

const DrawerHeader = styled('div')(({ theme }) => {
    return {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: `${theme.palette.primary.main}`,
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    };
});

export default function Leftbar({ open, handleDrawerClose }: { open: boolean; handleDrawerClose: () => void }) {
    return (
        <Box component="nav" sx={{ width: { sm: '15px' }, flexShrink: { sm: 0 } }} aria-label="mailbox folders">
            <Drawer
                variant="temporary"
                open={open}
                onClose={() => handleDrawerClose()}
                ModalProps={{
                    keepMounted: true,
                }}
                sx={{
                    display: { xs: 'block', sm: 'none' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                }}
                color="primary"
            >
                <Menulist />
            </Drawer>
            <Drawer
                variant="persistent"
                sx={{
                    display: { xs: 'none', sm: 'block' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                }}
                open={open}
            >
                <DrawerHeader>
                    <IconButton sx={{ color: 'white' }} onClick={() => handleDrawerClose()}>
                        <ChevronLeftIcon />
                    </IconButton>
                </DrawerHeader>
                <Menulist />
            </Drawer>
        </Box>
    );
}
