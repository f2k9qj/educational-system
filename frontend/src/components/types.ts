import { AlertColor } from '@mui/material';

export interface MessageContextValue {
    successMessage?: string;
    errorMessage?: string;
}

export interface MenuItem {
    label: string;
    icon?: JSX.Element;
    link?: string;
    children?: RoledMenuItem[];
}

export interface RoledMenuItem extends MenuItem {
    roles?: string[];
}

export interface TableHeaderProps {
    columns: TableColumnHeader[];
}

export interface EmbeddedTableProps {
    data: DataElement[];
    columns: TableColumnHeader[];
    customButton?: JSX.Element;
    operations?: (row: DataElement) => JSX.Element;
}

export interface TablePageProps extends EmbeddedTableProps {
    title?: string;
    prefilter?: JSX.Element;
    tableHidden?: boolean;
}

export interface TableColumnHeader {
    name: string;
    label: string;
    width?: string;
    align?: string;
}

export interface DataElement {
    [key: string]: string | number | boolean | JSX.Element | undefined;
}

export interface FormField {
    name: string;
    label: string;
    required?: boolean;
    type?: string;
    disabled?: boolean;
    value?: number | string;
    size?: number;
    errorName?: string;
    inputProps?: DataElement;
    options?: Option[];
    renderValue?: (selected: string, element: FormField) => React.ReactNode;
}

export interface Option {
    label: string;
    value: string;
}

export interface AlertDetails {
    alert: boolean;
    type?: AlertColor;
    message?: string;
}

export interface AlertContextDetails {
    alert: AlertDetails;
    setAlert: React.Dispatch<React.SetStateAction<AlertDetails>>;
}
