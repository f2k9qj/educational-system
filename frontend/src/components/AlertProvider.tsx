import React, { useState } from 'react';
import { AlertContextDetails, AlertDetails } from './types';

const AlertContext = React.createContext<AlertContextDetails | null>(null);

const AlertProvider = ({ children }: { children: React.ReactNode }) => {
    const [alert, setAlert] = useState<AlertDetails>({ alert: false });

    return (
        <AlertContext.Provider
            value={{
                alert: alert,
                setAlert,
            }}
        >
            {children}
        </AlertContext.Provider>
    );
};

export { AlertProvider };
export default AlertContext;
