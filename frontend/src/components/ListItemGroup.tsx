import { Collapse, Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText, useTheme } from '@mui/material';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { useState } from 'react';
import { MenuItem } from './types';
import './ListItem.css';
import { useNavigate } from 'react-router';

export default function ListItemGroup({ item }: { item: MenuItem }) {
    const [open, setOpen] = useState(true);
    const navigate = useNavigate();
    const theme = useTheme();

    const handleClick = () => {
        setOpen(!open);
    };
    return (
        <>
            <Divider />
            <ListItemButton
                sx={{
                    backgroundColor: `${theme.palette.primary.main}`,
                    ':hover': { backgroundColor: `${theme.palette.primary.dark}` },
                }}
                onClick={handleClick}
            >
                {'icon' in item && item['icon'] != undefined && <ListItemIcon>{item.icon}</ListItemIcon>}
                <ListItemText className="white" primary={item.label} />
                {open ? <ExpandLess className="white" /> : <ExpandMore className="white" />}
            </ListItemButton>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    {item.children?.map((subitem, j) => (
                        <ListItem className="listItem" key={`${item.label}Item${j}`}>
                            <ListItemButton onClick={() => navigate(subitem?.link ?? '/')}>
                                {'icon' in subitem ? (
                                    <ListItemIcon sx={{ minWidth: '40px' }}>{subitem.icon}</ListItemIcon>
                                ) : (
                                    <></>
                                )}
                                <ListItemText primary={subitem.label} />
                            </ListItemButton>
                        </ListItem>
                    ))}
                </List>
            </Collapse>
            <Divider />
        </>
    );
}
