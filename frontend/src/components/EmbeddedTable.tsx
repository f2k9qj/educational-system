import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, useTheme } from '@mui/material';
import { DataElement, EmbeddedTableProps, TableColumnHeader, TableHeaderProps } from './types';

function TableHeader(props: TableHeaderProps) {
    const theme = useTheme();

    return (
        <TableHead>
            <TableRow>
                {props.columns.map((headCell) => (
                    <TableCell
                        key={headCell.name}
                        sx={{
                            fontWeight: 'bold',
                            backgroundColor: `${theme.palette.primary.main}`,
                            border: '1px groove #f6f6f6',
                            color: '#f6f6f6',
                        }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

function TableData<T extends DataElement>({ data, columns }: { data: T[]; columns: TableColumnHeader[] }) {
    return (
        <TableBody>
            {data.map((dataElement: T, index) => {
                return (
                    <TableRow key={'tableRow' + index} hover>
                        {columns.map((column) => {
                            if (dataElement[column.name]) {
                                return (
                                    <TableCell sx={{ padding: '15px' }} key={column.name + index}>
                                        {dataElement[column.name]}
                                    </TableCell>
                                );
                            } else {
                                return (
                                    <TableCell sx={{ padding: '15px' }} key={column.name + index}>
                                        -
                                    </TableCell>
                                );
                            }
                        })}
                    </TableRow>
                );
            })}
            {data.length === 0 && (
                <TableRow>
                    <TableCell colSpan={columns.length} align="center">
                        Nincs adat
                    </TableCell>
                </TableRow>
            )}
        </TableBody>
    );
}

export default function EmbeddedTable(props: EmbeddedTableProps) {
    return (
        <TableContainer>
            <Table aria-labelledby="tableTitle" size={'small'}>
                <TableHeader columns={props.columns} />
                <TableData
                    data={props.data.map((e: DataElement) => ({
                        ...e,
                        operations: props.operations ? props.operations(e) : undefined,
                    }))}
                    columns={props.columns}
                />
            </Table>
        </TableContainer>
    );
}
