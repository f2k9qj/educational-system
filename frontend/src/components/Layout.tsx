import { useState } from 'react';
import Topbar from './Topbar';
import Leftbar from './Leftbar';
import { Box } from '@mui/material';
import { appbarHeight, drawerWidth } from './utils';
import { useMediaQuery } from 'react-responsive';
import { useRoutes } from 'react-router';
import { router } from '../routing/router';

export default function Layout() {
    const isMobile = useMediaQuery({ query: `(max-width: 768px)` });
    const [open, setOpen] = useState(isMobile ? false : true);
    const Routing = () => useRoutes(router);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Topbar open={open} handleDrawerOpen={handleDrawerOpen} />
                <Leftbar open={open} handleDrawerClose={handleDrawerClose} />
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    marginLeft: { sm: open ? `${drawerWidth}px` : 0 },
                    marginTop: `${appbarHeight}px`,
                }}
            >
                <main>
                    <Routing />
                </main>
            </Box>
        </>
    );
}
