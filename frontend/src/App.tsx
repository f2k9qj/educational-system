import { Router } from 'react-router-dom';
import { BrowserHistory, createBrowserHistory } from 'history';
import Layout from './components/Layout';
import './App.css';
import { createContext, useEffect, useLayoutEffect, useState } from 'react';
import instance from './api/tools';
import { isLoggedIn } from './utils/accessUtil';
import Loading from './components/Loading';
import { AlertProvider } from './components/AlertProvider';
import Alert from './components/Alert';

export const RoleContext = createContext<string[]>([]);

function App() {
    const [loading, setLoading] = useState(false);
    const [roles, setRoles] = useState<string[]>([]);
    const history = createBrowserHistory({ window });

    useEffect(() => {
        if (isLoggedIn()) {
            setLoading(true);
            instance
                .get('/roles')
                .then((response) => {
                    setRoles(response.data);
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <AlertProvider>
                <CustomRouter history={history}>
                    <RoleContext.Provider value={roles}>
                        <Layout />
                    </RoleContext.Provider>
                </CustomRouter>
                <Alert />
            </AlertProvider>
        </>
    );
}

interface CustomRouterProps {
    history: BrowserHistory;
    children: React.ReactNode;
}
const CustomRouter = (props: CustomRouterProps) => {
    const { history } = props;
    const [state, setState] = useState({
        action: history.action,
        location: history.location,
    });

    useLayoutEffect(() => history.listen(setState), [history]);

    return <Router {...props} location={state.location} navigationType={state.action} navigator={history} />;
};

export default App;
