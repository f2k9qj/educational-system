export const columns = [
    { name: 'createdAt', label: 'Létrehozva' },
    { name: 'title', label: 'Értesítés címe' },
    { name: 'type', label: 'Értesítés típusa' },
    { name: 'operations', label: 'Műveletek' },
];
