import { useEffect, useState } from 'react';
import TablePage from '../../../components/TablePage';
import { INotification } from './types';
import Loading from '../../../components/Loading';
import instance from '../../../api/tools';
import { columns } from './utils';
import { DataElement } from '../../../components/types';
import { IconButton, Tooltip } from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import NotificationModal from './NotificationModal';
import './notifications.css';

export default function Notifications() {
    const [loading, setLoading] = useState(true);
    const [notifications, setNotifications] = useState<INotification[]>([]);
    const [selectedNotification, setSelectedNotification] = useState<INotification>();
    const [open, setOpen] = useState(false);

    useEffect(() => {
        instance
            .get('/notifications/list')
            .then((response) => setNotifications(response.data))
            .catch((error) => console.log('asd', error))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <TablePage
                title="Értesítések"
                data={notifications}
                columns={columns}
                operations={(row: DataElement) => (
                    <Tooltip title="Értesítés megtekintése">
                        <IconButton
                            onClick={() => {
                                setOpen(true);
                                setSelectedNotification(row as INotification);
                            }}
                        >
                            <VisibilityIcon color="info" />
                        </IconButton>
                    </Tooltip>
                )}
            />
            <NotificationModal open={open} setOpen={setOpen} notification={selectedNotification} />
        </>
    );
}
