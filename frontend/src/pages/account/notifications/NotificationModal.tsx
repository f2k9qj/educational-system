import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, useTheme } from '@mui/material';
import { Dispatch, SetStateAction } from 'react';
import { INotification } from './types';

export default function NotificationModal({
    notification,
    open,
    setOpen,
}: {
    notification?: INotification;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
}) {
    const theme = useTheme();

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle
                sx={{
                    backgroundColor: theme.palette.primary.main,
                    color: 'white',
                }}
            >
                {notification?.title}
            </DialogTitle>
            <DialogContent style={{ padding: 0 }}>
                <Box dangerouslySetInnerHTML={{ __html: notification?.message ?? '' }} sx={{ padding: '15px' }}></Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Dialog>
    );
}
