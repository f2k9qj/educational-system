import { DataElement } from '../../../components/types';

export interface INotification extends DataElement {
    id: number;
    title: string;
    message: string;
    type: string;
    createdAt: string;
}
