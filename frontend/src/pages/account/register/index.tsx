import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { FormEvent, useContext } from 'react';
import instance from '../../../api/tools';
import AlertContext from '../../../components/AlertProvider';

export default function RegisterForm() {
    const useAlert = useContext(AlertContext);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        instance
            .post('/register', {
                email: data.get('email'),
                username: data.get('username'),
                password: data.get('password'),
                repassword: data.get('repassword'),
            })
            .then(() => useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres regisztráció!' }))
            .catch(() => useAlert?.setAlert({ alert: true, type: 'error', message: 'Sikertelen regisztráció!' }));
    };

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '50%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Regisztráció
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField name="email" required fullWidth label="Email cím" autoFocus />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField required fullWidth id="username" label="Felhasználónév" name="username" />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField required fullWidth id="password" label="Jelszó" name="password" type="password" />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            id="repassword"
                            label="Jelszó újra"
                            name="repassword"
                            type="password"
                        />
                    </Grid>
                    <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center' }}>
                        <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                            Regisztráció
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
