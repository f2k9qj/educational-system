import { FormField } from '../../../components/types';

export const inputFields = (email: string): FormField[] => [
    {
        label: 'Email cím',
        name: 'email',
        disabled: true,
        required: true,
        value: email,
    },
    {
        label: 'Felhasználónév',
        name: 'username',
        required: true,
    },
    {
        label: 'Jelszó',
        name: 'password',
        required: true,
        type: 'password',
    },
    {
        label: 'Jelszó újra',
        name: 'repassword',
        errorName: 'password',
        required: true,
        type: 'password',
    },
    {
        label: 'Vezetéknév',
        name: 'lastName',
        required: true,
        size: 6,
    },
    {
        label: 'Keresztnév',
        name: 'firstName',
        required: true,
        size: 6,
    },
    {
        label: 'Anyja vezetékneve',
        name: 'motherLastName',
        size: 6,
    },
    {
        label: 'Anyja keresztneve',
        name: 'motherFirstName',
        size: 6,
    },
    {
        label: 'Diákigazolvány száma',
        name: 'studentCardNumber',
        size: 6,
    },
    {
        label: 'Személyigazolvány száma',
        name: 'idCardNumber',
        size: 6,
    },
    {
        label: 'Születési helye',
        name: 'birthPlace',
        size: 6,
    },
    {
        label: 'Születési ideje',
        name: 'birthDate',
        type: 'date',
        size: 6,
    },
];
