import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { FormEvent, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement } from '../../../components/types';
import FirstLoginModal from './FirstLoginModal';

export default function StudentFirstLogin() {
    const [firstLogin, setFirstLogin] = useState(false);
    const [errors, setErrors] = useState<DataElement>({});
    const [email, setEmail] = useState('');
    const [studentId, setStudentId] = useState('');

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setEmail(event.currentTarget.email.value);
        setErrors({});
        instance
            .post('/students/email-check', {
                email: data.get('email'),
            })
            .then((response) => {
                setFirstLogin(true);
                setStudentId(response.data.student);
            })
            .catch((error) => setErrors(error.response.data.violations));
    };

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '50%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Hallgatói első bejelentkezés
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            name="email"
                            required
                            fullWidth
                            label="Email cím"
                            error={errors.email ? true : false}
                            helperText={errors.email ?? ''}
                            autoFocus
                        />
                    </Grid>
                    <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center' }}>
                        <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                            Aktiválás elkezdése
                        </Button>
                    </Grid>
                </Grid>
            </Box>
            {firstLogin && (
                <FirstLoginModal email={email} studentId={studentId} open={firstLogin} setOpen={setFirstLogin} />
            )}
        </>
    );
}
