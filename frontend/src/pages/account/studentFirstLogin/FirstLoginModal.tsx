import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    TextField,
    useTheme,
} from '@mui/material';
import { Dispatch, FormEvent, SetStateAction, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement, FormField } from '../../../components/types';
import { inputFields } from './utils';
import { useNavigate } from 'react-router';

export default function FirstLoginModal({
    email,
    studentId,
    open,
    setOpen,
}: {
    email: string;
    studentId: string;
    open: true;
    setOpen: Dispatch<SetStateAction<boolean>>;
}) {
    const theme = useTheme();
    const navigate = useNavigate();
    const [errors, setErrors] = useState<DataElement>({});

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const password = event.currentTarget.password.value;
        const repassword = event.currentTarget.repassword.value;
        const same = password === repassword;

        if (same) {
            instance
                .post('/students/activate', {
                    studentId: studentId,
                    username: data.get('username'),
                    email: data.get('email'),
                    password: data.get('password'),
                    lastName: data.get('lastName'),
                    firstName: data.get('firstName'),
                    motherLastName: data.get('motherLastName'),
                    motherFirstName: data.get('motherFirstName'),
                    studentCardNumber: data.get('studentCardNumber'),
                    idCardNumber: data.get('idCardNumber'),
                    birthDate: data.get('birthDate'),
                    birthPlace: data.get('birthPlace'),
                })
                .then(() => {
                    navigate('/login');
                })
                .catch((error) => setErrors(error.response.data.violations));
        } else {
            setErrors({ password: 'A jelszavaknak egyezniük kell!' });
        }
    };

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)}>
            <DialogTitle sx={{ backgroundColor: theme.palette.primary.main, color: 'white' }}>
                Hallgatói adatok megadása aktiváláshoz
            </DialogTitle>
            <DialogContent style={{ marginTop: '4px' }}>
                <Box component="form" noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        {inputFields(email).map((element: FormField) => (
                            <Grid item xs={element?.size ?? 12} key={element.name}>
                                <TextField
                                    name={element.name}
                                    required={element.required}
                                    fullWidth
                                    label={element.label}
                                    disabled={element.disabled}
                                    value={element.value}
                                    type={element.type}
                                    error={errors[element.errorName ?? element.name] ? true : false}
                                    helperText={(errors[element.errorName ?? element.name] as string) ?? ''}
                                    InputLabelProps={{ shrink: element.type === 'date' ? true : undefined }}
                                />
                            </Grid>
                        ))}
                        <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center' }}>
                            <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                                Aktiválás
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </DialogContent>
            <DialogActions></DialogActions>
        </Dialog>
    );
}
