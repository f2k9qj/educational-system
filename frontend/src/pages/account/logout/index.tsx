import { useEffect } from 'react';
import { logout } from '../../../utils/accessUtil';

export default function Logout() {
    useEffect(() => {
        logout();
        window.location.href = '/';
    }, []);

    return <>Kijelentkezés...</>;
}
