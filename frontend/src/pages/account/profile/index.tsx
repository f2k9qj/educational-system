import { Box, Grid, IconButton, Typography } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import Loading from '../../../components/Loading';
import instance from '../../../api/tools';
import { IProfile } from './types';
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from 'react-router';
import { RoleContext } from '../../../App';

export default function Profile() {
    const roles = useContext(RoleContext);
    const student = roles.some((element) => element === 'ROLE_STUDENT');
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState<IProfile>();

    useEffect(() => {
        if (student) {
            instance.get('/profile/student').then((response) => {
                setData(response.data);
                setLoading(false);
            });
        } else {
            instance.get('/profile').then((response) => {
                setData(response.data);
                setLoading(false);
            });
        }
    }, [student]);

    if (loading) {
        return <Loading />;
    }

    return (
        <Box sx={{ mt: 3, boxShadow: 3, width: '50%', padding: '20px' }}>
            <Grid container spacing={2}>
                <Grid item xs={11}>
                    <Typography component="h1" variant="h5">
                        Profil
                    </Typography>
                </Grid>
                <Grid item xs={1}>
                    <IconButton onClick={() => navigate('/profile/edit')}>
                        <EditIcon />
                    </IconButton>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        <span style={{ fontWeight: 'bold' }}>Email cím:</span> {data?.email ?? ''}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        <span style={{ fontWeight: 'bold' }}>Felhasználónév:</span> {data?.username ?? ''}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        <span style={{ fontWeight: 'bold' }}>Vezetéknév:</span> {data?.lastName ?? '-'}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        <span style={{ fontWeight: 'bold' }}>Keresztnév:</span> {data?.firstName ?? '-'}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        <span style={{ fontWeight: 'bold' }}>Jogosultságok:</span> {data?.roles ?? ''}
                    </Typography>
                </Grid>
                {student && (
                    <>
                        <Grid item xs={11} sx={{ mt: '10px' }}>
                            <Typography component="h1" variant="h6">
                                Hallgatói adatok
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Anyja vezetékneve:</span>{' '}
                                {data?.motherLastName ?? ''}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Anyja keresztneve:</span>{' '}
                                {data?.motherFirstName ?? ''}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Diákigazolvány száma:</span>{' '}
                                {data?.studentCardNumber ?? ''}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Személyigazolvány száma:</span>{' '}
                                {data?.idCardNumber ?? ''}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Születés ideje:</span> {data?.birthDate ?? ''}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                <span style={{ fontWeight: 'bold' }}>Születés helye:</span> {data?.birthPlace ?? ''}
                            </Typography>
                        </Grid>
                    </>
                )}
            </Grid>
        </Box>
    );
}
