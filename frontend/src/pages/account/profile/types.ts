export interface IProfile {
    id: number;
    email?: string;
    username?: string;
    lastName?: string;
    firstName?: string;
    roles?: string;
    studentId?: number;
    motherLastName?: string;
    motherFirstName?: string;
    studentCardNumber?: string;
    idCardNumber?: string;
    birthDate?: string;
    birthPlace?: string;
}
