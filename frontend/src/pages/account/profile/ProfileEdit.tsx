import { FormEvent, useContext, useEffect, useState } from 'react';
import { IProfile } from './types';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { Alert, AlertTitle, Box, Button, Grid, TextField, Typography } from '@mui/material';
import { RoleContext } from '../../../App';
import { convertDateFormat } from '../../../utils/dateTimeUtil';

export default function ProfileEdit() {
    const roles = useContext(RoleContext);
    const student = roles.some((element) => element === 'ROLE_STUDENT');
    const [loading, setLoading] = useState(true);
    const [profile, setProfile] = useState<IProfile>();
    const [successAlert, setSuccessAlert] = useState<boolean>(false);

    useEffect(() => {
        if (student) {
            instance.get('/profile/student').then((response) => {
                setProfile(response.data);
                setLoading(false);
            });
        } else {
            instance.get('/profile').then((response) => {
                setProfile(response.data);
                setLoading(false);
            });
        }
    }, [student]);

    useEffect(() => {
        if (successAlert) {
            setTimeout(() => {
                setSuccessAlert(false);
            }, 3000);
        }
    }, [successAlert]);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        if (student) {
            instance
                .post('/profile/student/edit', {
                    id: profile?.id,
                    email: data.get('email'),
                    username: data.get('username'),
                    lastName: data.get('lastName'),
                    firstName: data.get('firstName'),
                    studentId: profile?.studentId,
                    motherLastName: data.get('motherLastName'),
                    motherFirstName: data.get('motherFirstName'),
                    studentCardNumber: data.get('studentCardNumber'),
                    idCardNumber: data.get('idCardNumber'),
                    birthDate: data.get('birthDate'),
                    birthPlace: data.get('birthPlace'),
                })
                .then(() => setSuccessAlert(true));
        } else {
            instance
                .post('/profile/edit', {
                    id: profile?.id,
                    email: data.get('email'),
                    username: data.get('username'),
                    lastName: data.get('lastName'),
                    firstName: data.get('firstName'),
                })
                .then(() => setSuccessAlert(true));
        }
    };

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Profil szerkesztése
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="email"
                            name="email"
                            label="Email cím"
                            size="small"
                            defaultValue={profile?.email}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="username"
                            name="username"
                            label="Felhasználónév"
                            size="small"
                            defaultValue={profile?.username}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="lastName"
                            name="lastName"
                            label="Vezetéknév"
                            size="small"
                            defaultValue={profile?.lastName}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            id="firstName"
                            name="firstName"
                            label="Keresztnév"
                            size="small"
                            defaultValue={profile?.firstName}
                            fullWidth
                        />
                    </Grid>
                    {student && (
                        <>
                            <Grid item xs={6}>
                                <TextField
                                    id="motherLastName"
                                    name="motherLastName"
                                    label="Anyja vezetékneve"
                                    size="small"
                                    defaultValue={profile?.motherLastName}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="motherFirstName"
                                    name="motherFirstName"
                                    label="Anyja keresztneve"
                                    size="small"
                                    defaultValue={profile?.motherFirstName}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="studentCardNumber"
                                    name="studentCardNumber"
                                    label="Diákigazolvány száma"
                                    size="small"
                                    defaultValue={profile?.studentCardNumber}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="idCardNumber"
                                    name="idCardNumber"
                                    label="Személyigazolvány száma"
                                    size="small"
                                    defaultValue={profile?.idCardNumber}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="birthDate"
                                    name="birthDate"
                                    label="Születési dátum"
                                    size="small"
                                    defaultValue={convertDateFormat(profile?.birthDate)}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    type="date"
                                />
                            </Grid>{' '}
                            <Grid item xs={6}>
                                <TextField
                                    id="birthPlace"
                                    name="birthPlace"
                                    label="Születés helye"
                                    size="small"
                                    defaultValue={profile?.birthPlace}
                                    fullWidth
                                />
                            </Grid>
                        </>
                    )}
                </Grid>
                <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                    <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                        Szerkesztés
                    </Button>
                </Grid>
            </Box>
            {successAlert && (
                <Alert className="message" severity="success">
                    <AlertTitle>Sikeres szerkesztés</AlertTitle>
                </Alert>
            )}
        </>
    );
}
