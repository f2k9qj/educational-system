import { Box, Button, Grid, Link, TextField, Typography } from '@mui/material';
import { FormEvent, useState } from 'react';
import instance from '../../../api/tools';

export default function LoginForm() {
    const [isError, setIsError] = useState(false);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        setIsError(false);

        instance
            .post('/login', {
                username: data.get('username'),
                password: data.get('password'),
            })
            .then((response) => {
                localStorage.setItem('token', response.data.token);
                window.location.href = '/profile';
            })
            .catch(() => setIsError(true));
    };

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '50%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Bejelentkezés
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            id="username"
                            label="Felhasználónév"
                            name="username"
                            error={isError}
                            helperText={isError ? 'Helytelen felhasználónév vagy jelszó' : ''}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            id="password"
                            label="Jelszó"
                            name="password"
                            type="password"
                            error={isError}
                            helperText={isError ? 'Helytelen felhasználónév vagy jelszó' : ''}
                        />
                    </Grid>
                    <Grid item xs={12} sx={{ display: 'grid', placeItems: 'end', paddingTop: '10px !important' }}>
                        <Link href="student-first-login" variant="body2">
                            Hallgatói első bejelentkezés
                        </Link>
                    </Grid>
                    <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', paddingTop: '10px !important' }}>
                        <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                            Bejelentkezés
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}
