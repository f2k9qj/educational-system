import { useEffect, useState } from 'react';
import Loading from '../../../components/Loading';
import TablePage from '../../../components/TablePage';
import {
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
} from '@mui/material';
import { Option } from '../../../components/types';
import instance from '../../../api/tools';
import { IExam } from './types';
import { columns } from './utils';
import { useNavigate } from 'react-router';

export default function Exams() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [courseOptions, setCourseOptions] = useState<Option[]>([]);
    const [course, setCourse] = useState<string>();
    const [exams, setExams] = useState<IExam[]>([]);

    useEffect(() => {
        instance
            .get('/exams/course-options')
            .then((response) => setCourseOptions(response.data))
            .finally(() => setLoading(false));
    }, [loading]);

    useEffect(() => {
        if (course) {
            instance.get(`/exams/course/${course}/list`).then((response) => setExams(response.data));
        }
    }, [course]);

    const handleCourseChange = (event: SelectChangeEvent<string>) => {
        setCourse(event.target.value);
    };

    if (loading) {
        return <Loading />;
    }

    return (
        <TablePage
            title="Vizsgák"
            data={exams}
            columns={columns}
            customButton={
                course ? (
                    <Button
                        variant="outlined"
                        size="small"
                        onClick={() => navigate('/administration/exams/add?course=' + course)}
                    >
                        Vizsga hozzáadása
                    </Button>
                ) : (
                    <></>
                )
            }
            prefilter={
                <Grid item xs={12} key="courses" sx={{ mt: '15px', mb: '15px' }}>
                    <FormControl fullWidth required size="small">
                        <InputLabel>Kurzusok</InputLabel>
                        <Select
                            label="Kurzusok"
                            name="courses"
                            required
                            value={course}
                            onChange={handleCourseChange}
                            renderValue={(selected: string) =>
                                courseOptions.find((option) => option.value === selected)?.label
                            }
                        >
                            {courseOptions.map((element) => (
                                <MenuItem key={element.value} value={element.value}>
                                    {element.label}
                                </MenuItem>
                            ))}
                        </Select>
                        <FormHelperText>Válassza ki azt a kurzust, aminek a vizsgáit kezelni szeretné</FormHelperText>
                    </FormControl>
                </Grid>
            }
            tableHidden={course ? false : true}
        />
    );
}
