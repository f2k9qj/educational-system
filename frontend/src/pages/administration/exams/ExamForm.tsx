import {
    Box,
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from '@mui/material';
import { FormEvent, useContext, useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement, FormField, Option } from '../../../components/types';
import { inputFields } from './utils';
import { useLocation, useNavigate } from 'react-router';
import AlertContext from '../../../components/AlertProvider';

export default function ExamForm() {
    const navigate = useNavigate();
    const location = useLocation();
    const params = new URLSearchParams(location.search);
    const [loading, setLoading] = useState<boolean>(true);
    const [courseOptions, setCourseOptions] = useState<Option[]>([]);
    const [scoringOptions, setScoringOptions] = useState<Option[]>([]);
    const [course, _] = useState<string>(params.get('course') ?? '');
    const [errors, setErrors] = useState<DataElement>({});
    const useAlert = useContext(AlertContext);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await instance.get('/exams/course-options');
                const response2 = await instance.get('/exams/scoring-options');

                if (response1.status === 200 && response2.status === 200) {
                    setCourseOptions(response1.data);
                    setScoringOptions(response2.data);
                }
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [loading]);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/exams/add', {
                course: course,
                name: data.get('name'),
                description: data.get('description'),
                scoring: data.get('scoring'),
                startDate: data.get('startDate'),
                endDate: data.get('endDate'),
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres hozzáadás!' });
                navigate('/administration/exams');
            })
            .catch((error) => setErrors(error.response.data.violations))
            .finally(() => console.log('errors', errors));
    };

    return (
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography component="h1" variant="h5">
                        Vizsga hozzáadása
                    </Typography>
                </Grid>
                {inputFields(course, courseOptions, scoringOptions).map((element: FormField) => (
                    <Grid item xs={element?.size ?? 12} key={element.name}>
                        {element.type === 'select' ? (
                            <FormControl
                                fullWidth
                                required
                                disabled={element.disabled}
                                error={errors[element.errorName ?? element.name] ? true : false}
                            >
                                <InputLabel>{element.label}</InputLabel>
                                <Select
                                    label={element.label}
                                    name={element.name}
                                    required
                                    value={element?.value as string}
                                    renderValue={(value: string) => {
                                        const option = element.options?.find((option) => option.value === value);
                                        return option?.label;
                                    }}
                                >
                                    {element.options?.map((element) => (
                                        <MenuItem key={element.value} value={element.value}>
                                            {element.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                                <FormHelperText>{errors[element.errorName ?? element.name] ?? ''}</FormHelperText>
                            </FormControl>
                        ) : (
                            <TextField
                                name={element.name}
                                required={element.required}
                                fullWidth
                                label={element.label}
                                disabled={element.disabled}
                                value={element.value}
                                type={element.type}
                                error={errors[element.errorName ?? element.name] ? true : false}
                                helperText={errors[element.errorName ?? element.name] ?? ''}
                                InputLabelProps={{
                                    shrink:
                                        element.type === 'date' || element.type === 'datetime-local' ? true : undefined,
                                }}
                                InputProps={{ inputProps: { ...element?.inputProps } }}
                            />
                        )}
                    </Grid>
                ))}
            </Grid>
            <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                    Hozzáadás
                </Button>
            </Grid>
        </Box>
    );
}
