import { FormField, Option } from '../../../components/types';

export const columns = [
    { name: 'name', label: 'Vizsga' },
    { name: 'scoring', label: 'Értékelés típusa' },
    { name: 'startDate', label: 'Vizsga kezdete' },
    { name: 'endDate', label: 'Vizsga vége' },
];

export const inputFields = (course: string, courseOptions: Option[], scoringOptions: Option[]): FormField[] => [
    {
        label: 'Kurzus',
        name: 'course',
        disabled: true,
        required: true,
        value: course,
        type: 'select',
        options: courseOptions,
    },
    {
        label: 'Vizsga neve',
        name: 'name',
        required: true,
        size: 12,
    },
    {
        label: 'Vizsga leírása',
        name: 'description',
        size: 12,
    },
    {
        label: 'Értékelés típusa',
        name: 'scoring',
        required: true,
        type: 'select',
        options: scoringOptions,
        size: 12,
    },
    {
        label: 'Vizsga kezdete',
        name: 'startDate',
        required: true,
        type: 'datetime-local',
        size: 6,
    },
    {
        label: 'Vizsga vége',
        name: 'endDate',
        required: true,
        type: 'datetime-local',
        size: 6,
    },
];
