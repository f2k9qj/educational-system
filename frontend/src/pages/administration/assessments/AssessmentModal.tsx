import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    useTheme,
} from '@mui/material';
import { Dispatch, FormEvent, SetStateAction, useContext } from 'react';
import { IAssessmentModalDetails } from './types';
import instance from '../../../api/tools';
import AlertContext from '../../../components/AlertProvider';

export default function AssessmentModal({
    assessmentModalDetails,
    open,
    setOpen,
    handleRefresh,
}: {
    assessmentModalDetails: IAssessmentModalDetails;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
    handleRefresh: () => void;
}) {
    const theme = useTheme();

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle
                sx={{
                    backgroundColor: theme.palette.primary.main,
                    color: 'white',
                }}
            >
                {assessmentModalDetails.studentName}: {assessmentModalDetails.assessmentName}
            </DialogTitle>
            <DialogContent style={{ marginTop: '4px', padding: 0 }}>
                <AssessmentModalForm
                    assessmentModalDetails={assessmentModalDetails}
                    setOpen={setOpen}
                    handleRefresh={handleRefresh}
                />
            </DialogContent>
        </Dialog>
    );
}

function AssessmentModalForm({
    assessmentModalDetails,
    setOpen,
    handleRefresh,
}: {
    assessmentModalDetails: IAssessmentModalDetails;
    setOpen: Dispatch<SetStateAction<boolean>>;
    handleRefresh: () => void;
}) {
    const useAlert = useContext(AlertContext);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        instance
            .post('/assessments/' + assessmentModalDetails.assessmentId, {
                grade: data.get('grade'),
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres értékelés!' });
            })
            .catch(() => useAlert?.setAlert({ alert: true, type: 'error', message: 'Értékelés meghiúsult!' }))
            .finally(() => {
                setOpen(false);
                handleRefresh();
            });
    };

    return (
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ padding: '20px 20px 0 25px' }}>
            <Grid container spacing={1} sx={{ marginBottom: '10px' }}>
                <Grid item xs={6}>
                    <TextField
                        label="Értékelés típusa"
                        name="scoring"
                        size="small"
                        value={assessmentModalDetails.scoring}
                        disabled
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6}>
                    {assessmentModalDetails.scoringType != 'percentage' ? (
                        <FormControl fullWidth required size="small">
                            <InputLabel>Értékelés</InputLabel>
                            {assessmentModalDetails.scoringType === 'grade' && (
                                <Select label="Értékelés" name="grade" required>
                                    <MenuItem value="1">1</MenuItem>
                                    <MenuItem value="2">2</MenuItem>
                                    <MenuItem value="3">3</MenuItem>
                                    <MenuItem value="4">4</MenuItem>
                                    <MenuItem value="5">5</MenuItem>
                                </Select>
                            )}
                            {assessmentModalDetails.scoringType === 'done' && (
                                <Select label="Értékelés" name="grade" required>
                                    <MenuItem value="1">Kész</MenuItem>
                                    <MenuItem value="0">Nincs kész</MenuItem>
                                </Select>
                            )}
                        </FormControl>
                    ) : (
                        <TextField
                            label="Értékelés"
                            name="grade"
                            size="small"
                            fullWidth
                            type="number"
                            inputProps={{ min: 1, max: 100 }}
                        />
                    )}
                </Grid>
            </Grid>
            <DialogActions>
                <Button type="submit" variant="contained">
                    Értékelés
                </Button>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Box>
    );
}
