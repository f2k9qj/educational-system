import { useEffect, useState } from 'react';
import Loading from '../../../components/Loading';
import TablePage from '../../../components/TablePage';
import {
    FormControl,
    Grid,
    IconButton,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    Tooltip,
} from '@mui/material';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import { DataElement, Option } from '../../../components/types';
import instance from '../../../api/tools';
import { IAssessmentModalDetails, IExam } from './types';
import { columns } from './utils';
import AssessmentModal from './AssessmentModal';

export default function Assessments() {
    const [loading, setLoading] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [courseOptionLoad, setCourseOptionLoad] = useState(false);
    const [studentOptions, setStudentOptions] = useState<Option[]>([]);
    const [courseOptions, setCourseOptions] = useState<Option[]>([]);
    const [student, setStudent] = useState<string>();
    const [course, setCourse] = useState<string>();
    const [exams, setExams] = useState<IExam[]>([]);
    const [openAssessment, setOpenAssessment] = useState<boolean>(false);
    const [assessmentModalDetails, setAssessMentModalDetails] = useState<IAssessmentModalDetails>({});

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await instance.get('/assessments/student-options');
                if (response1.status === 200) {
                    setStudentOptions(response1.data);
                }
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [loading]);

    useEffect(() => {
        if (student) {
            setCourseOptionLoad(true);
            instance
                .get('/assessments/course-options?student=' + student)
                .then((response) => setCourseOptions(response.data))
                .finally(() => setCourseOptionLoad(false));
        }
    }, [student]);

    useEffect(() => {
        if (student && course) {
            instance
                .get(`/assessments/student/${student}/course/${course}/list`)
                .then((response) => setExams(response.data));
        }
    }, [student, course, refresh]);

    const handleStudentChange = (event: SelectChangeEvent<string>) => {
        setStudent(event.target.value);
        setCourse('');
    };

    const handleCourseChange = (event: SelectChangeEvent<string>) => {
        setCourse(event.target.value);
    };

    const handleRefresh = () => {
        setRefresh((prev) => !prev);
    };

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <TablePage
                title="Értékelések"
                data={exams}
                columns={columns}
                prefilter={
                    <>
                        <Grid item xs={12} key="students" sx={{ mt: '15px', mb: '15px' }}>
                            <FormControl fullWidth required size="small">
                                <InputLabel>Hallgatók</InputLabel>
                                <Select
                                    label="Hallgatók"
                                    name="students"
                                    required
                                    value={student ?? ''}
                                    onChange={handleStudentChange}
                                    renderValue={(selected: string) =>
                                        studentOptions.find((option) => option.value === selected)?.label
                                    }
                                >
                                    {studentOptions.map((element) => (
                                        <MenuItem key={element.value} value={element.value}>
                                            {element.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        {courseOptionLoad ? (
                            <Skeleton />
                        ) : (
                            <Grid item xs={12} key="courses" sx={{ mt: '15px', mb: '15px' }}>
                                <FormControl fullWidth required size="small" disabled={!student}>
                                    <InputLabel>Kurzusok</InputLabel>
                                    <Select
                                        label="Kurzusok"
                                        name="courses"
                                        required
                                        value={course ?? ''}
                                        onChange={handleCourseChange}
                                        renderValue={(selected: string) =>
                                            courseOptions.find((option) => option.value === selected)?.label
                                        }
                                    >
                                        {courseOptions.map((element) => (
                                            <MenuItem key={element.value} value={element.value}>
                                                {element.label}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                        )}
                    </>
                }
                tableHidden={student && course ? false : true}
                operations={(row: DataElement) => (
                    <Tooltip title="Hallgató értékelése">
                        <IconButton
                            onClick={() => {
                                setOpenAssessment(true);
                                setAssessMentModalDetails({
                                    assessmentId: row.id as number,
                                    assessmentName: row.name as string,
                                    scoring: row.scoring as string,
                                    scoringType: row.scoringType as string,
                                    studentName: studentOptions.find((option) => option.value === student)?.label,
                                });
                            }}
                        >
                            <AssignmentTurnedInIcon color="info" />
                        </IconButton>
                    </Tooltip>
                )}
            />
            <AssessmentModal
                assessmentModalDetails={assessmentModalDetails}
                open={openAssessment}
                setOpen={setOpenAssessment}
                handleRefresh={handleRefresh}
            />
        </>
    );
}
