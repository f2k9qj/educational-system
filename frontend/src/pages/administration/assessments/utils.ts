import { FormField, Option } from '../../../components/types';

export const columns = [
    { name: 'name', label: 'Értékelés neve' },
    { name: 'due', label: 'Határidő' },
    { name: 'scoring', label: 'Értékelés típusa' },
    { name: 'grade', label: 'Értékelés' },
    { name: 'gradedAt', label: 'Értékelve ekkor' },
    { name: 'operations', label: 'Műveletek' },
];

export const inputFields = (
    course: string,
    courseOptions: Option[],
    renderCourse: (selected: string, element: FormField) => React.ReactNode,
): FormField[] => [
    {
        label: 'Kurzus',
        name: 'course',
        disabled: true,
        required: true,
        value: course,
        type: 'select',
        options: courseOptions,
        renderValue: renderCourse,
    },
    {
        label: 'Vizsga neve',
        name: 'name',
        required: true,
        size: 12,
    },
    {
        label: 'Vizsga leírása',
        name: 'description',
        size: 12,
    },
    {
        label: 'Vizsga kezdete',
        name: 'startDate',
        required: true,
        type: 'datetime-local',
        size: 6,
    },
    {
        label: 'Vizsga vége',
        name: 'endDate',
        required: true,
        type: 'datetime-local',
        size: 6,
    },
];
