import { FormField, Option } from '../../../components/types';

export const columns = [
    { name: 'name', label: 'Feladat neve' },
    { name: 'type', label: 'Feladat típusa' },
    { name: 'scoring', label: 'Értékelés típusa' },
    { name: 'deadline', label: 'Határidő' },
];

export const inputFields = (
    course: string,
    courseOptions: Option[],
    typeOptions: Option[],
    scoringOptions: Option[],
): FormField[] => [
    {
        label: 'Kurzus',
        name: 'course',
        disabled: true,
        required: true,
        value: course,
        type: 'select',
        options: courseOptions,
    },
    {
        label: 'Feladat neve',
        name: 'name',
        required: true,
        size: 12,
    },
    {
        label: 'Feladat leírása',
        name: 'description',
        size: 12,
    },
    {
        label: 'Feladat típusa',
        name: 'type',
        required: true,
        type: 'select',
        options: typeOptions,
        size: 6,
    },
    {
        label: 'Értékelés típusa',
        name: 'scoring',
        required: true,
        type: 'select',
        options: scoringOptions,
        size: 6,
    },
    {
        label: 'Leadási határidő',
        name: 'deadline',
        required: true,
        type: 'datetime-local',
        size: 12,
    },
];
