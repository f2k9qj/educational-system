import { DataElement } from '../../../components/types';

export interface ITask extends DataElement {
    id: number;
    name: string;
    description: string;
    startDate: string;
    endDate: string;
}
