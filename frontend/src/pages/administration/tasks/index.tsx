import { useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { Option } from '../../../components/types';
import TablePage from '../../../components/TablePage';
import {
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
} from '@mui/material';
import { useNavigate } from 'react-router';
import { columns } from './utils';
import { ITask } from './types';

export default function Tasks() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [courseOptions, setCourseOptions] = useState<Option[]>([]);
    const [course, setCourse] = useState<string>();
    const [tasks, setTasks] = useState<ITask[]>([]);

    useEffect(() => {
        instance
            .get('/tasks/course-options')
            .then((response) => setCourseOptions(response.data))
            .finally(() => setLoading(false));
    }, [loading]);

    useEffect(() => {
        if (course) {
            instance.get(`/tasks/course/${course}/list`).then((response) => setTasks(response.data));
        }
    }, [course]);

    const handleCourseChange = (event: SelectChangeEvent<string>) => {
        setCourse(event.target.value);
    };

    return (
        <TablePage
            title="Feladatok"
            data={tasks}
            columns={columns}
            customButton={
                course ? (
                    <Button
                        variant="outlined"
                        size="small"
                        onClick={() => navigate('/administration/tasks/add?course=' + course)}
                    >
                        Feladat hozzáadása
                    </Button>
                ) : (
                    <></>
                )
            }
            prefilter={
                <Grid item xs={12} key="courses" sx={{ mt: '15px', mb: '15px' }}>
                    <FormControl fullWidth required size="small">
                        <InputLabel>Kurzusok</InputLabel>
                        <Select
                            label="Kurzusok"
                            name="courses"
                            required
                            value={course}
                            onChange={handleCourseChange}
                            renderValue={(selected: string) =>
                                courseOptions.find((option) => option.value === selected)?.label
                            }
                        >
                            {courseOptions.map((element) => (
                                <MenuItem key={element.value} value={element.value}>
                                    {element.label}
                                </MenuItem>
                            ))}
                        </Select>
                        <FormHelperText>Válassza ki azt a kurzust, aminek a feladatait kezelni szeretné</FormHelperText>
                    </FormControl>
                </Grid>
            }
            tableHidden={course ? false : true}
        />
    );
}
