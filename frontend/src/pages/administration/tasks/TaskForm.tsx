import { Box, Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField, Typography } from '@mui/material';
import { FormEvent, useContext, useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement, FormField, Option } from '../../../components/types';
import { inputFields } from './utils';
import { useLocation, useNavigate } from 'react-router';
import AlertContext from '../../../components/AlertProvider';
import FormBuilder from '../../../components/FormBuilder';

export default function TaskForm() {
    const navigate = useNavigate();
    const location = useLocation();
    const params = new URLSearchParams(location.search);
    const [loading, setLoading] = useState<boolean>(true);
    const [courseOptions, setCourseOptions] = useState<Option[]>([]);
    const [typeOptions, setTypeOptions] = useState<Option[]>([]);
    const [scoringOptions, setScoringOptions] = useState<Option[]>([]);
    const [course, _] = useState<string>(params.get('course') ?? '');
    const [errors, setErrors] = useState<DataElement>({});
    const useAlert = useContext(AlertContext);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await instance.get('/tasks/course-options');
                const response2 = await instance.get('/tasks/type-options');
                const response3 = await instance.get('/tasks/scoring-options');

                if (response1.status === 200 && response2.status === 200 && response3.status === 200) {
                    setCourseOptions(response1.data);
                    setTypeOptions(response2.data);
                    setScoringOptions(response3.data);
                }
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [loading]);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/tasks/add', {
                course: course,
                name: data.get('name'),
                description: data.get('description'),
                type: data.get('type'),
                scoring: data.get('scoring'),
                deadline: data.get('deadline'),
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres hozzáadás!' });
                navigate('/administration/tasks');
            })
            .catch((error) => setErrors(error.response.data.violations))
            .finally(() => console.log('errors', errors));
    };

    return (
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography component="h1" variant="h5">
                        Feladat hozzáadása
                    </Typography>
                </Grid>
                {inputFields(course, courseOptions, typeOptions, scoringOptions).map((element: FormField) => 
                            FormBuilder(element, errors),
                )}        
            </Grid>
            <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                    Hozzáadás
                </Button>
            </Grid>
        </Box>
    );
}
