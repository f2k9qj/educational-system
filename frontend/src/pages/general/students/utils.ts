export const columns = [
    { name: 'username', label: 'Felhasználónév' },
    { name: 'email', label: 'Email cím' },
    { name: 'name', label: 'Hallgató neve' },
    { name: 'motherName', label: 'Anyja neve' },
    { name: 'birthPlace', label: 'Születési helye' },
    { name: 'birthDate', label: 'Születési ideje' },
];
