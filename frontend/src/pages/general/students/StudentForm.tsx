import { Alert, AlertTitle, Box, Button, Grid, TextField, Typography } from '@mui/material';
import { FormEvent, useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement } from '../../../components/types';
import { useNavigate } from 'react-router';

export default function StudentForm() {
    const navigate = useNavigate();
    const [errors, setErrors] = useState<DataElement>({});
    const [successAlert, setSuccessAlert] = useState<boolean>(false);

    useEffect(() => {
        if (successAlert) {
            setTimeout(() => {
                setSuccessAlert(false);
                navigate('/general/students');
            }, 3000);
        }
    }, [successAlert, navigate]);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/students/add', {
                email: data.get('email'),
            })
            .then(() => setSuccessAlert(true))
            .catch((error) => setErrors(error.response.data.violations));
    };

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Hallgató hozzáadása
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            size="small"
                            fullWidth
                            id="email"
                            label="Email cím"
                            name="email"
                            error={errors.email ? true : false}
                            helperText={errors.email ?? ''}
                        />
                    </Grid>
                </Grid>
                <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                    <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                        Hozzáadás
                    </Button>
                </Grid>
            </Box>
            {successAlert && (
                <Alert className="message" severity="success">
                    <AlertTitle>Sikeres hozzzáadás</AlertTitle>
                </Alert>
            )}
        </>
    );
}
