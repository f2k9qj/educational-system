import { Button } from '@mui/material';
import { useNavigate } from 'react-router';
import TablePage from '../../../components/TablePage';
import { columns } from './utils';
import { useEffect, useState } from 'react';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { IStudent } from './types';

export default function Students() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [students, setStudents] = useState<IStudent[]>([]);

    useEffect(() => {
        instance
            .get('/students/list')
            .then((response) => setStudents(response.data))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <TablePage
            title="Hallgatók"
            data={students}
            columns={columns}
            customButton={
                <Button variant="outlined" size="small" onClick={() => navigate('/general/students/add')}>
                    Hallgató hozzáadása
                </Button>
            }
        />
    );
}
