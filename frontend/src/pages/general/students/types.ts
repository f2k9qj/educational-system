import { DataElement } from '../../../components/types';

export interface IStudent extends DataElement {
    id: number;
    username: string;
    email: string;
    name: string;
    motherName: string;
    birthPlace: string;
    birthDate: string;
}
