import {
    Alert,
    AlertTitle,
    Box,
    Button,
    Chip,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Typography,
} from '@mui/material';
import { FormEvent, useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement, FormField, Option } from '../../../components/types';
import { inputFields } from './utils';
import Loading from '../../../components/Loading';
import { useNavigate } from 'react-router';
import FormBuilder from '../../../components/FormBuilder';

export default function CourseForm() {
    const navigate = useNavigate();
    const [instructors, setInstructors] = useState<string[]>([]);
    const [loading, setLoading] = useState(true);
    const [instructorOptions, setInstructorOptions] = useState<Option[]>([]);
    const [roomOptions, setRoomOptions] = useState<Option[]>([]);
    const [errors, setErrors] = useState<DataElement>({});
    const [successAlert, setSuccessAlert] = useState<boolean>(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await instance.get('/courses/instructor-options');
                const response2 = await instance.get('/courses/room-options');
                if (response1.status === 200 && response2.status === 200) {
                    setInstructorOptions(response1.data);
                    setRoomOptions(response2.data);
                }
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [loading]);

    useEffect(() => {
        if (successAlert) {
            setTimeout(() => {
                setSuccessAlert(false);
                navigate('/general/courses');
            }, 3000);
        }
    }, [successAlert, navigate]);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/courses/add', {
                name: data.get('name'),
                courseCode: data.get('courseCode'),
                location: data.get('location'),
                minHeadCount: data.get('minHeadCount'),
                maxHeadCount: data.get('maxHeadCount'),
                maxAbsence: data.get('maxAbsence'),
                instructors: data.get('instructors'),
            })
            .then(() => setSuccessAlert(true))
            .catch((error) => setErrors(error.response.data.violations ?? {}))
            .finally(() => console.log('errors', errors));
    };

    const handleInstructorChange = (event: SelectChangeEvent<string[]>) => {
        setInstructors([...event.target.value]);
    };

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Kurzus hozzáadása
                        </Typography>
                    </Grid>
                    {inputFields(instructorOptions, roomOptions).map((element: FormField) =>
                        FormBuilder(element, errors),
                    )}
                    <Grid item xs={12} key="instructors">
                        <FormControl fullWidth error={!!errors['instructors']} required>
                            <InputLabel>Oktatók</InputLabel>
                            <Select
                                label="Oktatók"
                                name="instructors"
                                multiple
                                required
                                value={instructors}
                                onChange={handleInstructorChange}
                                renderValue={(selected: string[]) =>
                                    selected.map((elem) => {
                                        const instructorOption = instructorOptions.find(
                                            (option) => option.value === elem,
                                        );
                                        return <Chip key={elem} label={instructorOption?.label} variant="outlined" />;
                                    })
                                }
                            >
                                {instructorOptions.map((element) => (
                                    <MenuItem key={element.value} value={element.value}>
                                        {element.label}
                                    </MenuItem>
                                ))}
                            </Select>
                            <FormHelperText>{errors['instructors']}</FormHelperText>
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                    <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                        Hozzáadás
                    </Button>
                </Grid>
            </Box>
            {successAlert && (
                <Alert className="message" severity="success">
                    <AlertTitle>Sikeres hozzzáadás</AlertTitle>
                </Alert>
            )}
        </>
    );
}
