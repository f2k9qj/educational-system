import { FormField, Option } from '../../../components/types';

export const columns = [
    { name: 'name', label: 'Kurzus neve' },
    { name: 'courseCode', label: 'Kurzus kódja' },
    { name: 'location', label: 'Helyszín' },
    { name: 'instructors', label: 'Oktatók' },
    { name: 'capacity', label: 'Férőhelyek' },
    { name: 'operations', label: 'Műveletek' },
];

export const studentListColumns = [
    { name: 'name', label: 'Teljes név' },
    { name: 'email', label: 'Email cím' },
];

export const inputFields = (instructorOptions: Option[], roomOptions: Option[]): FormField[] => [
    {
        label: 'Kurzus neve',
        name: 'name',
        required: true,
        size: 6,
    },
    {
        label: 'Kurzus kódja',
        name: 'courseCode',
        required: true,
        size: 6,
    },
    {
        label: 'Helyszín',
        name: 'location',
        required: true,
        type: 'select',
        options: roomOptions,
    },
    {
        label: 'Kurzus minimum létszáma',
        name: 'minHeadCount',
        size: 4,
        type: 'number',
        inputProps: {
            min: 0,
            max: 99,
        },
    },
    {
        label: 'Kurzus maximum létszáma',
        name: 'maxHeadCount',
        size: 4,
        type: 'number',
        inputProps: {
            min: 0,
            max: 99,
        },
    },
    {
        label: 'Maximum hiányzás',
        name: 'maxAbsence',
        size: 4,
        type: 'number',
        inputProps: {
            min: 0,
            max: 99,
        },
    },
];
