import { DataElement } from '../../../components/types';

export interface ICourse extends DataElement {
    id: number;
    name: string;
    courseCode: string;
    location: string;
    instructors: string;
    capacity: string;
}

export interface ICourseStudent extends DataElement {
    id: number;
    name: string;
    email: string;
}
