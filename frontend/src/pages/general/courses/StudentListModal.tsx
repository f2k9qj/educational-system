import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Skeleton, useTheme } from '@mui/material';
import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import EmbeddedTable from '../../../components/EmbeddedTable';
import { studentListColumns } from './utils';
import instance from '../../../api/tools';
import { ICourseStudent } from './types';

export default function StudentListModal({
    courseId,
    open,
    setOpen,
}: {
    courseId?: number;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
}) {
    const theme = useTheme();
    const [loading, setLoading] = useState<boolean>(false);
    const [data, setData] = useState<ICourseStudent[]>([]);

    useEffect(() => {
        if (courseId) {
            instance
                .get(`/courses/${courseId}/students`)
                .then((response) => {
                    setData(response.data);
                })
                .finally(() => setLoading(false));
        }
    }, [courseId]);

    if (loading) {
        return <Skeleton />;
    }

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle sx={{ backgroundColor: theme.palette.primary.main, color: 'white' }}>
                Kurzus hallgatói
            </DialogTitle>
            <DialogContent style={{ marginTop: '4px' }}>
                <EmbeddedTable data={data} columns={studentListColumns} />
            </DialogContent>

            <DialogActions>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Dialog>
    );
}
