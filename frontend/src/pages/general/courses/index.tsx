import { useEffect, useState } from 'react';
import TablePage from '../../../components/TablePage';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { ICourse } from './types';
import { columns } from './utils';
import { Button, IconButton, Tooltip } from '@mui/material';
import { useNavigate } from 'react-router';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import StudentListModal from './StudentListModal';
import { DataElement } from '../../../components/types';

export default function Courses() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [courses, setCourses] = useState<ICourse[]>([]);
    const [openStudentList, setOpenStudentList] = useState<boolean>(false);
    const [courseId, setCourseId] = useState<number | undefined>();

    useEffect(() => {
        instance
            .get('/courses/list')
            .then((response) => setCourses(response.data))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <TablePage
                title="Kurzusok"
                data={courses}
                columns={columns}
                customButton={
                    <Button variant="outlined" size="small" onClick={() => navigate('/general/courses/add')}>
                        Kurzus hozzáadása
                    </Button>
                }
                operations={(row: DataElement) => (
                    <Tooltip title="Hallgatók megtekintése">
                        <IconButton
                            onClick={() => {
                                setOpenStudentList(true);
                                setCourseId(row.id as number);
                            }}
                        >
                            <PermContactCalendarIcon color="info" />
                        </IconButton>
                    </Tooltip>
                )}
            />
            <StudentListModal courseId={courseId} open={openStudentList} setOpen={setOpenStudentList} />
        </>
    );
}
