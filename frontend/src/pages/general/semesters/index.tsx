import { useEffect, useState } from 'react';
import TablePage from '../../../components/TablePage';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { columns } from './utils';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router';
import { ISemester } from './types';

export default function Semesters() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [semesters, setSemesters] = useState<ISemester[]>([]);

    useEffect(() => {
        instance
            .get('/semesters/list')
            .then((response) => setSemesters(response.data))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <TablePage
            title="Szemeszterek"
            data={semesters}
            columns={columns}
            customButton={
                <Button variant="outlined" size="small" onClick={() => navigate('/general/semesters/add')}>
                    Szemeszter hozzáadása
                </Button>
            }
        />
    );
}
