export const columns = [
    { name: 'name', label: 'Szemeszter neve' },
    { name: 'startDate', label: 'Szemeszter kezdete' },
    { name: 'endDate', label: 'Szemeszter vége' },
    { name: 'enrollmentStartDate', label: 'Beiratkozás kezdete' },
    { name: 'enrollmentEndDate', label: 'Beiratkozás vége' },
];
