import { DataElement } from '../../../components/types';

export interface ISemester extends DataElement {
    id: number;
    name: string;
    startDate: string;
    endDate: string;
    enrollmentStart: string;
    enrollmentEnd: string;
}
