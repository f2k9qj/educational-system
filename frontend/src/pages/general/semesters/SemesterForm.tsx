import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { FormEvent, useContext, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement } from '../../../components/types';
import AlertContext from '../../../components/AlertProvider';
import { useNavigate } from 'react-router';

export default function SemesterForm() {
    const useAlert = useContext(AlertContext);
    const navigate = useNavigate();
    const [errors, setErrors] = useState<DataElement>({});

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/semesters/add', {
                name: data.get('name'),
                startDate: data.get('startDate'),
                endDate: data.get('endDate'),
                enrollmentStartDate: data.get('enrollmentStartDate'),
                enrollmentEndDate: data.get('enrollmentEndDate'),
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres hozzáadás!' });
                navigate('/general/semesters');
            })
            .catch((error) => setErrors(error.response.data.violations))
            .finally(() => console.log('errors', errors));
    };

    return (
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography component="h1" variant="h5">
                        Szemeszter hozzáadása
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        size="small"
                        fullWidth
                        id="name"
                        label="Szemeszter neve"
                        name="name"
                        error={errors.name ? true : false}
                        helperText={errors.name ?? ''}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id="startDate"
                        name="startDate"
                        label="Szemeszter kezdete"
                        type="date"
                        size="small"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        error={errors.startDate ? true : false}
                        helperText={errors.startDate ?? ''}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id="endDate"
                        name="endDate"
                        label="Szemeszter vége"
                        type="date"
                        size="small"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        error={errors.endDate ? true : false}
                        helperText={errors.endDate ?? ''}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id="enrollmentStartDate"
                        name="enrollmentStartDate"
                        label="Beiratkozás kezdete"
                        type="date"
                        size="small"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        error={errors.enrollmentStartDate ? true : false}
                        helperText={errors.enrollmentStartDate ?? ''}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id="enrollmentEndDate"
                        name="enrollmentEndDate"
                        label="Beiratkozás vége"
                        type="date"
                        size="small"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        error={errors.enrollmentEndDate ? true : false}
                        helperText={errors.enrollmentEndDate ?? ''}
                    />
                </Grid>
            </Grid>
            <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                    Hozzáadás
                </Button>
            </Grid>
        </Box>
    );
}
