import { useEffect, useState } from 'react';
import TablePage from '../../../components/TablePage';
import { columns } from './utils';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { DataElement, Option } from '../../../components/types';
import { IconButton, Tooltip } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import RolesEditModal from './RolesEditModal';

export default function Users() {
    const [loading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(false);
    const [users, setUsers] = useState([]);
    const [roles, setRoles] = useState<Option[]>([]);
    const [selected, setSelected] = useState<DataElement>();
    const [open, setOpen] = useState<boolean>(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await instance.get('/users/list');
                const response2 = await instance.get('/users/roles-options');
                if (response1.status === 200 && response2.status === 200) {
                    setUsers(response1.data);
                    setRoles(response2.data);
                }
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [refresh]);

    const handleRefresh = () => {
        setRefresh((prev) => !prev);
    };

    if (loading) {
        return <Loading />;
    }
    return (
        <>
            <TablePage
                title="Felhasználók"
                data={users}
                columns={columns}
                operations={(row: DataElement) => (
                    <Tooltip title="Jogosultságok szerkesztése">
                        <IconButton
                            sx={{ cursor: 'default' }}
                            onClick={() => {
                                setSelected(row);
                                setOpen(true);
                            }}
                        >
                            <EditIcon color="warning" />
                        </IconButton>
                    </Tooltip>
                )}
            />
            <RolesEditModal
                open={open}
                setOpen={setOpen}
                refresh={handleRefresh}
                rolesOptions={roles}
                selected={selected}
            />
        </>
    );
}
