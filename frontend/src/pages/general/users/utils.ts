export const columns = [
    { name: 'email', label: 'Email cím' },
    { name: 'username', label: 'Felhasználónév' },
    { name: 'lastName', label: 'Vezetéknév' },
    { name: 'firstName', label: 'Keresztnév' },
    { name: 'roles', label: 'Jogosultságok' },
    { name: 'operations', label: 'Műveletek' },
];
