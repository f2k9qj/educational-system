import {
    Button,
    Chip,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Skeleton,
    useTheme,
} from '@mui/material';
import { Dispatch, SetStateAction, useContext, useEffect, useState } from 'react';
import { DataElement, Option } from '../../../components/types';
import instance from '../../../api/tools';
import AlertContext from '../../../components/AlertProvider';

export default function RolesEditModal({
    rolesOptions,
    selected,
    open,
    setOpen,
    refresh,
}: {
    rolesOptions?: Option[];
    selected?: DataElement;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
    refresh: () => void;
}) {
    const theme = useTheme();
    const [loading, setLoading] = useState(true);
    const [selectedRoles, setSelectedRoles] = useState<string[]>([]);
    const useAlert = useContext(AlertContext);

    useEffect(() => {
        if (selected?.id) {
            instance
                .get('/users/' + selected?.id + '/roles')
                .then((response) => setSelectedRoles(response.data))
                .finally(() => setLoading(false));
        }
    }, [selected?.id]);

    const handleRolesChange = (event: SelectChangeEvent<string[]>) => {
        setSelectedRoles([...event.target.value]);
    };

    const handleSubmit = () => {
        instance
            .post('/users/' + selected?.id + '/edit', {
                roles: selectedRoles,
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, type: 'success', message: 'Sikeres mentés!' });
            })
            .catch(() => useAlert?.setAlert({ alert: true, type: 'error', message: 'Sikertelen mentés!' }))
            .finally(() => {
                setOpen(false);
                refresh();
            });
    };

    if (loading && !selected?.id) return <Skeleton />;

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle sx={{ backgroundColor: theme.palette.primary.main, color: 'white' }}>
                {selected?.username} jogosultságainak szerkesztése
            </DialogTitle>
            <DialogContent style={{ marginTop: '10px', padding: '10px 20px 10px 20px' }}>
                <FormControl fullWidth required>
                    <InputLabel>Jogosultságok</InputLabel>
                    <Select
                        label="Jogosultságok"
                        name="roles"
                        multiple
                        required
                        value={selectedRoles}
                        onChange={handleRolesChange}
                        renderValue={(selected: string[]) =>
                            selected.map((elem) => {
                                if (elem === 'ROLE_USER') return;
                                const role = rolesOptions?.find((option) => option.value === elem);
                                return <Chip key={elem} label={role?.label} variant="outlined" />;
                            })
                        }
                    >
                        {rolesOptions?.map((element) => (
                            <MenuItem key={element.value} value={element.value}>
                                {element.label}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button type="submit" variant="contained" onClick={handleSubmit}>
                    Mentés
                </Button>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Dialog>
    );
}
