import { useEffect, useState } from 'react';
import TablePage from '../../../components/TablePage';
import instance from '../../../api/tools';
import Loading from '../../../components/Loading';
import { columns } from './utils';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router';
import { IRoom } from './types';

export default function Rooms() {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [rooms, setRooms] = useState<IRoom[]>([]);

    useEffect(() => {
        instance
            .get('/rooms/list')
            .then((response) => setRooms(response.data))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loading />;
    }

    return (
        <TablePage
            title="Termek"
            data={rooms}
            columns={columns}
            customButton={
                <Button variant="outlined" size="small" onClick={() => navigate('/general/rooms/add')}>
                    Terem hozzáadása
                </Button>
            }
        />
    );
}
