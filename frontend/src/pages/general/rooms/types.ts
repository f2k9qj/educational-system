import { DataElement } from '../../../components/types';

export interface IRoom extends DataElement {
    id: number;
    name: string;
    postCode: string;
    city: string;
    streetName: string;
    streetType: string;
}
