import { FormField } from '../../../components/types';

export const columns = [
    { name: 'name', label: 'Terem neve' },
    { name: 'postCode', label: 'Irányítószám' },
    { name: 'city', label: 'Város' },
    { name: 'streetName', label: 'Közterület neve' },
    { name: 'streetType', label: 'Közterület típusa' },
    { name: 'houseNumber', label: 'Házszám' },
];

export const inputFields = (): FormField[] => [
    {
        label: 'Terem neve',
        name: 'name',
        required: true,
    },
    {
        label: 'Irányítószám',
        name: 'postCode',
        required: true,
        size: 4,
    },
    {
        label: 'Város',
        name: 'city',
        required: true,
        size: 4,
    },
    {
        label: 'Közterület neve',
        name: 'streetName',
        required: true,
        size: 4,
    },
    {
        label: 'Közterület típusa',
        name: 'streetType',
        required: true,
        size: 4,
    },
    {
        label: 'Házszám',
        name: 'houseNumber',
        required: true,
        size: 4,
    },
];
