import { Box, Button, Grid, Typography } from '@mui/material';
import { FormEvent, useContext, useState } from 'react';
import instance from '../../../api/tools';
import { DataElement, FormField } from '../../../components/types';
import { inputFields } from './utils';
import { useNavigate } from 'react-router';
import FormBuilder from '../../../components/FormBuilder';
import AlertContext from '../../../components/AlertProvider';

export default function RoomForm() {
    const navigate = useNavigate();
    const [errors, setErrors] = useState<DataElement>({});
    const useAlert = useContext(AlertContext);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setErrors({});

        instance
            .post('/rooms/add', {
                name: data.get('name'),
                postCode: data.get('postCode'),
                city: data.get('city'),
                streetName: data.get('streetName'),
                streetType: data.get('streetType'),
                houseNumber: data.get('houseNumber'),
            })
            .then(() => {
                useAlert?.setAlert({ alert: true, message: 'Terem sikeresen hozzáadva' });
                navigate('/general/rooms');
            })
            .catch((error) => setErrors(error.response.data.violations));
    };

    return (
        <>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3, boxShadow: 3, width: '80%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Terem hozzáadása
                        </Typography>
                    </Grid>
                    {inputFields().map((element: FormField) => FormBuilder(element, errors))}
                </Grid>
                <Grid item xs={12} sx={{ display: 'grid', placeItems: 'center', marginTop: '15px' }}>
                    <Button type="submit" variant="contained" sx={{ minWidth: '20%' }}>
                        Hozzáadás
                    </Button>
                </Grid>
            </Box>
        </>
    );
}
