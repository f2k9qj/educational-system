export const columns = [
    { name: 'name', label: 'Kurzus neve' },
    { name: 'courseCode', label: 'Kurzus kódja' },
    { name: 'location', label: 'Helyszín' },
    { name: 'instructors', label: 'Oktatók' },
    { name: 'operations', label: 'Műveletek' },
];
