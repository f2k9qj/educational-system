import { useContext, useEffect, useState } from 'react';
import Loading from '../../../components/Loading';
import TablePage from '../../../components/TablePage';
import { columns } from './utils';
import instance from '../../../api/tools';
import { IconButton, Tooltip } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { IEnrollmentCourse } from './types';
import { DataElement } from '../../../components/types';
import AlertContext from '../../../components/AlertProvider';

export default function Enrollment() {
    const [loading, setLoading] = useState<boolean>(true);
    const [courses, setCourses] = useState<IEnrollmentCourse[]>([]);
    const useAlert = useContext(AlertContext);
    const [reload, setReload] = useState<boolean>(false);

    useEffect(() => {
        instance
            .get('/students/courses')
            .then((response) => setCourses(response.data))
            .finally(() => setLoading(false));
    }, [reload]);

    const makeReload = () => {
        setReload(!reload);
    };

    const handleEnroll = (courseId?: number) => {
        instance
            .post(`/students/courses/${courseId}/enroll`)
            .then((response) => {
                if (response.data.status === 'enrolled') {
                    useAlert?.setAlert({ alert: true, message: 'Sikeres jelentkezés', type: 'success' });
                } else if (response.data.status === 'failed') {
                    useAlert?.setAlert({ alert: true, message: response.data.message, type: 'error' });
                }
            })
            .finally(() => makeReload());
    };

    if (loading) {
        return <Loading />;
    }

    return (
        <>
            <TablePage
                title="Kurzusjelentkezés"
                data={courses}
                columns={columns}
                operations={(row: DataElement) =>
                    row.enrolled ? (
                        <Tooltip title="Kurzus felvéve">
                            <IconButton sx={{ cursor: 'default' }}>
                                <CheckCircleIcon color="success" />
                            </IconButton>
                        </Tooltip>
                    ) : (
                        <Tooltip title="Jelentkezés">
                            <IconButton onClick={() => handleEnroll(row.id as number)}>
                                <AddCircleIcon color="warning" />
                            </IconButton>
                        </Tooltip>
                    )
                }
            />
        </>
    );
}
