import { DataElement } from '../../../components/types';

export interface IEnrollmentCourse extends DataElement {
    id: number;
    name: string;
    courseCode: string;
    location: string;
    instructors: string;
    enrolled: boolean;
}
