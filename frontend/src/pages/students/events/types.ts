export interface IDateParts {
    year: number;
    month: number;
    day: number;
    hour: number;
    minute: number;
    second: number;
}

export interface IBackendEvent {
    title: string;
    description?: string;
    color?: string;
    start: IDateParts;
    end: IDateParts;
}

export interface IFrontendEvent {
    title: string;
    description?: string;
    color?: string;
    start: Date;
    end: Date;
}
