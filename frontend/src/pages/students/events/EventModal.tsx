import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Paper, Typography, useTheme } from '@mui/material';
import { Dispatch, SetStateAction } from 'react';
import { IFrontendEvent } from './types';

export default function EventModal({
    event,
    open,
    setOpen,
}: {
    event?: IFrontendEvent;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
}) {
    const theme = useTheme();

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle sx={{ backgroundColor: theme.palette.primary.main, color: 'white' }}>
                {event?.title}
            </DialogTitle>
            <DialogContent style={{ marginTop: '10px', paddingBottom: 0 }}>
                <Typography variant="h6" sx={{ marginLeft: '10px' }}>
                    Az esemény leírása:
                </Typography>
                <Paper
                    sx={{
                        padding: '20px',
                        margin: '10px',
                    }}
                    elevation={3}
                >
                    <Typography variant="inherit">
                        {event?.description != '' ? event?.description : 'Nincs az eseményhez tartozó leírás!'}
                    </Typography>
                </Paper>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Dialog>
    );
}
