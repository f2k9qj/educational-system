import { Calendar, dateFnsLocalizer } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import startOfWeek from 'date-fns/startOfWeek';
import getDay from 'date-fns/getDay';
import hu from 'date-fns/locale/hu';
import { calendarTrans } from './trans';
import { Box, Paper, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import instance from '../../../api/tools';
import { IBackendEvent, IFrontendEvent } from './types';
import EventModal from './EventModal';

const locales = {
    'hu': hu,
};

const localizer = dateFnsLocalizer({
    format,
    parse,
    startOfWeek,
    getDay,
    locales,
});

export default function Events() {
    const [events, setEvents] = useState<IBackendEvent[]>([]);
    const [open, setOpen] = useState<boolean>(false);
    const [selectedEvent, setSelectedEvent] = useState<IFrontendEvent>();

    useEffect(() => {
        instance.get('students/events').then((response) => setEvents(response.data));
    }, []);

    const eventStyleGetter = (event: IFrontendEvent) => {
        const backgroundColor = event.color ?? '#f72a2a';
        const style = {
            backgroundColor: backgroundColor,
            borderRadius: '5px',
            opacity: 0.9,
            color: 'white',
            border: '1px',
        };
        return {
            style: style,
        };
    };

    return (
        <Box sx={{ width: '95%', maxHeight: '90vh', overflow: 'auto' }}>
            <Typography component="h1" variant="h5" margin={'10px 0 0 10px'}>
                Események
            </Typography>
            <Paper
                sx={{
                    padding: '20px',
                    margin: '10px',
                }}
                elevation={3}
            >
                <Calendar
                    localizer={localizer}
                    defaultDate={new Date()}
                    defaultView="month"
                    style={{ minHeight: 700, overflow: 'auto' }}
                    culture={'hu'}
                    messages={calendarTrans}
                    formats={{ agendaDateFormat: 'MMMM d. EEEE' }}
                    onSelectEvent={(event: IFrontendEvent) => {
                        setOpen(true);
                        setSelectedEvent(event);
                    }}
                    events={[
                        ...events.map((element: IBackendEvent) => ({
                            'title': element.title,
                            'description': element.description,
                            'color': element.color,
                            'start': new Date(
                                element.start.year,
                                element.start.month,
                                element.start.day,
                                element.start.hour,
                                element.start.minute,
                                element.start.second,
                            ),
                            'end': new Date(
                                element.end.year,
                                element.end.month,
                                element.end.day,
                                element.end.hour,
                                element.end.minute,
                                element.end.second,
                            ),
                        })),
                    ]}
                    eventPropGetter={eventStyleGetter}
                />
            </Paper>
            {open && <EventModal open={open} setOpen={setOpen} event={selectedEvent} />}
        </Box>
    );
}
