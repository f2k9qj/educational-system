export interface IResultData {
    firstMonth: number;
    lastMonth: number;
    grades: Record<string, IResult[]>;
}

export interface IResult {
    month: number;
    date: string;
    grade: string;
    scoring: string;
}

export interface IAvgs {
    examAvg: number;
    mandatoryAvg: number;
    optionalsDone: number;
    optionalsAvg: number;
}
