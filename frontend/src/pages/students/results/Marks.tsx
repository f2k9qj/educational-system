import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { IResult } from './types';
import { Chip, Tooltip } from '@mui/material';
import { useState } from 'react';
import ResultModal from './ResultModal';

export default function Marks({ data }: { data: IResult[] }) {
    const [open, setOpen] = useState(false);
    const [clickedGrade, setClickedGrade] = useState<IResult>();

    return (
        <>
            {data.map((grade) => (
                <Tooltip key={grade.date} title={'Értékelés megtekintése'}>
                    <Chip
                        key={grade.date}
                        label={
                            <>
                                {grade.scoring === 'percentage' ? (
                                    grade.grade + '%'
                                ) : grade.scoring === 'grade' ? (
                                    grade.grade
                                ) : grade.grade === '1' ? (
                                    <CheckIcon sx={{ padding: 0 }} />
                                ) : (
                                    <CloseIcon sx={{ padding: 0 }} />
                                )}
                            </>
                        }
                        sx={{ margin: '0 2px 0 2px' }}
                        onClick={() => {
                            setClickedGrade(grade);
                            setOpen(true);
                        }}
                    />
                </Tooltip>
            ))}
            <ResultModal open={open} setOpen={setOpen} clickedGrade={clickedGrade} />
        </>
    );
}
