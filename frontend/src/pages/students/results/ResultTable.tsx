import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, useTheme } from '@mui/material';
import { IResult, IResultData } from './types';
import { columns, months } from './utils';
import { TableColumnHeader, TableHeaderProps } from '../../../components/types';
import Marks from './Marks';
import { useEffect, useState } from 'react';
import instance from '../../../api/tools';

function TableHeader(props: TableHeaderProps) {
    const theme = useTheme();

    return (
        <TableHead>
            <TableRow>
                {props.columns.map((headCell) => (
                    <TableCell
                        key={headCell.name}
                        sx={{
                            fontWeight: 'bold',
                            backgroundColor: `${theme.palette.primary.main}`,
                            border: '1px groove #f6f6f6',
                            color: '#f6f6f6',
                            minWidth: headCell?.width ?? '100px',
                            textAlign: headCell?.align ?? 'left',
                        }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

function TableData({ data, columns }: { data: Record<string, IResult[]>; columns: TableColumnHeader[] }) {
    return (
        <TableBody>
            {Object.entries(data).map(([key, value]) => {
                return (
                    <TableRow key={key} hover>
                        {columns.map((column) => {
                            if (column.name === 'course') {
                                return (
                                    <TableCell sx={{ padding: '15px' }} key={column.name + key}>
                                        {key}
                                    </TableCell>
                                );
                            } else {
                                const marks = value.filter((element) => months[element.month - 1] === column.label);
                                if (marks.length != 0) {
                                    return (
                                        <TableCell sx={{ padding: '15px' }} key={column.name + key}>
                                            <Marks data={marks} />
                                        </TableCell>
                                    );
                                } else {
                                    return (
                                        <TableCell sx={{ padding: '15px' }} key={column.name + key}>
                                            -
                                        </TableCell>
                                    );
                                }
                            }
                        })}
                    </TableRow>
                );
            })}
        </TableBody>
    );
}

export default function ResultTable() {
    const [data, setData] = useState<IResultData>({ firstMonth: 1, lastMonth: 12, grades: {} });

    useEffect(() => {
        instance.get('students/results').then((response) => {
            setData(response.data);
        });
    }, []);

    return (
        <TableContainer>
            <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={'small'}>
                <TableHeader columns={columns(data.firstMonth, data.lastMonth) as TableColumnHeader[]} />
                <TableData
                    data={data.grades}
                    columns={columns(data.firstMonth, data.lastMonth) as TableColumnHeader[]}
                />
            </Table>
        </TableContainer>
    );
}
