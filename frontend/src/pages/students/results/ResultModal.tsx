import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Typography, useTheme } from '@mui/material';
import { scoring } from './utils';
import { Dispatch, SetStateAction } from 'react';
import { IResult } from './types';

export default function ResultModal({
    clickedGrade,
    open,
    setOpen,
}: {
    clickedGrade?: IResult;
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
}) {
    const theme = useTheme();

    return (
        <Dialog open={open} fullWidth onClose={() => setOpen(false)} maxWidth="sm">
            <DialogTitle sx={{ backgroundColor: theme.palette.primary.main, color: 'white' }}>
                Értékelés megtekintése
            </DialogTitle>
            <DialogContent style={{ marginTop: '4px', paddingTop: '10px' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant="body1">
                            <span style={{ fontWeight: 'bold' }}>Értékelés időpontja: </span>
                            {clickedGrade?.date ?? ''}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">
                            <span style={{ fontWeight: 'bold' }}>Értékelés típusa: </span>
                            {scoring.get(clickedGrade?.scoring) ?? ''}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">
                            <span style={{ fontWeight: 'bold' }}>Értékelés: </span>
                            {clickedGrade?.grade ?? '-'}
                        </Typography>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)}>Vissza</Button>
            </DialogActions>
        </Dialog>
    );
}
