import { Box, Paper, Typography } from '@mui/material';
import ResultTable from './ResultTable';
import AvarageTable from './AvarageTable';

export default function Result() {
    return (
        <Box sx={{ width: '100%', overflow: 'auto' }}>
            <Paper sx={{ padding: '20px', margin: '1px' }}>
                <Typography component="h1" variant="h5" sx={{ marginBottom: '10px' }}>
                    Eredmények
                </Typography>
                <ResultTable />
            </Paper>
            <Paper
                sx={{
                    padding: '20px',
                    margin: '20px 1px 1px 1px',
                    maxWidth: '500px',
                    mx: 'left',
                    borderRadius: '10px',
                }}
            >
                <Typography
                    variant="inherit"
                    sx={{
                        marginBottom: '10px',
                        fontSize: '21px',
                        fontFamily: '"Roboto","Helvetica","Arial", sans-serif',
                    }}
                >
                    Átlagok
                </Typography>
                <AvarageTable />
            </Paper>
        </Box>
    );
}
