export const months = [
    'Január',
    'Február',
    'Március',
    'Április',
    'Május',
    'Június',
    'Július',
    'Augusztus',
    'Szeptember',
    'Október',
    'November',
    'December',
];

export const scoring = new Map();
scoring.set('grade', 'osztályzat');
scoring.set('done', 'beadás');
scoring.set('percentage', 'százalékos');

export const columns = (firstMonth: number, lastMonth: number) => {
    const monthNames = [];
    let i = firstMonth - 1;

    while (i != lastMonth) {
        if (i === 12) {
            i = 1;
        } else {
            i++;
        }
        monthNames.push(months[i - 1]);
    }
    const monthColumns = monthNames.map((element, index) => ({ name: index, label: element }));
    return [
        {
            name: 'course',
            label: 'Kurzus',
            width: '200px',
            align: 'center',
        },
        ...monthColumns.map((element) => ({ ...element, width: '100px' })),
    ];
};
