import { Table, TableBody, TableCell, TableContainer, TableRow, useTheme } from '@mui/material';
import { IAvgs } from './types';
import { useEffect, useState } from 'react';
import instance from '../../../api/tools';

export default function AvarageTable() {
    const theme = useTheme();
    const [data, setData] = useState<IAvgs>();

    const tableHeaderTheme = { backgroundColor: `${theme.palette.primary.main}`, color: '#f6f6f6', fontWeight: 'bold' };
    const tableHeaderHorizontalTheme = {
        ...tableHeaderTheme,
        width: '50%',
    };

    useEffect(() => {
        instance.get('students/avarages').then((response) => {
            setData(response.data);
        });
    }, []);

    return (
        <TableContainer>
            <Table sx={{ maxWidth: '500px', border: `1px solid #dddd` }} aria-labelledby="tableTitle" size={'medium'}>
                <TableBody>
                    <TableRow hover>
                        <TableCell sx={tableHeaderHorizontalTheme}>Vizsgák átlaga</TableCell>
                        <TableCell>{data?.examAvg ?? '-' + ' %'}</TableCell>
                    </TableRow>
                    <TableRow hover>
                        <TableCell sx={tableHeaderHorizontalTheme}>Kötelező feladatok átlaga</TableCell>
                        <TableCell>{data?.mandatoryAvg ?? '-' + ' %'}</TableCell>
                    </TableRow>
                    <TableRow hover>
                        <TableCell sx={tableHeaderHorizontalTheme}>Opcionális feladat elkészítve</TableCell>
                        <TableCell>{data?.optionalsDone ?? '-' + ' db'}</TableCell>
                    </TableRow>
                    <TableRow hover>
                        <TableCell sx={tableHeaderHorizontalTheme}>Opcionális feladatok átlaga</TableCell>
                        <TableCell>{data?.optionalsDone != 0 ? data?.optionalsAvg ?? '-' + ' %' : '-'}</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
}
