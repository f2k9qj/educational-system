module.exports = {
    parser: '@typescript-eslint/parser',
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:react-hooks/recommended',
        'prettier',
        'plugin:prettier/recommended',
    ],
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        'react/react-in-jsx-scope': 'off',
        complexity: ['error', 15],
        'max-lines-per-function': ['error', 200],
        'react/jsx-max-depth': ['error', { max: 6 }],
        'react/prop-types': 'off',
        'no-restricted-imports': [
            'error',
            {
                patterns: ['@mui/*/*/*', '!@mui/material/test-utils/*'],
            },
        ],
        'react-hooks/exhaustive-deps': 'error',
        'react/jsx-key': 'error',
        '@typescript-eslint/no-unused-vars': [
            'error',
            { argsIgnorePattern: '^_', destructuredArrayIgnorePattern: '^_' },
        ], // Nem használt változók..
        '@typescript-eslint/ban-ts-comment': 'error',
        '@typescript-eslint/ban-types': 'error',
        '@typescript-eslint/no-explicit-any': 'error',
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
    env: {
        jest: true,
        node: true,
    },
};
