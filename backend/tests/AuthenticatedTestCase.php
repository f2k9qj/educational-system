<?php

namespace App\Tests;

use App\Entity\Student;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\RouterInterface;

class AuthenticatedTestCase extends WebTestCase
{
    protected function createAuthenticatedClient(string $username = 'test', string $password = 'pass123', ?array $roles = [], ?bool $student = false)
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneBy(["username" => $username]);

        if (!$user) {
            $client->request('POST', $this->getPath("register"), content: json_encode([
                "email" => $username . "@test.com",
                "username" => $username,
                "password" => $password,
                "repassword" => $password,
            ]));


            if (!empty($roles)) {
                $user = $userRepository->findOneBy(["username" => $username]);
                $user->setRoles($roles);
                $userRepository->save($user);

                if ($student) {
                    $studentRepository = static::getContainer()->get(StudentRepository::class);
                    $student = new Student();
                    $student->setUser($user);
                    $studentRepository->save($student);
                }
            }
        }

        $client->request(
            'POST',
            '/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'username' => $username,
                'password' => $password,
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    protected function getPath(string $routeName): string
    {
        $router = static::getContainer()->get(RouterInterface::class);
        $route = $router->getRouteCollection()->get($routeName);
        $path = $route->getPath();

        return $path;
    }
}