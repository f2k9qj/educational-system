<?php

namespace App\Tests\Student;

use App\Entity\Course;
use App\Entity\Room;
use App\Repository\CourseRepository;
use App\Repository\RoomRepository;
use App\Repository\SemesterRepository;
use App\Repository\UserRepository;
use App\Tests\AuthenticatedTestCase;
use App\Tests\TestUtils;
use App\Utils\UserRoles;

class StudentTest extends AuthenticatedTestCase
{
    public function testProfile_Details(): void
    {
        $client = $this->createAuthenticatedClient('testStudent', 'pass123', [UserRoles::ROLE_USER, UserRoles::ROLE_STUDENT], true);
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByUsername('testStudent');

        $this->assertNotNull($user);

        if ($user) {
            $client->request('GET', $this->getPath("student_profile"));
            $this->assertResponseIsSuccessful();
            $profile = json_decode($client->getResponse()->getContent(), true);
            $profile["lastName"] = "Test";
            $profile["firstName"] = "User";
            $profile["motherLastName"] = "Saint";
            $profile["motherFirstName"] = "John";
            $profile["studentCardNumber"] = "11111111";
            $profile["idCardNumber"] = "2222222";
            $profile["birthDate"] = "2002-03-04";
            $profile["birthPlace"] = "Szeged";

            $client->loginUser($user);
            $client->request('POST', $this->getPath("student_profile_edit"), content: json_encode($profile));
            $this->assertResponseIsSuccessful();

            $client->request('GET', $this->getPath("student_profile"));
            $profileData = json_decode($client->getResponse()->getContent(), true);

            $birthDate = str_replace(['. ', '.'], ['-', ''], $profileData["birthDate"]);

            $this->assertSame($profile["email"], $profileData["email"]);
            $this->assertSame($profile["username"], $profileData["username"]);
            $this->assertSame($profile["lastName"], $profileData["lastName"]);
            $this->assertSame($profile["motherLastName"], $profileData["motherLastName"]);
            $this->assertSame($profile["motherFirstName"], $profileData["motherFirstName"]);
            $this->assertSame($profile["studentCardNumber"], $profileData["studentCardNumber"]);
            $this->assertSame($profile["idCardNumber"], $profileData["idCardNumber"]);
            $this->assertSame($profile["birthDate"], $birthDate);
            $this->assertSame($profile["birthPlace"], $profileData["birthPlace"]);
        }
    }

    public function testCourseEnrollment_ListEnroll(): void
    {
        $client = $this->createAuthenticatedClient('testStudent', 'pass123', [UserRoles::ROLE_USER, UserRoles::ROLE_STUDENT], true);

        $userRepository = static::getContainer()->get(UserRepository::class);
        $roomRepository = static::getContainer()->get(RoomRepository::class);
        $courseRepository = static::getContainer()->get(CourseRepository::class);
        $semesterRepository = static::getContainer()->get(SemesterRepository::class);


        $room = new Room();
        $room->setName('TestRoomForCourseEnrollment');
        $roomRepository->save($room);

        $currentSemester = TestUtils::haveCurrentSemester($semesterRepository);
        $instructor = TestUtils::haveInstructorUser($userRepository);

        $course = new Course();
        $course->setName('TestCourseEnrollment');
        $course->setCourseCode('CT-2');
        $course->setLocation($room);
        $course->setMinHeadCount(1);
        $course->setMaxHeadCount(10);
        $course->addInstructor($instructor);

        $course->setSemester($currentSemester);
        $courseRepository->save($course);

        $client->request('GET', '/students/courses');
        $this->assertResponseIsSuccessful();
        $courses = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($courses);

        $client->request('POST', '/students/courses/' . reset($courses)['id'] . '/enroll');
        $this->assertResponseIsSuccessful();
    }
}