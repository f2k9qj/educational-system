<?php

namespace App\Tests\Admin;

use App\Entity\Notification;
use App\Entity\Room;
use App\Repository\RoomRepository;
use App\Repository\UserRepository;
use App\Service\NotificationService;
use App\Tests\AuthenticatedTestCase;
use App\Tests\TestConsts;
use App\Utils\UserRoles;

class ProfileTest extends AuthenticatedTestCase
{
    public function testProfile_Register(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $client->request('POST', $this->getPath("register"), content: json_encode(TestConsts::REGISTER_REQUEST));

        $this->assertResponseIsSuccessful();

        $user = $userRepository->findOneBy(["email" => TestConsts::REGISTER_REQUEST["email"]]);

        $this->assertNotNull($user);

        if ($user) {
            $this->assertSame(TestConsts::REGISTER_REQUEST["username"], $user->getUserName());
        }
    }

    public function testProfile_Details(): void
    {
        $client = $this->createAuthenticatedClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $users = $userRepository->findAll();

        $this->assertTrue(!empty($users));

        if (!empty($users)) {
            $user = $users[0];

            $client->request('GET', "/profile");
            $this->assertResponseIsSuccessful();
            $profile = json_decode($client->getResponse()->getContent(), true);
            $profile["lastName"] = "Test";
            $profile["firstName"] = "User";

            $client->loginUser($user);
            $client->request('POST', $this->getPath("profile_edit"), content: json_encode($profile));
            $this->assertResponseIsSuccessful();

            $client->request('GET', "/profile");
            $profileData = json_decode($client->getResponse()->getContent(), true);

            $this->assertSame($profile["email"], $profileData["email"]);
            $this->assertSame($profile["username"], $profileData["username"]);
            $this->assertSame($profile["lastName"], $profileData["lastName"]);
            $this->assertSame($profile["firstName"], $profileData["firstName"]);
        }
    }

    public function testNotifications_List(): void
    {
        $client = $this->createAuthenticatedClient('testAdmin', 'pass123', [UserRoles::ROLE_USER, UserRoles::ROLE_ADMINISTRATOR]);

        $notificationService = static::getContainer()->get(NotificationService::class);
        $userRepository = static::getContainer()->get(UserRepository::class);
        $roomRepository = static::getContainer()->get(RoomRepository::class);

        $room = new Room();
        $room->setName('TestNotificationRoom');

        $roomRepository->save($room);

        $notificationService->create(
            Notification::NOTIFICATION_ROOM,
            [
                'name' => $room->getName(),
            ],
            $userRepository->getAdminUsers(),
        );

        $client->request('GET', "/notifications/list");
        $this->assertResponseIsSuccessful();

        $notifications = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEmpty($notifications);
    }
}