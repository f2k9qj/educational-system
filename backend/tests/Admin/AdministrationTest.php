<?php

namespace App\Tests\Admin;

use App\Entity\Exam;
use App\Entity\Submission;
use App\Repository\AssessmentRepository;
use App\Repository\CourseRepository;
use App\Repository\ExamRepository;
use App\Repository\SemesterRepository;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use App\Tests\AuthenticatedTestCase;
use App\Tests\TestConsts;
use App\Tests\TestUtils;

class AdministrationTest extends AuthenticatedTestCase
{
    public function testExam_CreateList(): void
    {
        $client = $this->createAuthenticatedClient();

        $courseRepository = static::getContainer()->get(CourseRepository::class);
        $semesterRepository = static::getContainer()->get(SemesterRepository::class);
        $userRepository = static::getContainer()->get(UserRepository::class);

        $actualCourse = TestUtils::haveActualCourse($courseRepository, $userRepository, $semesterRepository);

        $client->request('POST', '/exams/add', content: json_encode(TestConsts::examRequest($actualCourse->getId())));
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/exams/course/' . $actualCourse->getId() . '/list');
        $this->assertResponseIsSuccessful();

        $exams = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($exams);
    }

    public function testTask_CreateList(): void
    {
        $client = $this->createAuthenticatedClient();

        $courseRepository = static::getContainer()->get(CourseRepository::class);
        $semesterRepository = static::getContainer()->get(SemesterRepository::class);
        $userRepository = static::getContainer()->get(UserRepository::class);

        $actualCourse = TestUtils::haveActualCourse($courseRepository, $userRepository, $semesterRepository);

        $client->request('POST', '/tasks/add', content: json_encode(TestConsts::taskRequest($actualCourse->getId())));
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/tasks/course/' . $actualCourse->getId() . '/list');
        $this->assertResponseIsSuccessful();

        $exams = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($exams);
    }

    public function testAssessment_Make(): void
    {
        $client = $this->createAuthenticatedClient();

        $entityManager = static::getContainer()->get('doctrine')->getManager();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $studentRepository = static::getContainer()->get(StudentRepository::class);
        $semesterRepository = static::getContainer()->get(SemesterRepository::class);
        $courseRepository = static::getContainer()->get(CourseRepository::class);
        $examRepository = static::getContainer()->get(ExamRepository::class);
        $assessmentRepository = static::getContainer()->get(AssessmentRepository::class);

        $actualCourse = TestUtils::haveActualCourse($courseRepository, $userRepository, $semesterRepository);
        $student = TestUtils::haveStudent($userRepository, $studentRepository);

        // Enroll student on course
        $actualCourse->addStudent($student);
        $courseRepository->save($actualCourse);

        // Create a submission
        $exam = new Exam();
        $exam->setName("Assessment Exam");
        $exam->setCourse($actualCourse);
        $exam->setScoring(Submission::SCORE_PERCENTAGE);
        $examRepository->save($exam);

        $client->request('GET', '/assessments/student/' . $student->getId() . '/course/' . $actualCourse->getId() . '/list');
        $this->assertResponseIsSuccessful();

        $submissions = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($submissions);

        $assessment = $assessmentRepository->findOneBy(['student' => $student, 'exam' => $exam]);
        $client->request(
            'POST',
            '/assessments/' . $assessment->getId(),
            content: json_encode(["grade" => "100"])
        );
        $this->assertResponseIsSuccessful();
        $entityManager->refresh($assessment);

        $this->assertEquals($assessment->getGrade(), '100');
        $this->assertNotNull($assessment->getGradedAt());
    }
}