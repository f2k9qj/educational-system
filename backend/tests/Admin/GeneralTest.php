<?php

namespace App\Tests\Admin;

use App\Entity\Room;
use App\Entity\Student;
use App\Entity\User;
use App\Repository\RoomRepository;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use App\Tests\AuthenticatedTestCase;
use App\Tests\TestConsts;
use App\Tests\TestUtils;
use App\Utils\UserRoles;

class GeneralTest extends AuthenticatedTestCase
{
    public function testUsers_List(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', $this->getPath("users_list"));

        $this->assertResponseIsSuccessful();

        $users = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotEmpty($users);
    }

    public function testStudent_Add(): void
    {
        $client = $this->createAuthenticatedClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $studentRepository = static::getContainer()->get(StudentRepository::class);

        $studentEmail = "student@test.com";
        $client->request('POST', '/students/add', content: json_encode(["email" => $studentEmail]));
        $this->assertResponseIsSuccessful();

        $testUser = $userRepository->findOneByEmail($studentEmail);
        $testStudent = $studentRepository->findOneBy(['user' => $testUser, 'registered' => false]);

        $this->assertNotNull($testUser);
        $this->assertNotNull($testStudent);
        $this->assertContains(UserRoles::ROLE_STUDENT, $testUser->getRoles());
    }

    public function testStudent_Register(): void
    {
        $client = $this->createAuthenticatedClient();

        $entityManager = static::getContainer()->get('doctrine')->getManager();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $studentRepository = static::getContainer()->get(StudentRepository::class);

        $studentEmail = "student2@test.com";
        $client->request('POST', '/students/add', content: json_encode(["email" => $studentEmail]));
        $user = $userRepository->findOneByEmail($studentEmail);
        $student = $studentRepository->findOneBy(["user" => $user, "registered" => false]);
        $client->request('POST', '/students/activate', content: json_encode(TestConsts::studentActivation($student->getId())));
        $this->assertResponseIsSuccessful();

        $entityManager->refresh($student);
        $this->assertEquals($student->getRegistered(), true);
    }

    public function testStudent_List(): void
    {
        $client = $this->createAuthenticatedClient();

        $studentRepository = static::getContainer()->get(StudentRepository::class);
        $user = new User();
        $student = new Student();
        $user->setEmail("student3@test.com");
        $student->setRegistered(true);
        $student->setUser($user);
        $studentRepository->save($student);

        $client->request('GET', '/students/list');

        $students = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEmpty($students);
    }

    public function testSemester_AddList(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('POST', '/semesters/add', content: json_encode(TestConsts::SEMESTER_REQUEST));
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/semesters/list');
        $this->assertResponseIsSuccessful();

        $semesters = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEmpty($semesters);
    }

    public function testRoom_AddList(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('POST', '/rooms/add', content: json_encode(TestConsts::ROOM_REQUEST));
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/rooms/list');
        $this->assertResponseIsSuccessful();

        $rooms = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEmpty($rooms);
    }

    public function testCourse_AddList(): void
    {
        $client = $this->createAuthenticatedClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $roomRepository = static::getContainer()->get(RoomRepository::class);

        $instructor = TestUtils::haveInstructorUser($userRepository);

        $room = new Room();
        $room->setName('TestRoomForCourse');

        $userRepository->save($instructor);
        $roomRepository->save($room);

        $client->request(
            'POST',
            '/courses/add',
            content: json_encode(TestConsts::courseRequest((string) $room->getId(), (string) $instructor->getId()))
        );
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/courses/list');
        $this->assertResponseIsSuccessful();

        $courses = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEmpty($courses);
    }
}