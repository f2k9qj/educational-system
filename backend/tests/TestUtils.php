<?php

namespace App\Tests;

use App\Entity\Course;
use App\Entity\Semester;
use App\Entity\Student;
use App\Entity\User;
use App\Repository\CourseRepository;
use App\Repository\SemesterRepository;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use App\Utils\UserRoles;
use DateTime;

class TestUtils
{
    public static function haveCurrentSemester(SemesterRepository $semesterRepository): Semester
    {
        $currentSemester = $semesterRepository->findCurrentSemester();

        if (!$currentSemester) {
            $currentSemester = new Semester();
            $currentSemester->setName('Test Semester');
            $start = (new DateTime())->modify('-1 month');
            $end = (new DateTime())->modify("+1 month");
            $currentSemester->setStartDate($start);
            $currentSemester->setEndDate($end);
            $currentSemester->setEnrollmentStartDate($start);
            $currentSemester->setEnrollmentEndDate($end);
            $semesterRepository->save($currentSemester);
        }

        return $currentSemester;
    }

    public static function haveInstructorUser(UserRepository $userRepository): User
    {
        $instructor = $userRepository->findOneByEmail("teacher@test.com");

        if (!$instructor) {
            $instructor = new User();
            $instructor->setEmail("teacher@test.com");
        }

        $instructor->setRoles([UserRoles::ROLE_USER, UserRoles::ROLE_INSTRUCTOR]);
        $userRepository->save($instructor);

        return $instructor;
    }

    public static function haveActualCourse(
        CourseRepository $courseRepository,
        UserRepository $userRepository,
        SemesterRepository $semesterRepository
    ): Course {
        $course = $courseRepository->findOneByCourseCode('AC-1');

        if (!$course) {
            $instructor = self::haveInstructorUser($userRepository);
            $currentSemester = self::haveCurrentSemester($semesterRepository);
            $course = new Course();
            $course->setName('Actual Course For Testing');
            $course->setCourseCode('AC-1');
            $course->setMinHeadCount(1);
            $course->setMaxHeadCount(10);
            $course->addInstructor($instructor);
            $course->setSemester($currentSemester);
            $courseRepository->save($course);
        }

        return $course;
    }

    public static function haveStudent(UserRepository $userRepository, StudentRepository $studentRepository): Student
    {
        $studentUser = $userRepository->findOneByUsername('student');

        if (!$studentUser) {
            $studentUser = new User();
            $studentUser->setUsername('utilStudent');
            $studentUser->setEmail('student@util.com');
            $studentUser->setRoles([UserRoles::ROLE_USER, UserRoles::ROLE_STUDENT]);
            $userRepository->save($studentUser);

            $student = new Student();
            $student->setUser($studentUser);
            $studentRepository->save($student);
        } else {
            $student = $studentRepository->findOneByUser($studentUser);
        }

        return $student;
    }
}