<?php

namespace App\Tests;

use App\Entity\Submission;
use App\Entity\Task;

class TestConsts
{
    public const REGISTER_REQUEST = [
        "email" => "register@test.hu",
        "username" => "register",
        "password" => "pass123",
        "repassword" => "pass123",
    ];

    public static function studentActivation(int $studentId)
    {
        return [
            "studentId" => $studentId,
            "username" => "student",
            "password" => "test123!",
            "lastName" => "Kecskés",
            "firstName" => "Lilla",
            "motherLastName" => "Semjén",
            "motherFirstName" => "Margit",
            "studentCardNumber" => "2565241",
            "idCardNumber" => "2565242",
            "birthDate" => "2000-04-04",
            "birthPlace" => "Budapest"
        ];
    }

    public const SEMESTER_REQUEST = [
        "name" => "SemesterTest",
        "startDate" => "2000-01-01",
        "endDate" => "2030-12-30",
        "enrollmentStartDate" => "2000-01-01",
        "enrollmentEndDate" => "2030-12-30",
    ];

    public const ROOM_REQUEST = [
        "name" => "TestRoom",
        "postCode" => "3453",
        "city" => "Test",
        "streetName" => "Test",
        "streetType" => "test",
        "houseNumber" => "11"
    ];

    public static function courseRequest(string $room, string $instructors)
    {
        return [
            "name" => "CourseTest",
            "courseCode" => "CT-1",
            "location" => $room,
            "minHeadCount" => "10",
            "maxHeadCount" => "20",
            "maxAbsence" => "1",
            "instructors" => $instructors
        ];
    }

    public static function examRequest(int $courseId)
    {
        return [
            "course" => (string) $courseId,
            "name" => "Test Exam",
            "description" => "This is an automated test exam",
            "scoring" => Submission::SCORE_PERCENTAGE,
            "startDate" => "2023-12-15T10:00",
            "endDate" => "2023-12-15T12:00"
        ];
    }

    public static function taskRequest(int $courseId)
    {
        return [
            "course" => (string) $courseId,
            "name" => "Test Task",
            "description" => "This is an automated test task",
            "type" => Task::TYPE_MANDATORY,
            "scoring" => Submission::SCORE_PERCENTAGE,
            "deadline" => "2023-12-15T10:00"
        ];
    }

}