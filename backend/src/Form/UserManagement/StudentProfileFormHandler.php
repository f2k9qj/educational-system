<?php

declare(strict_types=1);

namespace App\Form\UserManagement;

use App\Repository\StudentRepository;
use DateTime;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class StudentProfileFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly StudentRepository $studentRepository
    ) {
    }

    public function __invoke(StudentProfileForm $form)
    {
        $student = $this->studentRepository->findOneById($form->studentId);
        $user = $student?->getUser();

        $user->setEmail($form->email);
        $user->setUsername($form->username);
        $user->setLastName($form->lastName);
        $user->setFirstName($form->firstName);

        $birthdate = $form->birthDate ? DateTime::createFromFormat('Y-m-d', $form->birthDate) : null;

        $student->setMotherLastName($form->motherLastName);
        $student->setMotherFirstName($form->motherFirstName);
        $student->setStudentCardNumber($form->studentCardNumber);
        $student->setIdCardNumber($form->idCardNumber);
        $student->setBirthDate($birthdate);
        $student->setBirthPlace($form->birthPlace);

        $this->studentRepository->save($student);
    }
}