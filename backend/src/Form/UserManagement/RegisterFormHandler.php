<?php

declare(strict_types=1);

namespace App\Form\UserManagement;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\UserRoles;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    public function __invoke(RegisterForm $form)
    {
        $user = $this->userRepository->findOneBy(['username' => $form->username, 'email' => $form->email]);

        if ($user) {
            throw new Exception("Létezik már ilyen felhasználó az adatbázisban.");
        }

        $user = new User();
        $user->setEmail($form->email);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $form->password
        );
        $user->setRoles([UserRoles::ROLE_USER]);
        $user->setPassword($hashedPassword);
        $user->setUsername($form->username);

        $this->userRepository->save($user);
    }
}