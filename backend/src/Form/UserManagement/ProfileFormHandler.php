<?php

declare(strict_types=1);

namespace App\Form\UserManagement;

use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProfileFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function __invoke(ProfileForm $form)
    {
        $user = $this->userRepository->findOneById($form->id);

        $user->setEmail($form->email);
        $user->setUsername($form->username);
        $user->setLastName($form->lastName);
        $user->setFirstName($form->firstName);

        $this->userRepository->save($user);
    }
}