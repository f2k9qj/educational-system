<?php

namespace App\Form\UserManagement;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $email = null;

    #[Assert\NotBlank]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $password = null;

    #[Assert\NotBlank]
    public ?string $repassword = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->email = $content['email'] ?? '';
        $form->username = $content['username'] ?? '';
        $form->password = $content['password'] ?? '';
        $form->repassword = $content['repassword'] ?? '';

        return $form;
    }
}