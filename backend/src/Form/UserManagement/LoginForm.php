<?php

namespace App\Form\UserManagement;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class LoginForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $password = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->username = $content['username'] ?? '';
        $form->password = $content['password'] ?? '';

        return $form;
    }
}