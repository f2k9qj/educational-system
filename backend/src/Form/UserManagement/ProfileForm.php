<?php

namespace App\Form\UserManagement;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ProfileForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?int $id = null;

    #[Assert\NotBlank]
    public ?string $email = null;

    #[Assert\NotBlank]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $lastName = null;

    #[Assert\NotBlank]
    public ?string $firstName = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->id = $content['id'];
        $form->email = $content['email'] ?? '';
        $form->username = $content['username'] ?? '';
        $form->lastName = $content['lastName'] ?? '';
        $form->firstName = $content['firstName'] ?? '';

        return $form;
    }
}