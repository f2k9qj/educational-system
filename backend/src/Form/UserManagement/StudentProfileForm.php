<?php

namespace App\Form\UserManagement;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class StudentProfileForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?int $id = null;

    #[Assert\NotBlank]
    public ?string $email = null;

    #[Assert\NotBlank]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $lastName = null;

    #[Assert\NotBlank]
    public ?string $firstName = null;

    #[Assert\NotBlank]
    public ?int $studentId = null;

    public ?string $motherLastName = null;

    public ?string $motherFirstName = null;

    public ?string $studentCardNumber = null;

    public ?string $idCardNumber = null;

    public ?string $birthDate = null;

    public ?string $birthPlace = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->id = $content['id'];
        $form->email = $content['email'] ?? '';
        $form->username = $content['username'] ?? '';
        $form->lastName = $content['lastName'] ?? '';
        $form->firstName = $content['firstName'] ?? '';

        $form->studentId = $content['studentId'];
        $form->motherLastName = $content['motherLastName'] ?? '';
        $form->motherFirstName = $content['motherFirstName'] ?? '';
        $form->studentCardNumber = $content['studentCardNumber'] ?? '';
        $form->idCardNumber = $content['idCardNumber'] ?? '';
        $form->birthDate = $content['birthDate'] ?? '';
        $form->birthPlace = $content['birthPlace'] ?? '';

        return $form;
    }
}