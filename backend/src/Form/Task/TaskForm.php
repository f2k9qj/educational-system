<?php

namespace App\Form\Task;

use App\Entity\Submission;
use App\Entity\Task;
use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class TaskForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $course = null;

    #[Assert\NotBlank]
    public ?string $name = null;

    public ?string $description = null;

    #[Assert\NotBlank]
    #[Assert\Choice(Task::TYPES)]
    public ?string $type = null;

    #[Assert\NotBlank]
    #[Assert\Choice(Submission::SCORING)]
    public ?string $scoring = null;

    #[Assert\NotBlank]
    public ?string $deadline = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->course = $content['course'] ?? '';
        $form->name = $content['name'] ?? '';
        $form->description = $content['description'] ?? '';
        $form->type = $content['type'] ?? '';
        $form->scoring = $content['scoring'] ?? '';
        $form->deadline = $content['deadline'] ?? '';

        return $form;
    }
}