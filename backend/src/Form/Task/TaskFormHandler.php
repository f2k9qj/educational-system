<?php

declare(strict_types=1);

namespace App\Form\Task;

use App\Entity\Notification;
use App\Entity\Task;
use App\Repository\CourseRepository;
use App\Repository\TaskRepository;
use App\Service\NotificationService;
use DateTime;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use UnexpectedValueException;

class TaskFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly CourseRepository $courseRepository,
        private readonly TaskRepository $taskRepository,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function __invoke(TaskForm $form)
    {
        $task = new Task();

        $course = $this->courseRepository->findOneById($form->course);

        if (!$course) {
            throw new UnexpectedValueException('Course could not be found!');
        }

        $task->setCourse($course);
        $task->setName($form->name);
        $task->setDescription($form->description);
        $task->setType($form->type);
        $task->setScoring($form->scoring);
        $task->setDeadline(DateTime::createFromFormat('Y-m-d\TH:i', $form->deadline));

        $this->notificationService->create(
            Notification::NOTIFICATION_TASK,
            [
                'name' => $task->getName(),
                'type' => $this->translator->trans('submissions.types.' . $task->getType()),
                'description' => $task->getDescription(),
                'deadline' => $task->getDeadline()?->format('Y.m.d H:i:s')
            ],
            array_map(fn($element) => $element?->getUser(), $course->getStudents()->toArray()),
        );

        $this->taskRepository->save($task);
    }
}