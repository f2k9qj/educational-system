<?php

namespace App\Form\Course;

use App\Form\FormInterface;
use App\Validator\CourseConstraint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

#[CourseConstraint]
class CourseForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $name = null;

    #[Assert\NotBlank]
    public ?string $courseCode = null;

    #[Assert\NotBlank]
    public ?string $location = null;

    public ?int $minHeadCount = null;

    public ?int $maxHeadCount = null;

    public ?int $maxAbsence = null;

    #[Assert\NotBlank]
    public ?array $instructors = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $instructors = $content['instructors'] != '' ? explode(',', $content['instructors']) : null;

        $form = new self();
        $form->name = $content['name'] ?? '';
        $form->courseCode = $content['courseCode'] ?? '';
        $form->location = $content['location'] ?? '';
        $form->minHeadCount = (int) $content['minHeadCount'] ?? null;
        $form->maxHeadCount = (int) $content['maxHeadCount'] ?? null;
        $form->maxAbsence = (int) $content['maxAbsence'] ?? null;
        $form->instructors = $instructors ?? null;

        return $form;
    }
}