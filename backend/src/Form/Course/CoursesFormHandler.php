<?php

declare(strict_types=1);

namespace App\Form\Course;

use App\Entity\Course;
use App\Entity\Notification;
use App\Form\Course\CourseForm;
use App\Repository\CourseRepository;
use App\Repository\RoomRepository;
use App\Repository\SemesterRepository;
use App\Repository\UserRepository;
use App\Service\NotificationService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CoursesFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly CourseRepository $courseRepository,
        private readonly SemesterRepository $semesterRepository,
        private readonly RoomRepository $roomRepository,
        private readonly UserRepository $userRepository,
        private readonly NotificationService $notificationService,
    ) {
    }

    public function __invoke(CourseForm $form)
    {
        $course = new Course();
        $semester = $this->semesterRepository->findCurrentSemester();
        $room = $this->roomRepository->findOneById($form->location);

        if (!$semester) {
            throw new NotFoundHttpException('Jelenleg nincs aktív szemeszter!');
        }

        if (!$room) {
            throw new NotFoundHttpException('Nincs terem ilyen id-val!');
        }

        $course->setName($form->name);
        $course->setCourseCode($form->courseCode);
        $course->setLocation($room);
        $course->setMinHeadCount($form->minHeadCount);
        $course->setMaxHeadCount($form->maxHeadCount);
        $course->setMaxAbsence($form->maxAbsence);
        $course->setSemester($semester);

        foreach ($form->instructors as $instructor) {
            $user = $this->userRepository->findOneBy(["id" => $instructor]);
            if ($user) {
                $course->addInstructor($user);
            }
        }

        $this->notificationService->create(
            Notification::NOTIFICATION_COURSE,
            [
                'name' => $course->getName(),
                'courseCode' => $course->getCourseCode(),
            ],
            $this->userRepository->getAdminUsers(),
        );

        $this->courseRepository->save($course);
    }
}