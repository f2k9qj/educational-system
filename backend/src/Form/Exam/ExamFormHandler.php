<?php

declare(strict_types=1);

namespace App\Form\Exam;

use App\Entity\Exam;
use App\Entity\Notification;
use App\Form\Exam\ExamForm;
use App\Repository\CourseRepository;
use App\Repository\ExamRepository;
use App\Service\NotificationService;
use DateTime;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use UnexpectedValueException;

class ExamFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly CourseRepository $courseRepository,
        private readonly ExamRepository $examRepository,
        private readonly NotificationService $notificationService,
    ) {
    }

    public function __invoke(ExamForm $form)
    {
        $exam = new Exam();

        $course = $this->courseRepository->findOneById($form->course);

        if (!$course) {
            throw new UnexpectedValueException('Course could not be found!');
        }

        $exam->setCourse($course);
        $exam->setName($form->name);
        $exam->setDescription($form->description);
        $exam->setScoring($form->scoring);
        $exam->setStartDate(DateTime::createFromFormat('Y-m-d\TH:i', $form->startDate));
        $exam->setEndDate(DateTime::createFromFormat('Y-m-d\TH:i', $form->endDate));

        $this->notificationService->create(
            Notification::NOTIFICATION_EXAM,
            [
                'name' => $exam->getName(),
                'description' => $exam->getDescription(),
                'start' => $exam->getStartDate()?->format('Y.m.d H:i:s'),
                'end' => $exam->getEndDate()?->format('Y.m.d H:i:s'),
            ],
            array_map(fn($element) => $element?->getUser(), $course->getStudents()->toArray()),
        );

        $this->examRepository->save($exam);
    }
}