<?php

namespace App\Form\Exam;

use App\Entity\Submission;
use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ExamForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $course = null;

    #[Assert\NotBlank]
    public ?string $name = null;

    public ?string $description = null;

    #[Assert\NotBlank]
    #[Assert\Choice(Submission::SCORING)]
    public ?string $scoring = null;

    #[Assert\NotBlank]
    public ?string $startDate = null;

    #[Assert\NotBlank]
    public ?string $endDate = null;


    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->course = $content['course'] ?? '';
        $form->name = $content['name'] ?? '';
        $form->description = $content['description'] ?? '';
        $form->scoring = $content['scoring'] ?? '';
        $form->startDate = $content['startDate'] ?? '';
        $form->endDate = $content['endDate'] ?? '';

        return $form;
    }
}