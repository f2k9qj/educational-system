<?php

namespace App\Form\Student;

use App\Form\FormInterface;
use App\Validator\UsernameConstraint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class StudentActivationForm implements FormInterface
{
    public int $studentId;

    #[Assert\NotBlank]
    #[UsernameConstraint]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $password = null;

    #[Assert\NotBlank]
    public ?string $lastName = null;

    #[Assert\NotBlank]
    public ?string $firstName = null;

    public ?string $motherLastName = null;

    public ?string $motherFirstName = null;

    public ?string $studentCardNumber = null;

    public ?string $idCardNumber = null;

    public ?string $birthDate = null;

    public ?string $birthPlace = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->studentId = $content['studentId'] ?? '';
        $form->username = $content['username'] ?? '';
        $form->password = $content['password'] ?? '';
        $form->lastName = $content['lastName'] ?? '';
        $form->firstName = $content['firstName'] ?? '';
        $form->motherLastName = $content['motherLastName'] ?? '';
        $form->motherFirstName = $content['motherFirstName'] ?? '';
        $form->studentCardNumber = $content['studentCardNumber'] ?? '';
        $form->idCardNumber = $content['idCardNumber'] ?? '';
        $form->birthDate = $content['birthDate'] ?? '';
        $form->birthPlace = $content['birthPlace'] ?? '';

        return $form;
    }
}