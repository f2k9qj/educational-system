<?php

declare(strict_types=1);

namespace App\Form\Student;

use DateTime;
use App\Repository\StudentRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class StudentActivationFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly StudentRepository $studentRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    public function __invoke(StudentActivationForm $form)
    {
        $student = $this->studentRepository->findOneBy(['id' => $form->studentId]);
        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ilyen id-val!');
        }
        $user = $student->getUser();
        $user->setUsername($form->username);
        $user->setLastName($form->lastName);
        $user->setFirstName($form->firstName);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $form->password
        );
        $user->setPassword($hashedPassword);
        $student->setMotherLastName($form->motherLastName);
        $student->setMotherFirstName($form->motherFirstName);
        $student->setStudentCardNumber($form->studentCardNumber);
        $student->setIdCardNumber($form->idCardNumber);
        $student->setBirthDate($form->birthDate ? DateTime::createFromFormat('Y-m-d', $form->birthDate) : null);
        $student->setBirthPlace($form->birthPlace);
        $student->setRegistered(true);

        $this->studentRepository->save($student);
    }
}