<?php

namespace App\Form\Student;

use App\Form\FormInterface;
use App\Validator\EmailConstraint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class StudentAddForm implements FormInterface
{
    #[Assert\NotBlank]
    #[Assert\Email(
        message: 'Az email {{ value }} nem egy helyes email cím!',
    )]
    #[EmailConstraint]
    public ?string $email = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->email = $content['email'] ?? '';

        return $form;
    }
}