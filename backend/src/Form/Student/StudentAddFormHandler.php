<?php

declare(strict_types=1);

namespace App\Form\Student;

use App\Entity\Student;
use App\Entity\User;
use App\Form\Student\StudentAddForm;
use App\Repository\StudentRepository;
use App\Utils\UserRoles;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class StudentAddFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly StudentRepository $studentRepository,
    ) {
    }

    public function __invoke(StudentAddForm $form)
    {
        $student = new Student();
        $user = new User();

        $user->setEmail($form->email);
        $user->setRoles([UserRoles::ROLE_USER, UserRoles::ROLE_STUDENT]);
        $student->setUser($user);
        $student->setRegistered(false);

        $this->studentRepository->save($student);
    }
}