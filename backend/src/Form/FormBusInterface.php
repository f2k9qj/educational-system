<?php

declare(strict_types=1);

namespace App\Form;

interface FormBusInterface
{
    public function handle(FormInterface $form);
}