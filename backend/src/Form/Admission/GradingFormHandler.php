<?php

declare(strict_types=1);

namespace App\Form\Admission;

use App\Entity\Notification;
use App\Repository\AssessmentRepository;
use App\Service\NotificationService;
use DateTime;
use DateTimeZone;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GradingFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly AssessmentRepository $assessmentRepository,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function __invoke(GradingForm $form)
    {
        $assessment = $this->assessmentRepository->findOneById($form->id);

        $now = new DateTime();
        $now->setTimezone(new DateTimeZone("Europe/Budapest"));
        $assessment->setGrade($form->grade);
        $assessment->setGradedAt($now);

        $submission = null;
        if ($assessment->getTask()) {
            $submission = $assessment->getTask();
        } else if ($assessment->getExam()) {
            $submission = $assessment->getExam();
        }
        $course = $submission?->getCourse()?->getName();
        $gradedAt = $assessment->getGradedAt()->format('Y.m.d H:i:s');
        $scoring = $this->translator->trans('submissions.scoring.' . $submission->getScoring());

        $this->notificationService->create(
            Notification::NOTIFICATION_GRADED,
            ['course' => $course, 'gradedAt' => $gradedAt, 'grade' => $assessment->getGrade(), 'scoring' => $scoring],
            [$assessment->getStudent()->getUser()]
        );

        $this->assessmentRepository->save($assessment);
    }
}