<?php

namespace App\Form\Admission;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class GradingForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $id = null;

    #[Assert\NotBlank]
    public ?string $grade = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->id = $request->get('assessmentId') ?? '';
        $form->grade = $content['grade'] ?? '';

        return $form;
    }
}