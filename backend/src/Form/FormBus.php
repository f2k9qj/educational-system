<?php

declare(strict_types=1);

namespace App\Form;

use App\Form\FormInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class FormBus implements FormBusInterface
{
    public function __construct(
        private readonly MessageBusInterface $bus
    ) {
    }

    public function handle(FormInterface $form)
    {
        try {
            $envelope = $this->bus->dispatch($form);
        } catch (HandlerFailedException $exception) {
            $exception = $exception->getPrevious();

            throw $exception;
        }

        $handledStamp = $envelope->last(HandledStamp::class);

        if ($handledStamp instanceof HandledStamp) {
            return $handledStamp->getResult();
        }

        return null;
    }
}