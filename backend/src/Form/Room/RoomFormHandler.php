<?php

declare(strict_types=1);

namespace App\Form\Room;

use App\Entity\Notification;
use App\Entity\Room;
use App\Repository\RoomRepository;
use App\Repository\UserRepository;
use App\Service\NotificationService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RoomFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly RoomRepository $roomRepository,
        private readonly UserRepository $userRepository,
        private readonly NotificationService $notificationService,
    ) {
    }

    public function __invoke(RoomForm $form)
    {
        $room = new Room();

        $room->setName($form->name);
        $room->setPostCode($form->postCode);
        $room->setCity($form->city);
        $room->setStreetName($form->streetName);
        $room->setStreetType($form->streetType);
        $room->setHouseNumber($form->houseNumber);

        $this->notificationService->create(
            Notification::NOTIFICATION_ROOM,
            [
                'name' => $room->getName(),
            ],
            $this->userRepository->getAdminUsers(),
        );

        $this->roomRepository->save($room);
    }
}