<?php

namespace App\Form\Room;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class RoomForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $name = null;

    #[Assert\NotBlank]
    public ?string $postCode = null;

    #[Assert\NotBlank]
    public ?string $city = null;

    #[Assert\NotBlank]
    public ?string $streetName = null;

    #[Assert\NotBlank]
    public ?string $streetType = null;

    #[Assert\NotBlank]
    public ?string $houseNumber = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->name = $content['name'] ?? '';
        $form->postCode = $content['postCode'] ?? '';
        $form->city = $content['city'] ?? '';
        $form->streetName = $content['streetName'] ?? '';
        $form->streetType = $content['streetType'] ?? '';
        $form->houseNumber = $content['houseNumber'] ?? '';

        return $form;
    }
}