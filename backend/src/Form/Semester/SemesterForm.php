<?php

namespace App\Form\Semester;

use App\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class SemesterForm implements FormInterface
{
    #[Assert\NotBlank]
    public ?string $name = null;

    #[Assert\NotBlank]
    public ?string $startDate = null;

    #[Assert\NotBlank]
    public ?string $endDate = null;

    #[Assert\NotBlank]
    public ?string $enrollmentStartDate = null;

    #[Assert\NotBlank]
    public ?string $enrollmentEndDate = null;

    public static function createFromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        $form = new self();
        $form->name = $content['name'] ?? '';
        $form->startDate = $content['startDate'] ?? '';
        $form->endDate = $content['endDate'] ?? '';
        $form->enrollmentStartDate = $content['enrollmentStartDate'] ?? '';
        $form->enrollmentEndDate = $content['enrollmentEndDate'] ?? '';

        return $form;
    }
}