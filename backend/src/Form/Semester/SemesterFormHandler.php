<?php

declare(strict_types=1);

namespace App\Form\Semester;

use App\Entity\Notification;
use App\Entity\Semester;
use App\Repository\SemesterRepository;
use App\Repository\UserRepository;
use App\Service\NotificationService;
use DateTime;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SemesterFormHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly SemesterRepository $semesterRepository,
        private readonly NotificationService $notificationService,
        private readonly UserRepository $userRepository,
    ) {
    }

    public function __invoke(SemesterForm $form)
    {
        $semester = new Semester();

        $semester->setName($form->name);
        $semester->setStartDate(DateTime::createFromFormat('Y-m-d', $form->startDate));
        $semester->setEndDate(DateTime::createFromFormat('Y-m-d', $form->endDate));
        $semester->setEnrollmentStartDate(DateTime::createFromFormat('Y-m-d', $form->enrollmentStartDate));
        $semester->setEnrollmentEndDate(DateTime::createFromFormat('Y-m-d', $form->enrollmentEndDate));

        $this->notificationService->create(
            Notification::NOTIFICATION_SEMESTER,
            [
                'name' => $semester->getName(),
                'startDate' => $semester->getStartDate()?->format('Y.m.d H:i:s'),
                'endDate' => $semester->getEndDate()?->format('Y.m.d H:i:s'),
                'enrollmentStartDate' => $semester->getEnrollmentStartDate()?->format('Y.m.d H:i:s'),
                'enrollmentEndDate' => $semester->getEnrollmentEndDate()?->format('Y.m.d H:i:s'),
            ],
            $this->userRepository->getAdminUsers(),
        );

        $this->semesterRepository->save($semester);
    }
}