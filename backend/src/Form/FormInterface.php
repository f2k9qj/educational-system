<?php

namespace App\Form;

use Symfony\Component\HttpFoundation\Request;

interface FormInterface
{
    public static function createFromRequest(Request $request): self;
}