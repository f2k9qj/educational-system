<?php

namespace App\Controller;

use App\Entity\Submission;
use App\Entity\Task;
use App\Form\FormBusInterface;
use App\Form\Task\TaskForm;
use App\Repository\CourseRepository;
use App\Repository\TaskRepository;
use App\Utils\ControllerUtils;
use App\Views\OptionView;
use App\Views\TaskView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/tasks', name: 'tasks_')]
class TaskController extends AbstractController
{
    public function __construct(
        private readonly CourseRepository $courseRepository,
        private readonly TaskRepository $taskRepository,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/course-options', name: 'course_options', methods: ['GET'])]
    public function courseOptions(Request $request): Response
    {
        $courses = $this->courseRepository->findAll();
        $courseOptions = array_map(function ($element) {
            return [
                "label" => $element->getName(),
                "value" => $element->getId(),
            ];
        }, $courses);

        return $this->json(OptionView::createList($courseOptions));
    }

    #[Route('/type-options', name: 'type_options', methods: ['GET'])]
    public function typeOptions(Request $request): Response
    {
        $typeOptions = array_map(function ($element) {
            return [
                "label" => $this->translator->trans('submissions.types.' . $element),
                "value" => $element,
            ];
        }, Task::TYPES);

        return $this->json(OptionView::createList($typeOptions));
    }

    #[Route('/scoring-options', name: 'scoring_options', methods: ['GET'])]
    public function scoringOptions(Request $request): Response
    {
        $scoringOptions = array_map(function ($element) {
            return [
                "label" => $this->translator->trans('submissions.scoring.' . $element),
                "value" => $element,
            ];
        }, Submission::SCORING);

        return $this->json(OptionView::createList($scoringOptions));
    }

    #[Route('/course/{courseId}/list', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        $courseId = $request->get('courseId');
        $course = $this->courseRepository->findOneById($courseId);

        if (!$course) {
            throw new NotFoundHttpException('Nem található kurzus');
        }

        $tasks = $this->taskRepository->findBy(['course' => $course]);

        return $this->json(TaskView::createList($tasks, [], $this->translator));
    }

    #[Route('/add', name: 'add', methods: ['POST'])]
    public function add(Request $request)
    {
        $form = TaskForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Task added");
    }
}