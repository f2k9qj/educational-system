<?php

namespace App\Controller;

use App\Form\FormBusInterface;
use App\Form\Course\CourseForm;
use App\Repository\CourseRepository;
use App\Repository\RoomRepository;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use App\Views\CourseStudentsView;
use App\Views\CourseView;
use App\Views\OptionView;
use App\Utils\ControllerUtils;
use App\Utils\UserRoles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/courses', name: 'courses_')]
class CourseController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly RoomRepository $roomRepository,
        private readonly StudentRepository $studentRepository,
        private readonly CourseRepository $courseRepository,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        $courses = $this->courseRepository->findAll();

        return $this->json(CourseView::createList($courses));
    }

    #[Route('/add', name: 'add', methods: ['POST'])]
    public function add(Request $request)
    {
        $form = CourseForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Course added");
    }

    #[Route('/instructor-options', name: 'instructor_options', methods: ['GET'])]
    public function instructorOptions(Request $request): Response
    {
        $instructors = $this->userRepository->getUsersByRole(UserRoles::ROLE_INSTRUCTOR);
        $instructorOptions = array_map(function ($element) {
            $firstName = $element->getFirstName();
            $lastName = $element->getLastName();
            $label = $firstName && $lastName ? $lastName . ' ' . $firstName : $element->getEmail();
            return [
                "label" => $label,
                "value" => $element->getId(),
            ];
        }, $instructors);

        return $this->json(OptionView::createList($instructorOptions));
    }

    #[Route('/room-options', name: 'room_options', methods: ['GET'])]
    public function roomOptions(Request $request): Response
    {
        $rooms = $this->roomRepository->findAll();
        $roomOptions = array_map(function ($element) {
            return [
                "label" => $element->getName(),
                "value" => $element->getId(),
            ];
        }, $rooms);

        return $this->json(OptionView::createList($roomOptions));
    }

    #[Route('/{courseId}/students', name: 'students_list', methods: ['GET'])]
    public function courseStudents(Request $request): Response
    {
        $courseId = (int) $request->get('courseId');
        $course = $this->courseRepository->findOneById($courseId);

        if (!$course) {
            throw new NotFoundHttpException('Nincs ilyen azonosítóval kurzus!');
        }

        return $this->json(CourseStudentsView::createList($course->getStudents()->toArray()));
    }


    #[Route('/{courseId}/students', name: 'students_add', methods: ['POST'])]
    public function addCourseStudent(Request $request): Response
    {
        $courseId = (int) $request->get('courseId');
        $course = $this->courseRepository->findOneById($courseId);

        if (!$course) {
            throw new NotFoundHttpException('Nincs ilyen azonosítóval kurzus!');
        }

        $content = json_decode($request->getContent(), true);
        $studentId = $content['student'] ?? null;
        $student = $studentId ? $this->studentRepository->findOneById($studentId) : null;

        if (!$student) {
            throw new NotFoundHttpException('Nincs megadva hallgató, vagy a hallgató nem található!');
        }

        $course->addStudent($student);
        $this->courseRepository->save($course);

        return $this->json(CourseStudentsView::createList($course->getStudents()->toArray()));
    }
}