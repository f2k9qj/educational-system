<?php

namespace App\Controller;

use App\Form\Admission\GradingForm;
use App\Form\FormBusInterface;
use App\Form\Task\TaskForm;
use App\Repository\AssessmentRepository;
use App\Repository\CourseRepository;
use App\Repository\StudentRepository;
use App\Service\AssessmentService;
use App\Utils\ControllerUtils;
use App\Views\AssessmentView;
use App\Views\OptionView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/assessments', name: 'assessments_')]
class AssessmentController extends AbstractController
{
    public function __construct(
        private readonly StudentRepository $studentRepository,
        private readonly CourseRepository $courseRepository,
        private readonly AssessmentRepository $assessmentRepository,
        private readonly AssessmentService $assessmentService,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/student-options', name: 'student_options', methods: ['GET'])]
    public function studentOptions(Request $request): Response
    {
        $students = $this->studentRepository->findForCourses();
        $studentOptions = array_map(function ($element) {
            return [
                "label" => $element->getUser()?->getFullName(),
                "value" => $element->getId(),
            ];
        }, $students);

        return $this->json(OptionView::createList($studentOptions));
    }

    #[Route('/course-options', name: 'course_options', methods: ['GET'])]
    public function courseOptions(Request $request): Response
    {
        $studentId = $request->query->get('student');
        $courses = $this->courseRepository->findCoursesForStudent($studentId);
        $courseOptions = array_map(function ($element) {
            return [
                "label" => $element->getName(),
                "value" => $element->getId(),
            ];
        }, $courses);

        return $this->json(OptionView::createList($courseOptions));
    }

    #[Route('/student/{studentId}/course/{courseId}/list', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        $studentId = $request->get('studentId');
        $student = $this->studentRepository->findOneById($studentId);
        $courseId = $request->get('courseId');
        $course = $this->courseRepository->findOneById($courseId);

        if (!$student || !$course) {
            throw new NotFoundHttpException('Nem található hallgató vagy kurzus');
        }

        $assessments = $this->assessmentService->createAssessments($student, $course);

        return $this->json(AssessmentView::createList($assessments, [], $this->translator));
    }

    #[Route('/{assessmentId}', name: 'grade', methods: ['POST'])]
    public function grade(Request $request): Response
    {
        $form = GradingForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Course added");
    }
}