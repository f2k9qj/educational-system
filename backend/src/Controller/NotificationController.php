<?php

namespace App\Controller;

use App\Repository\NotificationRepository;
use App\Views\NotificationView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/notifications', name: 'notifications_')]
class NotificationController extends AbstractController
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            throw new NotFoundHttpException('Nem található felhasználó!');
        }

        $notifications = $this->notificationRepository->getNotifications($user);

        return $this->json(NotificationView::createList($notifications, [], $this->translator));
    }
}