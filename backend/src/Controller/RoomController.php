<?php

namespace App\Controller;

use App\Form\FormBusInterface;
use App\Form\Room\RoomForm;
use App\Repository\RoomRepository;
use App\Utils\ControllerUtils;
use App\Views\RoomView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/rooms', name: 'rooms_')]
class RoomController extends AbstractController
{
    public function __construct(
        private readonly RoomRepository $roomRepository,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request)
    {
        $rooms = $this->roomRepository->findAll();

        return $this->json(RoomView::createList($rooms));
    }

    #[Route('/add', name: 'add', methods: ['POST'])]
    public function add(Request $request)
    {
        $form = RoomForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Semester added");
    }
}