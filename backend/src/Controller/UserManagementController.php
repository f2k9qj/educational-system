<?php

namespace App\Controller;

use App\Form\FormBusInterface;
use App\Form\UserManagement\ProfileForm;
use App\Form\UserManagement\RegisterForm;
use App\Form\UserManagement\StudentProfileForm;
use App\Repository\StudentRepository;
use App\Repository\UserRepository;
use App\Utils\UserRoles;
use App\Views\ProfileView;
use App\Views\StudentProfileView;
use App\Utils\ControllerUtils;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserManagementController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly StudentRepository $studentRepository,
        private readonly FormBusInterface $bus,
        private readonly JWTTokenManagerInterface $jwtManager,
        private readonly TranslatorInterface $translator,
        private readonly ValidatorInterface $validator,
    ) {
    }

    #[Route('/register', name: 'register', methods: ['POST'])]
    public function register(Request $request)
    {
        $form = RegisterForm::createFromRequest($request);
        
        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Registered");
    }


    #[Route('/roles', name: 'roles', methods: ['GET'])]
    public function roles(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->json([]);
        }

        return $this->json($user->getRoles());
    }

    #[Route('/profile', name: 'profile', methods: ['GET'])]
    public function profile(Request $request): Response
    {
        $user = $this->getUser();

        $view = ProfileView::create($user, [], $this->translator);

        return $this->json($view);
    }

    #[Route('/profile/edit', name: 'profile_edit', methods: ['POST'])]
    public function profileEdit(Request $request)
    {
        $form = ProfileForm::createFromRequest($request);

        $this->bus->handle($form);

        return $this->json("Profile edited");
    }


    #[Route('/profile/student', name: 'student_profile', methods: ['GET'])]
    public function studentProfile(Request $request): Response
    {
        $user = $this->getUser();
        $roles = $user->getRoles();

        if (!in_array(UserRoles::ROLE_STUDENT, $roles)) {
            throw new AccessDeniedException('Hallgatói jogosultság szükséges ehhez a művelethez!');
        }

        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $view = StudentProfileView::create($student, [], $this->translator);

        return $this->json($view);
    }

    #[Route('/profile/student/edit', name: 'student_profile_edit', methods: ['POST'])]
    public function studentProfileEdit(Request $request)
    {
        $form = StudentProfileForm::createFromRequest($request);

        $this->bus->handle($form);

        return $this->json("Profile edited");
    }
}