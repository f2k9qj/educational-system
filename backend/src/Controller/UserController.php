<?php

namespace App\Controller;

use App\Form\FormBusInterface;
use App\Repository\UserRepository;
use App\Utils\UserRoles;
use App\Views\OptionView;
use App\Views\UserView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/users', name: 'users_')]
class UserController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly TranslatorInterface $translator,
        private readonly FormBusInterface $bus,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request): Response
    {
        $semesters = $this->userRepository->findAll();

        return $this->json(UserView::createList($semesters, null, $this->translator));
    }

    #[Route('/roles-options', name: 'roles_options', methods: ['GET'])]
    public function rolesOptions(Request $request): Response
    {
        $roles = UserRoles::ROLES;
        $rolesOptions = array_map(function ($element) {
            return [
                "label" => $this->translator->trans('roles.' . UserRoles::TRANSLATION_MAP[$element]),
                "value" => $element,
            ];
        }, $roles);

        return $this->json(OptionView::createList($rolesOptions));
    }

    #[Route('/{userId}/roles', name: 'roles', methods: ['GET'])]
    public function userRoles(Request $request): Response
    {
        $userId = (int) $request->attributes->get('userId');
        $user = $this->userRepository->findOneById($userId);

        if (!$user) {
            throw new NotFoundHttpException("User not found in the database!");
        }

        return $this->json($user->getRoles());
    }

    #[Route('/{userId}/edit', name: 'edit', methods: ['POST'])]
    public function edit(Request $request): Response
    {
        $userId = (int) $request->attributes->get('userId');
        $user = $this->userRepository->findOneById($userId);

        if (!$user) {
            throw new NotFoundHttpException("User not found in the database!");
        }

        $content = json_decode($request->getContent(), true);
        $user->setRoles($content['roles']);
        $this->userRepository->save($user);

        return $this->json("Ok");
    }
}