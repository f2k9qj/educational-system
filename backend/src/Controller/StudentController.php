<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Form\FormBusInterface;
use App\Form\Student\StudentActivationForm;
use App\Form\Student\StudentAddForm;
use App\Repository\AssessmentRepository;
use App\Repository\CourseRepository;
use App\Repository\ExamRepository;
use App\Repository\SemesterRepository;
use App\Repository\StudentRepository;
use App\Repository\TaskRepository;
use App\Service\NotificationService;
use App\Utils\ControllerUtils;
use App\Views\EnrollmentCourseView;
use App\Views\StudentAvarageView;
use App\Views\StudentCalendarEventView;
use App\Views\StudentResultView;
use App\Views\StudentView;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/students', name: 'students_')]
class StudentController extends AbstractController
{
    public function __construct(
        private readonly AssessmentRepository $assessmentRepository,
        private readonly StudentRepository $studentRepository,
        private readonly CourseRepository $courseRepository,
        private readonly SemesterRepository $semesterRepository,
        private readonly ExamRepository $examRepository,
        private readonly TaskRepository $taskRepository,
        private readonly NotificationService $notificationService,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request)
    {
        $students = $this->studentRepository->findRegistered();

        return $this->json(StudentView::createList($students));
    }

    #[Route('/add', name: 'add', methods: ['POST'])]
    public function add(Request $request)
    {
        $form = StudentAddForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Student added");
    }

    #[Route('/email-check', name: 'email_check', methods: ['POST'])]
    public function emailCheck(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        if (!array_key_exists('email', $content) || !$content['email']) {
            return new JsonResponse(["violations" => ["email" => "Nincs megadva email cím!"]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $content['email'];
        $student = $this->studentRepository->findOneForFirstLogin($email);

        if (!$student) {
            return new JsonResponse(["violations" => ["email" => "Ezzel az email címmel nem aktiválhatod a hallgatót!"]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->json(["status" => "success", "student" => $student->getId()]);
    }

    #[Route('/activate', name: 'activate', methods: ['POST'])]
    public function activate(Request $request)
    {
        $form = StudentActivationForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Student activated");
    }

    #[Route('/courses', name: 'courses', methods: ['GET'])]
    public function courses(Request $request)
    {
        $user = $this->getUser();
        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $currentSemester = $this->semesterRepository->findCurrentSemester();
        if (!$currentSemester) {
            return $this->json([]);
        }

        $courses = $this->courseRepository->findCoursesForSemester($currentSemester);

        return $this->json(EnrollmentCourseView::createList($courses, ['student' => $student]));
    }

    #[Route('/courses/{courseId}/enroll', name: 'course_enroll', methods: ['POST'])]
    public function courseEnroll(Request $request)
    {
        $courseId = $request->get('courseId');
        $course = $this->courseRepository->findOneById($courseId);

        if (!$course) {
            throw new NotFoundHttpException('Nem található kurzus ehhez az azonosítóhoz');
        }

        $user = $this->getUser();
        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $semester = $course->getSemester();
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone("Europe/Budapest"));

        if ($semester->getEnrollmentStartDate() > $now || $semester->getEnrollmentEndDate() < $now) {
            return $this->json(['status' => 'failed', 'message' => 'Jelenleg nincs felvételi időszak!']);
        }

        $studentsCount = $course->getStudents()->count();
        $maxCount = $course->getMaxHeadCount();

        if ($studentsCount < $maxCount) {
            $course->addStudent($student);
            $this->courseRepository->save($course);

            $this->notificationService->create(
                Notification::NOTIFICATION_ENROLLED,
                ['course' => $course?->getName()],
                [$student->getUser()]
            );

            return $this->json(['status' => 'enrolled']);
        }

        return $this->json(['status' => 'failed', 'message' => 'A kurzus szabad helyei beteltek!']);
    }

    #[Route('/events', name: 'events', methods: ['GET'])]
    public function events(Request $request): Response
    {
        $user = $this->getUser();
        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $exams = $this->examRepository->findByStudent($student);
        $tasks = $this->taskRepository->findByStudent($student);

        $list = array_merge($exams, $tasks);

        return $this->json(StudentCalendarEventView::createList($list));
    }

    #[Route('/results', name: 'results', methods: ['GET'])]
    public function results(Request $request): Response
    {
        $user = $this->getUser();
        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $currentSemester = $this->semesterRepository->findCurrentSemester();
        $assessments = $this->assessmentRepository->findStudentYearlyGrades($student, $currentSemester);

        return $this->json(StudentResultView::create($assessments, ['semester' => $currentSemester]));
    }

    #[Route('/avarages', name: 'avarages', methods: ['GET'])]
    public function avarages(Request $request): Response
    {
        $user = $this->getUser();
        $student = $this->studentRepository->findOneBy(['user' => $user]);

        if (!$student) {
            throw new NotFoundHttpException('Nem található hallgató ehhez a felhasználóhoz!');
        }

        $currentSemester = $this->semesterRepository->findCurrentSemester();
        $assessments = $this->assessmentRepository->findStudentYearlyGrades($student, $currentSemester);

        return $this->json(StudentAvarageView::create($assessments));
    }
}