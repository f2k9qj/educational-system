<?php

namespace App\Controller;

use App\Form\FormBusInterface;
use App\Form\Semester\SemesterForm;
use App\Repository\SemesterRepository;
use App\Utils\ControllerUtils;
use App\Views\SemesterView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/semesters', name: 'semesters_')]
class SemesterController extends AbstractController
{
    public function __construct(
        private readonly SemesterRepository $semesterRepository,
        private readonly ValidatorInterface $validator,
        private readonly FormBusInterface $bus,
    ) {
    }

    #[Route('/list', name: 'list', methods: ['GET'])]
    public function list(Request $request)
    {
        $semesters = $this->semesterRepository->findAll();

        return $this->json(SemesterView::createList($semesters));
    }

    #[Route('/add', name: 'add', methods: ['POST'])]
    public function add(Request $request)
    {
        $form = SemesterForm::createFromRequest($request);

        $errors = $this->validator->validate($form);
        $violations = ControllerUtils::makeViolations($errors);

        if (!empty($violations)) {
            return new JsonResponse(["violations" => $violations], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->bus->handle($form);

        return $this->json("Semester added");
    }
}