<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\UserRoles;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateSuperadminCommand extends Command
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:superadmin:create')
            ->addArgument('username', InputArgument::REQUIRED, 'Username')
            ->addArgument('password', InputArgument::REQUIRED, 'Email address')
            ->setDescription('Szuperadmin felhasználó létrehozására szolgáló parancs');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $user = $this->userRepository->findOneBy(['username' => $username]);

        if ($user) {
            throw new Exception("Létezik már ilyen felhasználó az adatbázisban.");
        }

        $user = new User();
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $password
        );
        $user->setRoles([UserRoles::ROLE_USER, UserRoles::ROLE_SUPERADMIN]);
        $user->setPassword($hashedPassword);
        $user->setUsername($username);

        $this->userRepository->save($user);

        return Command::SUCCESS;
    }
}
