<?php

namespace App\Service;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use DateTime;
use DateTimeZone;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class NotificationService
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly string $projectDir,
    ) {
    }

    public function create(string $type, ?array $tags = [], ?array $targets = []): void
    {
        $notificationsDir = $this->projectDir . "/notifications/" . $type;

        $settingFile = $notificationsDir . "/setting.yaml";
        $messageFile = $notificationsDir . "/message.html";

        if (!file_exists($settingFile) || !file_exists($messageFile)) {
            throw new NotFoundResourceException('Nem található fájl az értesítéshez!');
        }

        $settingData = json_decode(file_get_contents($settingFile), true);
        $messageData = file_get_contents($messageFile);
        $replacedMessageData = $this->replaceTags($messageData, $tags);

        foreach ($targets as $target) {
            $notification = new Notification();
            $notification->setTitle($settingData['title']);
            $notification->setMessage($replacedMessageData);
            $notification->setType($type);
            $notification->setTarget($target);
            $now = new DateTime();
            $now->setTimezone(new DateTimeZone("Europe/Budapest"));
            $notification->setCreatedAt($now);
            $this->notificationRepository->save($notification);
        }
    }

    private function replaceTags(string $message, array $tags): string
    {
        foreach ($tags as $tag => $replacement) {
            $message = str_replace('{{ ' . $tag . ' }}', $replacement, $message);
        }

        return $message;
    }
}