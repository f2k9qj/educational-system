<?php

namespace App\Service;

use App\Entity\Assessment;
use App\Entity\Course;
use App\Entity\Student;
use App\Repository\AssessmentRepository;
use App\Repository\ExamRepository;
use App\Repository\TaskRepository;

class AssessmentService
{
    public function __construct(
        private readonly AssessmentRepository $assessmentRepository,
        private readonly ExamRepository $examRepository,
        private readonly TaskRepository $taskRepository,
    ) {
    }

    public function createAssessments(Student $student, Course $course): array
    {
        $assessments = [];
        $exams = $this->examRepository->findByStudentAndCourse($student, $course);
        $tasks = $this->taskRepository->findByStudentAndCourse($student, $course);

        foreach ($exams as $exam) {
            $assessment = $this->assessmentRepository->findOneBy(['student' => $student, 'exam' => $exam]);
            if (!$assessment) {
                $assessment = new Assessment();
                $assessment->setStudent($student);
                $assessment->setExam($exam);
                $this->assessmentRepository->save($assessment);
            }
            $assessments[] = $assessment;
        }

        foreach ($tasks as $task) {
            $assessment = $this->assessmentRepository->findOneBy(['student' => $student, 'task' => $task]);
            if (!$assessment) {
                $assessment = new Assessment();
                $assessment->setStudent($student);
                $assessment->setTask($task);
                $this->assessmentRepository->save($assessment);
            }
            $assessments[] = $assessment;
        }

        return $assessments;
    }
}