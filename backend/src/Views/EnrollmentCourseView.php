<?php

namespace App\Views;

use App\Entity\Course;
use Symfony\Contracts\Translation\TranslatorInterface;

class EnrollmentCourseView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $courseCode = null;
    public ?string $location = null;
    public ?string $instructors = null;
    public ?bool $enrolled = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Course $element) use ($params) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getName();
            $self->courseCode = $element->getCourseCode();
            $self->location = $element->getLocation()?->getName();
            $instuctorNames = array_map(
                function ($e) {
                    $firstName = $e->getFirstName();
                    $lastName = $e->getLastName();
                    $label = $firstName && $lastName ? $lastName . ' ' . $firstName : $e->getEmail();
                    return $label;
                },
                $element->getInstructors()?->toArray()
            );
            $self->instructors = implode(', ', $instuctorNames);
            $enrolled = $element->getStudents()?->contains($params['student']);
            $self->enrolled = $enrolled;
            return $self;
        }, $list);
    }
}