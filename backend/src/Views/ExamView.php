<?php

namespace App\Views;

use App\Entity\Exam;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExamView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $description = null;
    public ?string $scoring = null;
    public ?string $startDate = null;
    public ?string $endDate = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Exam $element) use ($translator) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getName();
            $self->description = $element->getDescription();
            $self->scoring = $translator->trans('submissions.scoring.' . $element->getScoring());
            $self->startDate = $element->getStartDate()?->format('Y.m.d H:i:s');
            $self->endDate = $element->getEndDate()?->format('Y.m.d H:i:s');
            return $self;
        }, $list);
    }
}