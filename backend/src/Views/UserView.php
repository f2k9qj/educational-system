<?php

namespace App\Views;

use App\Entity\User;
use App\Utils\UserRoles;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $email = null;
    public ?string $username = null;
    public ?string $lastName = null;
    public ?string $firstName = null;
    public ?string $roles = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (User $element) use ($translator) {
            $self = new self();
            $self->id = $element->getId();
            $self->email = $element->getEmail();
            $self->username = $element->getUsername();
            $self->lastName = $element->getLastName();
            $self->firstName = $element->getFirstName();
            $self->roles = implode(', ', array_map(fn($e) => $translator->trans('roles.' . UserRoles::TRANSLATION_MAP[$e]), $element->getRoles()));
            return $self;
        }, $list);
    }
}