<?php

namespace App\Views;

use App\Entity\Assessment;
use App\Entity\Submission;
use App\Entity\Task;
use Symfony\Contracts\Translation\TranslatorInterface;

class StudentAvarageView implements ViewInterface
{
    public float $examAvg = 0.0;
    public float $mandatoryAvg = 0.0;
    public int $optionalsDone = 0;
    public float $optionalsAvg = 0.0;

    public static function create(array $list, ?array $params = [], ?TranslatorInterface $translator = null): StudentAvarageView
    {
        $self = new self();
        $examsCount = 0;
        $allExamPercent = 0;
        $mandatoryCount = 0;
        $mandatoryPercent = 0;
        $optionalPercent = 0;

        /** @var Assessment $value */
        foreach ($list as $value) {
            if ($value->getExam()) {
                $examsCount++;
                $allExamPercent += self::getPercentage($value->getExam(), $value->getGrade());
            } else if ($value->getTask()) {
                self::taskProcessor($self, $value, $mandatoryCount, $mandatoryPercent, $optionalPercent);
            }
        }

        $self->examAvg = $examsCount ? round($allExamPercent / $examsCount, 2) : 0.0;
        $self->mandatoryAvg = $mandatoryCount ? round($mandatoryPercent / $mandatoryCount, 2) : 0.0;
        $self->optionalsAvg = $self->optionalsDone ? round($optionalPercent / $self->optionalsDone, 2) : 0.0;

        return $self;
    }

    private static function getPercentage(Submission $submission, mixed $grade): int
    {
        switch ($submission->getScoring()) {
            case Submission::SCORE_PERCENTAGE:
                return (int) $grade;
            case Submission::SCORE_GADE:
                return (int) $grade * 20;
            case Submission::SCORE_DONE:
                return (int) $grade * 100;
        }
        return 0;
    }

    private static function taskProcessor(
        StudentAvarageView &$self,
        Assessment $assessment,
        int &$mandatoryCount,
        int &$mandatoryPercent,
        int &$optionalPercent,
    ): void {
        $task = $assessment->getTask();
        if ($task->getType() === Task::TYPE_MANDATORY) {
            $mandatoryCount++;
            $mandatoryPercent += self::getPercentage($task, $assessment->getGrade());
        } else {
            $self->optionalsDone++;
            $optionalPercent += self::getPercentage($task, $assessment->getGrade());
        }
    }
}