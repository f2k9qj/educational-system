<?php

namespace App\Views;

use Symfony\Contracts\Translation\TranslatorInterface;

interface ViewInterface
{
    public static function create(array $list, ?array $params = [], ?TranslatorInterface $translator = null): ViewInterface;
}