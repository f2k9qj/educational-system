<?php

namespace App\Views;

use Symfony\Contracts\Translation\TranslatorInterface;

interface ViewListInterface
{
    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array;
}