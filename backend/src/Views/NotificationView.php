<?php

namespace App\Views;

use App\Entity\Notification;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotificationView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $title = null;
    public ?string $message = null;
    public ?string $type = null;
    public ?string $createdAt = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Notification $element) use ($translator) {
            $self = new self();
            $self->id = $element->getId();
            $self->title = $element->getTitle();
            $self->message = $element->getMessage();
            $self->type = $translator->trans('notifications.types.' . $element->getType());
            $self->createdAt = $element->getCreatedAt()->format('Y.m.d H:i:s');
            return $self;
        }, $list);
    }
}