<?php

namespace App\Views;

use App\Entity\Semester;
use Symfony\Contracts\Translation\TranslatorInterface;

class SemesterView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $startDate = null;
    public ?string $endDate = null;
    public ?string $enrollmentStartDate = null;
    public ?string $enrollmentEndDate = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Semester $element) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getName();
            $self->startDate = $element->getStartDate()?->format('Y.m.d H:i:s');
            $self->endDate = $element->getEndDate()?->format('Y.m.d H:i:s');
            $self->enrollmentStartDate = $element->getEnrollmentStartDate()?->format('Y.m.d H:i:s');
            $self->enrollmentEndDate = $element->getEnrollmentEndDate()?->format('Y.m.d H:i:s');
            return $self;
        }, $list);
    }
}