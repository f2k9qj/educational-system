<?php

namespace App\Views;

use App\Entity\Exam;
use App\Entity\Submission;
use App\Entity\Task;
use Symfony\Contracts\Translation\TranslatorInterface;

class StudentCalendarEventView implements ViewListInterface
{
    public ?string $title = null;
    public ?string $description = null;
    public ?string $color = null;
    public ?array $start = null;
    public ?array $end = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        $events = array_map(function (Submission $element) {
            if ($element instanceof Exam) {
                $self = new self();
                $self->title = $element->getCourse()?->getName() . ': ' . $element->getName();
                $self->description = $element->getDescription();
                $self->start = [
                    "year" => (int) $element->getStartDate()->format("Y"),
                    "month" => (int) $element->getStartDate()->format("m") - 1,
                    "day" => (int) $element->getStartDate()->format("d"),
                    "hour" => (int) $element->getStartDate()->format("H"),
                    "minute" => (int) $element->getStartDate()->format("i"),
                    "second" => (int) $element->getStartDate()->format("s"),
                ];
                $self->end = [
                    "year" => (int) $element->getEndDate()->format("Y"),
                    "month" => (int) $element->getEndDate()->format("m") - 1,
                    "day" => (int) $element->getEndDate()->format("d"),
                    "hour" => (int) $element->getEndDate()->format("H"),
                    "minute" => (int) $element->getEndDate()->format("i"),
                    "second" => (int) $element->getEndDate()->format("s"),
                ];
                $self->color = "#c21725";
                return $self;
            } else if ($element instanceof Task) {
                $self = new self();
                $self->title = $element->getCourse()?->getName() . ': ' . $element->getName();
                $self->description = $element->getDescription();
                $self->start = [
                    "year" => (int) $element->getDeadline()->format("Y"),
                    "month" => (int) $element->getDeadline()->format("m") - 1,
                    "day" => (int) $element->getDeadline()->format("d"),
                    "hour" => (int) $element->getDeadline()->format("H"),
                    "minute" => (int) $element->getDeadline()->format("i"),
                    "second" => (int) $element->getDeadline()->format("s"),
                ];
                $self->end = [
                    "year" => (int) $element->getDeadline()->format("Y"),
                    "month" => (int) $element->getDeadline()->format("m") - 1,
                    "day" => (int) $element->getDeadline()->format("d"),
                    "hour" => (int) $element->getDeadline()->format("H"),
                    "minute" => (int) $element->getDeadline()->format("i"),
                    "second" => (int) $element->getDeadline()->format("s"),
                ];
                $self->color = "#1d28a8";
                return $self;
            }

        }, $list);

        return $events;
    }
}