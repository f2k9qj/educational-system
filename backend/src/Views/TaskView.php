<?php

namespace App\Views;

use App\Entity\Task;
use Symfony\Contracts\Translation\TranslatorInterface;

class TaskView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $description = null;
    public ?string $type = null;
    public ?string $scoring = null;
    public ?string $deadline = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Task $element) use ($translator) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getName();
            $self->description = $element->getDescription();
            $self->type = $translator->trans('submissions.types.' . $element->getType());
            $self->scoring = $translator->trans('submissions.scoring.' . $element->getScoring());
            $self->deadline = $element->getDeadline()?->format('Y.m.d H:i:s');
            return $self;
        }, $list);
    }
}