<?php

namespace App\Views;

use App\Entity\Student;
use Symfony\Contracts\Translation\TranslatorInterface;

class StudentView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $username = null;
    public ?string $email = null;
    public ?string $name = null;
    public ?string $motherName = null;
    public ?string $birthPlace = null;
    public ?string $birthDate = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Student $element) {
            $self = new self();
            $self->id = $element->getId();
            $user = $element->getUser();
            $self->email = $user?->getEmail();
            $self->username = $user?->getUsername();
            $self->name = $user?->getLastName() . ' ' . $user?->getFirstName();
            $self->motherName = $element->getMotherLastName() . ' ' . $element->getMotherFirstName();
            $self->birthPlace = $element->getBirthPlace();
            $self->birthDate = $element->getBirthDate()?->format('Y.m.d');
            return $self;
        }, $list);
    }
}