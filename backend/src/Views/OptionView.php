<?php

namespace App\Views;

use App\Entity\Student;
use Symfony\Contracts\Translation\TranslatorInterface;

class OptionView implements ViewListInterface
{
    public string $label;

    public string $value;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (array $element) {
            $self = new self();
            $self->label = $element['label'];
            $self->value = $element['value'];
            return $self;
        }, $list);
    }
}