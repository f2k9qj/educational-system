<?php

namespace App\Views;

use App\Entity\Student;
use Symfony\Contracts\Translation\TranslatorInterface;

class CourseStudentsView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $email = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Student $element) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getUser()?->getLastName() . ' ' . $element->getUser()?->getFirstName();
            $self->email = $element->getUser()?->getEmail();
            return $self;
        }, $list);
    }
}