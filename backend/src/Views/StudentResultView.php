<?php

namespace App\Views;

use App\Entity\Assessment;
use App\Entity\Semester;
use Symfony\Contracts\Translation\TranslatorInterface;

class StudentResultView implements ViewInterface
{
    public ?int $firstMonth = null;
    public ?int $lastMonth = null;
    public ?array $grades = null;

    public static function create(array $list, ?array $params = [], ?TranslatorInterface $translator = null): StudentResultView
    {
        $self = new self();
        /** @var Semester */
        $semester = array_key_exists('semester', $params) ? $params['semester'] : null;
        $self->firstMonth = (int) $semester?->getStartDate()?->format('n');
        $self->lastMonth = (int) $semester?->getEndDate()?->format('n');
        $self->grades = [];
        array_map(function (Assessment $element) use ($self) {
            $date = $element->getGradedAt()?->format('Y. m. d.');
            $month = (int) $element->getGradedAt()?->format('m');
            $done = $element->getTask() ?? $element->getExam();
            $course = $done->getCourse()?->getName();
            $self->grades[$course][] = ['month' => $month, 'date' => $date, 'grade' => $element->getGrade(), 'scoring' => $done?->getScoring()];
        }, $list);
        return $self;
    }
}