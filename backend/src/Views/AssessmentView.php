<?php

namespace App\Views;

use App\Entity\Assessment;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssessmentView implements ViewListInterface
{
    public ?int $id = null;
    public ?int $submissionId = null;
    public ?string $name = null;
    public ?string $description = null;
    public ?string $due = null;
    public ?string $scoringType = null;
    public ?string $scoring = null;
    public ?string $grade = null;
    public ?string $gradedAt = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function ($element) use ($translator) {
            if ($element->getExam()) {
                $self = AssessmentView::createExamListView($self, $element, $translator);
            } else if ($element->getTask()) {
                $self = AssessmentView::createTaskListView($self, $element, $translator);
            }
            return $self;
        }, $list);
    }

    private static function createExamListView(&$self, Assessment $element, ?TranslatorInterface $translator = null): AssessmentView
    {
        $self = new self();
        $exam = $element->getExam();
        $self->id = $element->getId();
        $self->submissionId = $exam->getId();
        $self->name = $exam->getName();
        $self->description = $exam->getDescription();
        $self->scoringType = $exam->getScoring();
        $self->scoring = $translator->trans('submissions.scoring.' . $exam->getScoring());
        $self->grade = $element->getGrade();
        $self->gradedAt = $element->getGradedAt()?->format('Y.m.d H:i:s');
        $self->due = $exam->getStartDate()?->format('Y.m.d H:i:s') . ' - ' . $exam->getEndDate()?->format('Y.m.d H:i:s');
        return $self;
    }

    private static function createTaskListView(&$self, Assessment $element, ?TranslatorInterface $translator = null): AssessmentView
    {
        $self = new self();
        $task = $element->getTask();
        $self->id = $element->getId();
        $self->submissionId = $task->getId();
        $self->name = $task->getName();
        $self->description = $task->getDescription();
        $self->scoringType = $task->getScoring();
        $self->scoring = $translator->trans('submissions.scoring.' . $task->getScoring());
        $self->grade = $element->getGrade();
        $self->gradedAt = $element->getGradedAt()?->format('Y.m.d H:i:s');
        $self->due = $task->getDeadline()?->format('Y.m.d H:i:s');
        return $self;
    }
}