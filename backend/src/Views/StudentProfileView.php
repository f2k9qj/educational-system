<?php

namespace App\Views;

use App\Utils\UserRoles;
use Symfony\Contracts\Translation\TranslatorInterface;

class StudentProfileView implements ViewInterface
{
    public int $id;
    public ?string $email = null;
    public ?string $username = null;
    public ?string $firstName = null;
    public ?string $lastName = null;
    public ?string $roles = null;

    public ?int $studentId;
    public ?string $motherLastName = null;
    public ?string $motherFirstName = null;
    public ?string $studentCardNumber = null;
    public ?string $idCardNumber = null;
    public ?string $birthDate = null;
    public ?string $birthPlace = null;

    public static function create($value, ?array $params = [], ?TranslatorInterface $translator = null): ViewInterface
    {
        $self = new self();
        $user = $value->getUser();

        $self->id = $user->getId();
        $self->email = $user->getEmail();
        $self->username = $user->getUsername();
        $self->lastName = $user->getLastName();
        $self->firstName = $user->getFirstName();
        $self->roles = implode(', ', array_map(fn($e) => $translator->trans('roles.' . UserRoles::TRANSLATION_MAP[$e]), $user->getRoles()));

        $self->studentId = $value->getId();
        $self->motherLastName = $value->getMotherLastName();
        $self->motherFirstName = $value->getMotherFirstName();
        $self->studentCardNumber = $value->getStudentCardNumber();
        $self->idCardNumber = $value->getIdCardNumber();
        $self->birthDate = $value->getBirthDate()?->format('Y. m. d.');
        $self->birthPlace = $value->getBirthPlace();

        return $self;
    }
}