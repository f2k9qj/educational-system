<?php

namespace App\Views;

use App\Utils\UserRoles;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProfileView implements ViewInterface
{
    public int $id;
    public ?string $email = null;
    public ?string $username = null;
    public ?string $firstName = null;
    public ?string $lastName = null;
    public ?string $roles = null;

    public static function create($value, ?array $params = [], ?TranslatorInterface $translator = null): ViewInterface
    {
        $self = new self();

        $self->id = $value->getId();
        $self->email = $value->getEmail();
        $self->username = $value->getUsername();
        $self->lastName = $value->getLastName();
        $self->firstName = $value->getFirstName();
        $self->roles = implode(', ', array_map(fn($e) => $translator->trans('roles.' . UserRoles::TRANSLATION_MAP[$e]), $value->getRoles()));

        return $self;
    }
}