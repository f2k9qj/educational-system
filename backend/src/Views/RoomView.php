<?php

namespace App\Views;

use App\Entity\Room;
use Symfony\Contracts\Translation\TranslatorInterface;

class RoomView implements ViewListInterface
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $postCode = null;
    public ?string $city = null;
    public ?string $streetName = null;
    public ?string $streetType = null;
    public ?string $houseNumber = null;

    public static function createList(array $list, ?array $params = [], ?TranslatorInterface $translator = null): array
    {
        return array_map(function (Room $element) {
            $self = new self();
            $self->id = $element->getId();
            $self->name = $element->getName();
            $self->postCode = $element->getPostCode();
            $self->city = $element->getCity();
            $self->streetName = $element->getStreetName();
            $self->streetType = $element->getStreetType();
            $self->houseNumber = $element->getHouseNumber();
            return $self;
        }, $list);
    }
}