<?php

namespace App\Validator;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UsernameValidator extends ConstraintValidator
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        $user = $this->userRepository->findOneBy(['username' => $value]);
        if ($user) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}