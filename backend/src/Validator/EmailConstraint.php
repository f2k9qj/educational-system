<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 */
#[\Attribute]
class EmailConstraint extends Constraint
{
    public $message = 'Ilyen email címmel már létezik felhasználó a rendszerben!';

    public function validatedBy(): string
    {
        return EmailValidator::class;
    }
}