<?php

namespace App\Validator;

use App\Entity\Semester;
use App\Form\Course\CourseForm;
use App\Repository\CourseRepository;
use App\Repository\SemesterRepository;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CourseValidator extends ConstraintValidator
{
    public function __construct(
        private readonly SemesterRepository $semesterRepository,
        private readonly CourseRepository $courseRepository,
    ) {
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof CourseForm) {
            throw new UnexpectedTypeException($value, CourseForm::class);
        }

        $this->validateName($value, $constraint);
        $this->validateCode($value, $constraint);
    }

    private function validateName($value, Constraint $constraint)
    {
        $semester = $this->semesterRepository->findCurrentSemester();
        $course = $this->courseRepository->findOneBy(["semester" => $semester, "name" => $value->name]);

        if ($course) {
            $this->context->buildViolation($constraint->nameInUse)
                ->atPath('name')
                ->addViolation();
        }
    }

    private function validateCode($value, Constraint $constraint)
    {
        $course = $this->courseRepository->findOneBy(["courseCode" => $value->courseCode]);

        if ($course) {
            $this->context->buildViolation($constraint->codeInUse)
                ->atPath('courseCode')
                ->addViolation();
        }
    }
}