<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 */
#[\Attribute]
class UsernameConstraint extends Constraint
{
    public $message = 'A felhasználónév foglalt!';

    public function validatedBy(): string
    {
        return UsernameValidator::class;
    }
}