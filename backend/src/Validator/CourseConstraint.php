<?php

namespace App\Validator;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute(Attribute::TARGET_CLASS)]
class CourseConstraint extends Constraint
{
    public string $nameInUse = 'Ebben a szemeszterben már van ilyen nevű kurzus!';
    public string $codeInUse = 'Ez a kurzuskód már foglalt!';

    public function validatedBy(): string
    {
        return CourseValidator::class;
    }

    public function getTargets(): array
    {
        return [
            Constraint::CLASS_CONSTRAINT
        ];
    }
}