<?php

namespace App\Utils;

class UserRoles
{
    public const ROLE_SUPERADMIN = "ROLE_SUPERADMIN";
    public const ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";
    public const ROLE_INSTRUCTOR = "ROLE_INSTRUCTOR";
    public const ROLE_STUDENT = "ROLE_STUDENT";
    public const ROLE_USER = "ROLE_USER";

    public const TRANSLATION_MAP = [
        self::ROLE_SUPERADMIN => "superadmin",
        self::ROLE_ADMINISTRATOR => "administrator",
        self::ROLE_INSTRUCTOR => "instructor",
        self::ROLE_STUDENT => "student",
        self::ROLE_USER => "user",
    ];

    public const ROLES = [
        self::ROLE_STUDENT,
        self::ROLE_INSTRUCTOR,
        self::ROLE_ADMINISTRATOR,
        self::ROLE_SUPERADMIN,
    ];
}