<?php

namespace App\Utils;

class ControllerUtils
{
    public static function makeViolations($errors): array
    {
        $violations = [];
        foreach ($errors as $error) {
            $violations[$error->getPropertyPath()] = $error->getMessage();
        }
        return $violations;
    }
}