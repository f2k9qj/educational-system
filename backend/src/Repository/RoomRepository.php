<?php

namespace App\Repository;

use App\Entity\Room;
use App\Entity\Semester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    public function save(Room $course)
    {
        $this->getEntityManager()->persist($course);
        $this->getEntityManager()->flush();
    }
}