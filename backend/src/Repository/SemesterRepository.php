<?php

namespace App\Repository;

use App\Entity\Semester;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SemesterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Semester::class);
    }

    public function save(Semester $semester)
    {
        $this->getEntityManager()->persist($semester);
        $this->getEntityManager()->flush();
    }

    public function findCurrentSemester(): ?Semester
    {
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone("Europe/Budapest"));
        return $this->createQueryBuilder('semester')
            ->andWhere('semester.startDate <= :now')
            ->andWhere('semester.endDate >= :now')
            ->setParameter('now', $now)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}