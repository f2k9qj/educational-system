<?php

namespace App\Repository;

use App\Entity\User;
use App\Utils\UserRoles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function getUsersByRole(string $role)
    {
        return $this->createQueryBuilder('user')
            ->andWhere("user.roles LIKE CONCAT('%', :role, '%')")
            ->setParameter('role', $role)
            ->getQuery()
            ->getResult();
    }

    public function getAdminUsers()
    {
        return $this->createQueryBuilder('user')
            ->orWhere("user.roles LIKE CONCAT('%', :superadmin, '%')")
            ->orWhere("user.roles LIKE CONCAT('%', :admin, '%')")
            ->orWhere("user.roles LIKE CONCAT('%', :instructor, '%')")
            ->setParameters(
                new ArrayCollection(
                    [
                        new Parameter('superadmin', UserRoles::ROLE_SUPERADMIN),
                        new Parameter('admin', UserRoles::ROLE_ADMINISTRATOR),
                        new Parameter('instructor', UserRoles::ROLE_INSTRUCTOR),
                    ]
                )
            )
            ->getQuery()
            ->getResult();
    }
}