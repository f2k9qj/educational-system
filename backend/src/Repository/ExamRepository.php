<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Exam;
use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ExamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exam::class);
    }

    public function save(Exam $exam)
    {
        $this->getEntityManager()->persist($exam);
        $this->getEntityManager()->flush();
    }

    public function findByStudentAndCourse(Student $student, Course $course)
    {
        return $this->createQueryBuilder("exam")
            ->leftJoin("exam.course", "course")
            ->leftJoin("course.students", "student")
            ->andWhere("student = :student")
            ->andWhere("exam.course = :course")
            ->setParameter("student", $student)
            ->setParameter("course", $course)
            ->getQuery()
            ->getResult();
    }

    public function findByStudent(Student $student)
    {
        return $this->createQueryBuilder("exam")
            ->leftJoin("exam.course", "course")
            ->leftJoin("course.students", "student")
            ->andWhere("student = :student")
            ->setParameter("student", $student)
            ->getQuery()
            ->getResult();
    }
}