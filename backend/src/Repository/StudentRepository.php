<?php

namespace App\Repository;

use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class StudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Student::class);
    }

    public function save(Student $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findRegistered()
    {
        return $this->createQueryBuilder('student')
            ->andWhere('student.registered = 1')
            ->getQuery()
            ->getResult();
    }

    public function findOneForFirstLogin(string $email)
    {
        return $this->createQueryBuilder('student')
            ->leftJoin('student.user', 'user')
            ->andWhere('user.email = :email')
            ->andWhere('student.registered = 0 OR student.registered IS NULL')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findForCourses(): array
    {
        return $this->createQueryBuilder('student')
            ->leftJoin('student.user', 'user')
            ->leftJoin('student.courses', 'course')
            ->andWhere('student.registered = 1')
            ->andWhere('course IS NOT NULL')
            ->addOrderBy('user.lastName', 'ASC')
            ->addOrderBy('user.firstName', 'ASC')
            ->getQuery()
            ->getResult();
    }
}