<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function save(Notification $notification)
    {
        $this->getEntityManager()->persist($notification);
        $this->getEntityManager()->flush();
    }

    public function getNotifications(User $user)
    {
        return $this->createQueryBuilder("notification")
            ->andWhere('notification.target = :user')
            ->setParameter('user', $user)
            ->addOrderBy('notification.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}