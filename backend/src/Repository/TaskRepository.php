<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Student;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function save(Task $task)
    {
        $this->getEntityManager()->persist($task);
        $this->getEntityManager()->flush();
    }

    public function findByStudentAndCourse(Student $student, Course $course)
    {
        return $this->createQueryBuilder("tasks")
            ->leftJoin("tasks.course", "course")
            ->leftJoin("course.students", "student")
            ->andWhere("student = :student")
            ->andWhere("tasks.course = :course")
            ->setParameter("student", $student)
            ->setParameter("course", $course)
            ->getQuery()
            ->getResult();
    }

    public function findByStudent(Student $student)
    {
        return $this->createQueryBuilder("tasks")
            ->leftJoin("tasks.course", "course")
            ->leftJoin("course.students", "student")
            ->andWhere("student = :student")
            ->setParameter("student", $student)
            ->getQuery()
            ->getResult();
    }
}