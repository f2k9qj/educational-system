<?php

namespace App\Repository;

use App\Entity\Assessment;
use App\Entity\Semester;
use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AssessmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Assessment::class);
    }

    public function save(Assessment $assessment)
    {
        $this->getEntityManager()->persist($assessment);
        $this->getEntityManager()->flush();
    }

    public function findStudentYearlyGrades(Student $student, Semester $semester)
    {
        return $this->createQueryBuilder("assessment")
            ->leftJoin('assessment.exam', 'exam')
            ->leftJoin('assessment.task', 'task')
            ->leftJoin('exam.course', 'examCourse')
            ->leftJoin('task.course', 'taskCourse')
            ->andWhere('(
                examCourse.semester = :semester OR 
                taskCourse.semester = :semester
            )')
            ->andWhere('assessment.student = :student')
            ->andWhere('assessment.gradedAt IS NOT NULL')
            ->setParameter('student', $student)
            ->setParameter('semester', $semester)
            ->getQuery()
            ->getResult();
    }

}