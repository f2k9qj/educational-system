<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Semester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function save(Course $course)
    {
        $this->getEntityManager()->persist($course);
        $this->getEntityManager()->flush();
    }

    public function findCoursesForSemester(Semester $semester)
    {
        return $this->createQueryBuilder('courses')
            ->andWhere('courses.semester = :semester')
            ->setParameter('semester', $semester)
            ->getQuery()
            ->getResult();
    }

    public function findCoursesForStudent(int $studentId)
    {
        return $this->createQueryBuilder('courses')
            ->leftJoin('courses.students', 'student')
            ->andWhere('student.id = :student')
            ->setParameter('student', $studentId)
            ->getQuery()
            ->getResult();
    }
}