<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Course
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $courseCode = null;

    #[ORM\ManyToOne(targetEntity: Room::class)]
    private ?Room $location = null;

    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $minHeadCount = null;

    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $maxHeadCount = null;

    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $maxAbsence = null;

    #[ORM\ManyToOne(targetEntity: Semester::class)]
    private ?Semester $semester = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: "course_instructors")]
    private Collection $instructors;

    #[ORM\ManyToMany(targetEntity: Student::class, inversedBy: 'courses')]
    #[ORM\JoinTable(name: "course_students")]
    private Collection $students;

    public function __construct()
    {
        $this->instructors = new ArrayCollection();
        $this->students = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getCourseCode(): ?string
    {
        return $this->courseCode;
    }

    public function setCourseCode(string $courseCode)
    {
        $this->courseCode = $courseCode;
    }

    public function getLocation(): ?Room
    {
        return $this->location;
    }

    public function setLocation(Room $location)
    {
        $this->location = $location;
    }

    public function getMinHeadCount(): ?int
    {
        return $this->minHeadCount;
    }

    public function setMinHeadCount(?int $minHeadCount)
    {
        $this->minHeadCount = $minHeadCount;
    }

    public function getMaxHeadCount(): int
    {
        return $this->maxHeadCount;
    }

    public function setMaxHeadCount(?int $maxHeadCount)
    {
        $this->maxHeadCount = $maxHeadCount;
    }

    public function getMaxAbsence(): ?int
    {
        return $this->maxAbsence;
    }

    public function setMaxAbsence(?int $maxAbsence)
    {
        $this->maxAbsence = $maxAbsence;
    }

    public function getSemester(): ?Semester
    {
        return $this->semester;
    }

    public function setSemester(Semester $semester)
    {
        $this->semester = $semester;
    }

    public function addInstructor(User $instructor): void
    {
        $this->instructors[] = $instructor;
    }

    public function removeInstructor(User $user): void
    {
        $this->instructors->removeElement($user);
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getInstructors(): ArrayCollection|Collection
    {
        return $this->instructors;
    }

    public function addStudent(Student $student): void
    {
        $this->students[] = $student;
    }

    public function removeStudent(Student $student): void
    {
        $this->students->removeElement($student);
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getStudents(): ArrayCollection|Collection
    {
        return $this->students;
    }
}