<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Task extends Submission
{
    public const TYPE_MANDATORY = 'mandatory';
    public const TYPE_OPTIONAL = 'optional';

    public const TYPES = [
        self::TYPE_MANDATORY,
        self::TYPE_OPTIONAL,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "string")]
    private string $type;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $deadline = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getDeadline(): ?DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?DateTimeInterface $deadline): void
    {
        $this->deadline = $deadline;
    }
}