<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Student
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(targetEntity: User::class, cascade: ["persist"])]
    private ?User $user = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $motherLastName = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $motherFirstName = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $studentCardNumber = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $idCardNumber = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $birthDate = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $birthPlace = null;

    #[ORM\Column(type: "boolean", nullable: true)]
    private ?bool $registered = null;

    #[ORM\ManyToMany(targetEntity: Course::class, mappedBy: 'students')]
    private Collection $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getMotherLastName(): ?string
    {
        return $this->motherLastName;
    }

    public function setMotherLastName(?string $motherLastName): void
    {
        $this->motherLastName = $motherLastName;
    }

    public function getMotherFirstName(): ?string
    {
        return $this->motherFirstName;
    }

    public function setMotherFirstName(?string $motherFirstName): void
    {
        $this->motherFirstName = $motherFirstName;
    }
    public function getStudentCardNumber(): ?string
    {
        return $this->studentCardNumber;
    }

    public function setStudentCardNumber(?string $studentCardNumber): void
    {
        $this->studentCardNumber = $studentCardNumber;
    }

    public function getIdCardNumber(): ?string
    {
        return $this->idCardNumber;
    }

    public function setIdCardNumber(?string $idCardNumber): void
    {
        $this->idCardNumber = $idCardNumber;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(?string $birthPlace): void
    {
        $this->birthPlace = $birthPlace;
    }

    public function getBirthDate(): ?DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?DateTimeInterface $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function getRegistered(): ?bool
    {
        return $this->registered;
    }

    public function setRegistered(bool $registered)
    {
        $this->registered = $registered;
    }
}