<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Semester
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "string")]
    private string $name;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $startDate = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $endDate = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $enrollmentStartDate = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $enrollmentEndDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function setEnrollmentEndDate(?DateTimeInterface $enrollmentEndDate): void
    {
        $this->enrollmentEndDate = $enrollmentEndDate;
    }

    public function getEnrollmentEndDate(): ?DateTimeInterface
    {
        return $this->enrollmentEndDate;
    }

    public function setEnrollmentStartDate(?DateTimeInterface $enrollmentStartDate): void
    {
        $this->enrollmentStartDate = $enrollmentStartDate;
    }

    public function getEnrollmentStartDate(): ?DateTimeInterface
    {
        return $this->enrollmentStartDate;
    }
}