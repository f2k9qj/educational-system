<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Notification
{
    const NOTIFICATION_GRADED = "graded";
    const NOTIFICATION_ENROLLED = "enrolled";
    const NOTIFICATION_TASK = "task";
    const NOTIFICATION_EXAM = "exam";
    const NOTIFICATION_SEMESTER = "semester";
    const NOTIFICATION_ROOM = "room";
    const NOTIFICATION_COURSE = "course";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $title = null;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $message = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $type = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $target = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $createdAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getTarget(): User
    {
        return $this->target;
    }

    public function setTarget(User $target): void
    {
        $this->target = $target;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}