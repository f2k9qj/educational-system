<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
class Submission
{
    public const SCORE_GADE = 'grade';
    public const SCORE_PERCENTAGE = 'percentage';
    public const SCORE_DONE = 'done';

    public const SCORING = [
        self::SCORE_DONE,
        self::SCORE_GADE,
        self::SCORE_PERCENTAGE,
    ];

    #[ORM\Column(type: "string")]
    private string $name;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity: Course::class)]
    private ?Course $course = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $scoring = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course)
    {
        $this->course = $course;
    }

    public function getScoring(): string
    {
        return $this->scoring;
    }

    public function setScoring(string $scoring): void
    {
        $this->scoring = $scoring;
    }
}