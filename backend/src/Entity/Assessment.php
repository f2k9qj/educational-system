<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Assessment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $grade = null;

    #[ORM\ManyToOne(targetEntity: Student::class)]
    private ?Student $student = null;

    #[ORM\ManyToOne(targetEntity: Task::class)]
    private ?Task $task = null;

    #[ORM\ManyToOne(targetEntity: Exam::class)]
    private ?Exam $exam = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?DateTimeInterface $gradedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(string $grade): void
    {
        $this->grade = $grade;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(Student $student)
    {
        $this->student = $student;
    }

    public function getExam(): ?Exam
    {
        return $this->exam;
    }

    public function setExam(Exam $exam)
    {
        $this->exam = $exam;
    }

    public function getTask(): ?Task
    {
        return $this->task;
    }

    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    public function getGradedAt(): ?DateTimeInterface
    {
        return $this->gradedAt;
    }

    public function setGradedAt(?DateTimeInterface $gradedAt): void
    {
        $this->gradedAt = $gradedAt;
    }
}