<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231022094702 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE assessment (id INT AUTO_INCREMENT NOT NULL, student_id INT DEFAULT NULL, task_id INT DEFAULT NULL, exam_id INT DEFAULT NULL, grade VARCHAR(255) DEFAULT NULL, graded_at DATETIME DEFAULT NULL, INDEX IDX_F7523D70CB944F1A (student_id), INDEX IDX_F7523D708DB60186 (task_id), INDEX IDX_F7523D70578D5E91 (exam_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assessment ADD CONSTRAINT FK_F7523D70CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE assessment ADD CONSTRAINT FK_F7523D708DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE assessment ADD CONSTRAINT FK_F7523D70578D5E91 FOREIGN KEY (exam_id) REFERENCES exam (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assessment DROP FOREIGN KEY FK_F7523D70CB944F1A');
        $this->addSql('ALTER TABLE assessment DROP FOREIGN KEY FK_F7523D708DB60186');
        $this->addSql('ALTER TABLE assessment DROP FOREIGN KEY FK_F7523D70578D5E91');
        $this->addSql('DROP TABLE assessment');
    }
}
