<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231028092112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course ADD location_id INT DEFAULT NULL, DROP location');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB964D218E FOREIGN KEY (location_id) REFERENCES room (id)');
        $this->addSql('CREATE INDEX IDX_169E6FB964D218E ON course (location_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB964D218E');
        $this->addSql('DROP INDEX IDX_169E6FB964D218E ON course');
        $this->addSql('ALTER TABLE course ADD location VARCHAR(255) DEFAULT NULL, DROP location_id');
    }
}
