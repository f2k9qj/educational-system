<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230930153256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, semester_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, course_code VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, min_head_count INT DEFAULT NULL, max_head_count INT DEFAULT NULL, max_absence INT DEFAULT NULL, UNIQUE INDEX UNIQ_169E6FB94A798B6F (semester_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_instructors (course_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9733ADF5591CC992 (course_id), INDEX IDX_9733ADF5A76ED395 (user_id), PRIMARY KEY(course_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_students (course_id INT NOT NULL, student_id INT NOT NULL, INDEX IDX_DDDE0E4591CC992 (course_id), INDEX IDX_DDDE0E4CB944F1A (student_id), PRIMARY KEY(course_id, student_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB94A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE course_instructors ADD CONSTRAINT FK_9733ADF5591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_instructors ADD CONSTRAINT FK_9733ADF5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_students ADD CONSTRAINT FK_DDDE0E4591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course_students ADD CONSTRAINT FK_DDDE0E4CB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB94A798B6F');
        $this->addSql('ALTER TABLE course_instructors DROP FOREIGN KEY FK_9733ADF5591CC992');
        $this->addSql('ALTER TABLE course_instructors DROP FOREIGN KEY FK_9733ADF5A76ED395');
        $this->addSql('ALTER TABLE course_students DROP FOREIGN KEY FK_DDDE0E4591CC992');
        $this->addSql('ALTER TABLE course_students DROP FOREIGN KEY FK_DDDE0E4CB944F1A');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE course_instructors');
        $this->addSql('DROP TABLE course_students');
    }
}
