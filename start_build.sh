docker-compose up -d --build
docker exec edu_sys_fpm composer install
docker exec edu_sys_fpm php bin/console doctrine:database:create
docker exec edu_sys_fpm php bin/console doctrine:migrations:migrate
docker exec edu_sys_fpm php bin/console lexik:jwt:generate-keypair --skip-if-exists
